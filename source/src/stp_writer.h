#ifndef STPWRITER_H
#define STPWRITER_H

#include "common.h"

class STPWriter : public IGraphWriter
{
public:
    /*!
     * \brief Save graph to output stream function.
     * \param os Output stream.
     * \param graph Graph to be written.
     * \param terminals Terminal list.
     * \param root Tree root.
     */
    void save(std::ostream &os, BoostGraph const& graph,
              std::vector<size_t> const& terminals, size_t root) override;

    /*!
     * \brief Save graph to file function.
     * \param fname File name.
     * \param terminals Terminal list.
     * \param root Tree root.
     * \param graph Graph to be written.
     */
    void save(std::string const& fname, BoostGraph const& graph,
              std::vector<size_t> const& terminals, size_t root) override;
};

#endif // STPWRITER_H
