#include <iomanip>
#include <iostream>
#include <fstream>
#include <memory>
#include <chrono>
#include <typeinfo>
#include "test_utils.h"
#include "stp_writer.h"
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/connected_components.hpp>

#define UNUSE_PARAM(x) (void)(x)


//! Append value to `values` without formatting.
template<typename T>
OutputValueCarrier& OutputValueCarrier::operator<<(T value) {
    std::stringstream ss;
    ss << value;
    values.push_back(ss.str());
    return *this;
}

//! Wrap string into quotes and append to `values`.
template<>
OutputValueCarrier& OutputValueCarrier::operator<< <std::string>(std::string value) {
    std::stringstream ss;
    ss << std::quoted(value);
    values.push_back(ss.str());
    return *this;
}

template
OutputValueCarrier& OutputValueCarrier::operator<< <double>(double value);


TestResult::TestResult(TimedSolution const& timedSol) {
    sptCost = timedSol.sol.sptCost;
    solution = timedSol.sol;
    time = timedSol.time;
    validCost = timedSol.validCost;
}

std::ostream& operator<<(std::ostream& os, TestResult const& result) {
    os << "Cost: " << result.sptCost << "; "
       << "Time: " << result.time;
    if (result.validCost != std::numeric_limits<size_t>::max()) {
        if (result.sptCost < result.validCost)
            os << "; Error: approximate cost is lower than exact solution";
        else
        {
            double gap = (result.sptCost - result.validCost) * 100.0;
            gap /= result.validCost;
            os << "; Gap to optimal: " << gap << "%";
        }
    }
    return os;
}

std::string Scenario::getStepName(std::size_t step) {
    std::vector<std::string> stepNames = {
        "Working path",
        "Protection path",
    };
    return stepNames[step];
}

std::unique_ptr<Scenario> Scenario::make(std::string const& name) {
    if (name == "basic")
        return std::make_unique<BasicScenario>();

    return nullptr;
}

TestingResults BasicScenario::run(TestCase const& testCase, ISPTSolver& solver) {
    auto test = dynamic_cast<BasicCase const&>(testCase);
    auto timedSolution = tryCalculateSPT(test.task, solver, "Error in solver");
    return { TestResult(timedSolution) };
}

TimedSolution Scenario::calculateSPT(Task const& task, ISPTSolver& solver) {
    auto start = std::chrono::high_resolution_clock::now();
    auto solution = solver.solve(task);
    auto end = std::chrono::high_resolution_clock::now();

    size_t treeCost = GraphTestUtil::computeSPTCost(solution.tree);
    if (treeCost != solution.sptCost) {
        throw std::runtime_error("Incorrect tree cost");
    }

    std::chrono::duration<double> diff = end - start;
    return TimedSolution { solution, diff.count(), task.validCost };
}

TimedSolution Scenario::tryCalculateSPT(Task const& task, ISPTSolver& solver, std::string const& exceptionComment) {
    try {
        return calculateSPT(task, solver);
    } catch (std::exception& e) {
        throw std::runtime_error(exceptionComment + ": " + e.what() + "\n");
    }
}

size_t GraphTestUtil::computeSPTCost(BoostGraph const& g) {
    return g.cost();
}

/*!
 * \brief Cycle detector visitor used with `boost::depth_first_search`.
 */
struct CycleDetector : public boost::dfs_visitor<> {
    CycleDetector(bool& hasCycle) : _hasCycle(hasCycle) {}

    template <class Edge, class Graph>
    void examine_edge(Edge e, Graph& g) {
        if (visited.count(boost::target(e, g)))
            _hasCycle = true;

        visited.insert(boost::target(e, g));
    }

    template <class Edge, class Graph>
    void back_edge(Edge e, Graph&) {
        _hasCycle = true;
    }

private:
    bool& _hasCycle;
    std::set<size_t> visited;
};

bool GraphTestUtil::isAcyclic(const BoostGraph &graph)
{
    bool hasCycle = false;
    CycleDetector cycleDetector(hasCycle);

    // Note, that `depth_first_search` could be replaced with `depth_first_visit`
    // and then separate computation of connected components will be superfluous.
    boost::depth_first_search(graph, boost::visitor(cycleDetector));

    return !hasCycle;
}

bool GraphTestUtil::isTreeConnected(BoostGraph const& graph, size_t source) {

    auto dijkstra = graph.shortestPaths(source);

    for (auto &dist: dijkstra.distances)
        if (dist == std::numeric_limits<size_t>::max())
            return false;
    return true;
}

bool GraphTestUtil::areTerminalsReached(const BoostGraph &graph, const std::vector<size_t> &terminals, size_t source)
{
    auto path = graph.shortestPaths(source);

    for (auto &t: terminals)
        if (path.distances[graph.getBoostVByOrigV(t)] == std::numeric_limits<size_t>::max())
            return false;

    return true;
}

bool GraphTestUtil::isTree(const BoostGraph &graph, size_t root)
{
    return isTreeConnected(graph, root) && isAcyclic(graph);
}

bool GraphTestUtil::areLeavesTerminals(BoostGraph const& tree, std::vector<size_t> const& terminals) {
    std::set<size_t> terminalSet(terminals.begin(), terminals.end());
    BoostGraph::vertex_iterator v, vend;
    for (boost::tie(v, vend) = boost::vertices(tree); v != vend; ++v) {
        // if node v is a leaf
        auto outEdges = boost::out_edges(*v, tree);
        auto numOutEdges = std::distance(outEdges.first, outEdges.second);
        if (numOutEdges == 0) {
            auto vertexId = tree[*v];
            // it should be a terminal
            if (!terminalSet.count(vertexId))
                return false;
        }
    }
    return true;
}

/*! \brief Visitor for back edge remove */
struct BackEdgeRemover : public boost::default_dfs_visitor {
    BackEdgeRemover(std::vector<BoostGraph::edge_descriptor> &edges ) :
        _idx(0), _edges(edges) {}

    template <class Edge, class Graph>
    void back_edge(Edge const &e, Graph&) {
        _edges[_idx++] = e;
    }

private:
    size_t _idx;
    std::vector<BoostGraph::edge_descriptor> &_edges;
};

void GraphUtil::removeBackEdges(BoostGraph &solTree, Vertex root)
{
    std::vector<BoostGraph::edge_descriptor> bedges(boost::num_edges(solTree) / 2);
    BackEdgeRemover edgeRemover(bedges);
    boost::depth_first_search(
                solTree,
                boost::visitor(edgeRemover).
                root_vertex(solTree.getBoostVByOrigV(root)));
    for (auto &e: bedges)
        boost::remove_edge(e, solTree);   
}

Benchmark::Benchmark(TestSet&& tests, unsigned repetitions) :
    _tests(std::forward<TestSet>(tests)),
    _repetitions(repetitions)
{}

void Benchmark::run(ITaskLoader const& loader, ISPTSolver& solver)
{
    BenchmarkFormatter format({"file",
        "threads",
        "mean_cost",
        "median_cost",
        "mean_time",
        "median_time"
    });

    std::vector<size_t> costs;
    for (auto& test : _tests.fileNames) {
        Statistics timeStats;
        Statistics costStats;

        Task task = loader.load(test);
        task.threadCount = _tests.threads;

        for (size_t i = 0; i < _repetitions; ++i) {
            try
            {
                std::cerr << "File " << test << " has been started\n";
                auto start = std::chrono::high_resolution_clock::now();

                auto solution = solver.solve(task);
                /* auto totalcost = solution.sptCost; */

                auto end = std::chrono::high_resolution_clock::now();

                std::cerr << "File " << test << " has been successfully processed\n";
                std::ofstream ofs(test + "_ans.stp");

                if (!ofs)
                    std::cerr << "Error opening file";
                STPWriter writer;
                writer.save(ofs, solution.tree, task.terminals, task.source);

                std::chrono::duration<double> diff = end - start;
                timeStats << diff.count();

                // check solution cost
                size_t solutionCost = GraphTestUtil::computeSPTCost(solution.tree);
                costStats << static_cast<double>(solutionCost);
            } catch (std::runtime_error &err)
            {
                std::cerr << "Runtime error: " << err.what() << "\n";
                continue;
            }
        }


        // SPT file name
        format["file"] << test;
        format["threads"] << task.threadCount;
        // Mean cost
        format["mean_cost"] << costStats.mean();
        // Median cost(
        format["median_cost"] << costStats.median();
        // Mean time
        format["mean_time"] << timeStats.mean();
        // Median time
        format["median_time"] << timeStats.median();
    }

    /* std::cout << "\nMean error: " << errorStats.get() << "%\n"; */
    std::cout << format << std::endl;

}


std::unique_ptr<TestCase> TestCaseFactory::make(
        Scenario& s,
        ITaskLoader const& loader,
        std::string const& file)
{
    // determine scenario type and make appropriate test case
    if (typeid(s) == typeid(BasicScenario)) {
        auto test = std::make_unique<BasicCase>(loader.load(file));
        return test;
    }

    return nullptr;
}

ScenarioBenchmark::ScenarioBenchmark(ScenarioTestSet&& tests, BenchmarkOutputMode mode) :
    _tests(std::forward<ScenarioTestSet>(tests)),
    _outputMode(mode) {}

AllTestingResults ScenarioBenchmark::run(ITaskLoader const& loader,
                            Scenario& scenario,
                            ISPTSolver& solver)
{
    AllTestingResults allResults;
    BenchmarkFormatter format({
        "file",
        /* other keys will be added dynamically */
    });
    if (_outputMode != BenchmarkOutputMode::silent)
        std::cout << "Solver: " << typeid(solver).name() << std::endl;
    for (auto& test : _tests) {
        format["file"] << test.first;
        if (_outputMode == BenchmarkOutputMode::pretty) {
            std::cout << "File: " << test.first << std::endl;
        }

        auto testCase = TestCaseFactory::make(scenario,
                loader,
                test.first);
        if (!testCase)
            throw std::runtime_error("Can't make test case");

        try {
            auto results = scenario.run(*testCase, solver);

            if (_outputMode != BenchmarkOutputMode::silent) {
                for (size_t step = 0; step < results.size(); ++step) {
                    auto& result = results[step];
                    std::string stepNum = std::to_string(step + 1);
                    std::string stepName = stepNum + "(" + scenario.getStepName(step) + ")";

                    if (_outputMode == BenchmarkOutputMode::pretty) {
                        std::cout << "  Step " << (step + 1) << " ("
                                  << scenario.getStepName(step) << "):"
                                  << std::endl;
                        std::cout << "    " << result << std::endl;
                    }


                    format["cost_" + stepName] << result.sptCost;
                    format["time_" + stepName] << result.time;

                    format["opt_" + stepName] << result.validCost;
                    double gap = (result.sptCost - result.validCost) * 100.0;
                    gap /= result.validCost;
                    format["gap_" + stepName] << gap;
                }

            }

            allResults.emplace(test.first, results);
        } catch (std::exception &e) {
            std::cerr << "Testcase error: " << e.what();
        }
    }

    if (_outputMode == BenchmarkOutputMode::csv) {
        std::cout << format << std::endl;
    }

    return allResults;
}


Statistics& Statistics::operator<<(double v) {
    _acc += v;
    _values.push_back(v);
    return *this;
}

double Statistics::mean() {
    if (_values.size() == 0)
        return 0.0;
    return _acc / _values.size();
}

double Statistics::median() {
    size_t size = _values.size();
    if (size == 0)
        return 0.0;

    if (size % 2 != 0) {
        size_t pos = size / 2;
        std::nth_element(_values.begin(), _values.begin() + pos, _values.end());
        return _values[pos];
    }

    // Median = (left + right) / 2
    size_t leftPos = size / 2;
    size_t rightPos = leftPos + 1;
    std::nth_element(_values.begin(), _values.begin() + rightPos, _values.end());
    double right = _values[rightPos];
    std::nth_element(_values.begin(), _values.begin() + leftPos, _values.end());
    double left = _values[leftPos];
    return (left + right) / 2.0;
}


BenchmarkFormatter::BenchmarkFormatter(std::vector<std::string>&& keys) : _keys(keys) {
    for (const std::string& key : keys) {
        _columns[key] = OutputValueCarrier();
    }
}

OutputValueCarrier& BenchmarkFormatter::operator [](std::string const& key) {
    auto it = _columns.find(key);
    if (it == _columns.end()) {
        _keys.push_back(key);
        _columns[key] = OutputValueCarrier();
        return _columns[key];
    }
    return it->second;
}

//! Put formatter contents into output stream.
std::ostream& operator<<(std::ostream& os, BenchmarkFormatter const& f) {
    /* for (auto& k : f._keys) { */
    for (auto it = f._keys.begin(); it != f._keys.end(); ++it) {
        os << std::quoted(*it);
        if (std::next(it) != f._keys.end()) {
            os << f._separator;
        }
    }
    os << std::endl;
    for (unsigned i = 0; i < f._columns.at(f._keys[0]).values.size(); i++) {
        for (auto it = f._keys.begin(); it != f._keys.end(); ++it) {
            if (f._columns.count(*it)) {
                os << f._columns.at(*it).values.at(i);
            }
            if (std::next(it) != f._keys.end()) {
                os << f._separator;
            }
        }
        os << std::endl;
    }
    return os;
}

void GraphUtil::rmBranch( std::vector<Vertex> const &path,
               BoostGraph const &graph,
               Vertex source,
               BoostGraph &tree,
               std::vector<Vertex> const& terminals,
               std::vector<Vertex> const& terminalsRm)
{
    // reverse iterators for faster search

    // otherwise remove nonterminal branch

    // remove edges until
    // - terminal that MUST NOT TO BE removed is reached
    //   condition: not primary terminal || (primary && removable) ==
    //   not primary || removable
    // - branching point reached
    auto it = path.rbegin();
    for (;
         (std::find(
             terminals.begin(), terminals.end(),
              graph[*it]) == terminals.end() ||
          std::find(
             terminalsRm.begin(), terminalsRm.end(),
              graph[*it]) != terminalsRm.end()) &&
         tree.outDegree(graph[*it]) == 0 &&
         it + 1 != path.rend();
         ++it)

    {
        tree.removeEdge(graph[*(it + 1)], graph[*it]);
        tree.removeVertex(graph[*it]);
    }

    // removed source
    if (it + 1 == path.rend())
    {
        if (graph[*it] != source)
        {
//                std::cout << _task.graph[*it] << " " << _task.source;
            throw std::runtime_error("Start point of path is not source");
        }

        if (tree.outDegree(source) == 0)
            tree.removeVertex(source);
    }
}

void GraphUtil::rmNtermBranchUndir(
        Vertex source,
        std::vector<Vertex> const &terminals,
        BoostGraph &tree)
{
    BoostGraph::vertex_iterator vertex, vertex_end;
    for (boost::tie(vertex, vertex_end) = boost::vertices(tree); vertex != vertex_end; ++vertex)
    {
        auto out_edges = boost::out_edges(*vertex, tree);
        auto out_edges_count = std::distance(out_edges.first, out_edges.second);

        // ORIGINAL vertex
        auto curVertex = tree[*vertex];

        assert(out_edges_count > 0);
        if (out_edges_count == 1 &&
                std::find(terminals.begin(), terminals.end(), curVertex) == terminals.end() &&
                source != curVertex)
        {
            size_t vertexId;
            bool run = true;
            std::vector<BoostGraph::out_edge_iterator> edgesToRemove;

            do
            {
                vertexId = tree[*(out_edges.first)].targetID;
                if (boost::out_degree(tree.getBoostVByOrigV(vertexId), tree) > 2)
                    run = false;

                for (auto l_out_edges = tree.outEdges(vertexId);
                     l_out_edges.first != l_out_edges.second; l_out_edges.first++)
                {
                    vertexId = tree[*(l_out_edges.first)].targetID;

                    edgesToRemove.push_back(l_out_edges.first);

                    run = std::find(terminals.begin(), terminals.end(), vertexId) == terminals.end();

                    // edge from terminal to the leave direction
                    if (!run)
                        tree.removeEdge(
                                    tree[*(l_out_edges.first)].targetID,
                                    tree[*(l_out_edges.first)].sourceID);
                }
            } while (run);

            for (auto &e: edgesToRemove)
                boost::remove_edge(e, tree);
        }
    }
}
