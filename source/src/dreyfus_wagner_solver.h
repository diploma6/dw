#ifndef SPTSOLVERIMPL_H
#define SPTSOLVERIMPL_H

#include "common.h"
#include "boost_graph.h"

/*!
 * \brief Solves task defined in SPT by Dreyfus-Wagner algorithm.
 */
class DreyfusWagnerSolver : public ISPTSolver
{
public:
    /*!
     * \brief Solves task defined in SPT. First terminal should be source.
     * \param task Task (problem) to solve.
     * \return Problem solution: tree and cost.
     */
    Solution solve(Task const& task) override;

    void setDeleteBackEdges(bool del);

private:
    /*!
     * \brief Builds MST over one vertex function.
     * \param terminal Vertex (terminal) number
     * \param resMinSpanTree MST
     */
    void buildMinSpanTree1Vert( size_t terminal, BoostGraph &resMinSpanTree ) const;

    /*!
     * \brief Builds MST over two terminals function.
     * \param terminal0 Terminal number
     * \param terminal1 Terminal number
     * \param resMinSpanTree MST
     */
    void buildMinSpanTree2Vert( size_t terminal0, size_t terminal1,
                                BoostGraph &resMinSpanTree ) const;

    /*!
     * \brief MST solver function
     * \return MST
     */
    Solution buildMinSpanTree( void );

    /*!
     * \brief Builds MST for trivial cases (1, 2 terminal) function.
     * \param m_terminals Terminal set
     * \param resMinSpanTree MST
     * \return true if trivial case, false otherwise
     */
    bool mstTrivialCases( std::vector<size_t> const &m_terminals, BoostGraph &resMinSpanTree );

    /*!
     * \brief Build MST over 1 terminal and ALL vertices function.
     * \param m_terminals Terminal set
     * \param m_tsize |T|
     * \param vertexCount |V|
     */
    void mst1Terminal(std::vector<size_t> const &m_terminals,
                      size_t m_tsize,
                      size_t vertexCount);

    /*!
     * \brief Restore MST function.
     * \param vc |V|
     * \param m_tsize |T|
     * \param terminalBack T[-1]
     * \return MST
     */
    BoostGraph mstFull( size_t vc, size_t m_tsize, size_t terminalBack ) const;

    /*!
     * \brief Recursive restore MST function (TMP) function.
     * \param x_comb Curent terminal combination
     * \param vc |V|
     * \param m_tsize |T|
     * \param v Common boostvertex
     * \param mst MST spanning over X U {v}
     */
    void mstFullRec( size_t x_comb, size_t vc, size_t m_tsize, size_t v, BoostGraph &mst ) const;


    /*!
     * \brief Precompute Dijkstra function.
     */
    void precomputeDijkstra();

    void dijkstraThread( size_t start, size_t end );

    size_t combinationIndex( size_t terminalComb,
                             size_t w, size_t vertexCount ) const;

    struct SolutionData
    {
        //! idx of T(x1 U v)
        size_t bipartition1_comb = maxUint;
        //! common vertex
        size_t w = maxUint;
        //! weight
        size_t weight = maxUint;
    };

    void minimizerPerComb(
            size_t *x_combs,
            size_t term_size,
            size_t comb_start, size_t comb_end,
            size_t vc ) const;

    //std::unordered_map<size_t, BoostGraph::BoostPath> precomputedPaths;
    std::vector<BoostGraph::BoostPath> precomputedPaths;

    SolutionData *solution_costs;

    static const size_t maxUint;
    Task _task;
    bool _deleteBackEdges = true;
};

#endif // SPTSOLVERIMPL_H
