#ifndef KMBSOLVER_H
#define KMBSOLVER_H

#include "common.h"
#include "boost_graph.h"

class KMBSolver : public ISPTSolver
{
public:
    Solution solve(Task const& task) override;

private:
    /*!
     * \brief FirstKMB step: build complete tree on terminals functions
     * \param graph1 Graph to be built
     */
    void step1(BoostGraph& graph1);

    /*!
     * \brief Build graph from edges list function.
     * \details Needed for transform boost Kruskal's algorithm
     * output to BoostGraph structure.
     * \param minMST Edges list
     * \param newGraph Built graph
     */
    void makeGraphFromMST(std::vector<BoostGraph::edge_descriptor>& minMST, BoostGraph& newGraph) const;

    /*!
     * \brief Delete non-terminal branches function.
     * \param minMST Tree to delete leaves from
     */
    void deleteLeaves(BoostGraph& minMST) const;

    //! vertex number -> Dijkstra data map
    std::unordered_map<size_t, BoostGraph::BoostPath> _dijkstra;
    //! Task to solve
    Task _task;
};

#endif // KMBSOLVER_H
