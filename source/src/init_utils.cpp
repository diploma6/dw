#include "init_utils.h"

// Solvers
#include "dreyfus_wagner_solver.h"
#include "erickson_monma_solver.h"
#include "check_reach_solver.h"

std::unique_ptr<ISPTSolver> createSolverByName(std::string const& solverName)
{
    if (solverName == "DW")
        return std::make_unique<DreyfusWagnerSolver>();
    if (solverName == "EMV")
        return std::make_unique<EricksonMonmaSolver>();
    if (solverName == "CR")
        return std::make_unique<CheckReachSolver>();
    return nullptr;
}

std::string appendPrefix(std::string const& fname, std::string const& prefix) {
    std::string basename = fname.substr(fname.find_last_of("/\\") + 1);
    std::string dirname = fname.substr(0, fname.length() - basename.length());
    return dirname + prefix + basename;
}

TestSet generateTestSetFromFile(std::string const& filename, size_t threads)
{
    std::ifstream ifs(filename);
    TestSet testset;

    if (!ifs)
        return testset;

    testset.threads = threads;

    std::string f;
    while (std::getline(ifs, f))
    {
        if (f.back() == '\r')
            f.pop_back();

        testset.fileNames.push_back(f);
    }
    return testset;
}

std::ostream & operator<<( std::ostream &os, Task const &t )
{
    os << "Source: " << t.source << "\n";
    os << "Graph:\n" << t.graph;
    os << "Terminals: ";
    std::for_each(t.terminals.begin(), t.terminals.end(),
                    [&os]( size_t t )
                    {
                        os << t << ' ';
                    });
    os << "\n";
    return os;
}
