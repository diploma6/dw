#ifndef INIT_UTILS_H
#define INIT_UTILS_H

#include <fstream>
#include "boost_graph.h"
#include "common.h"
#include "test_utils.h"

/*!
 * \brief Create SPT solver by name.
 * \param solverName Solver name.
 * \return Created SPT solver.
 */
std::unique_ptr<ISPTSolver> createSolverByName(std::string const& solverName = "");

/*!
 * \brief Append prefix to file name without changing dirname.
 * \param fname File name.
 * \param prefix Prefix to prepend.
 * \return File name with prefix.
 */
std::string appendPrefix(std::string const& fname, std::string const& prefix);

TestSet generateTestSetFromFile(std::string const& filename, size_t threads);

#endif // INIT_UTILS_H
