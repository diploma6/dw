#include <fstream>
#include "stp_writer.h"

void STPWriter::save(std::ostream &os, const BoostGraph &graph, std::vector<size_t> const& terminals, size_t root)
{
    os << "SECTION Graph\n";
    os << "Nodes " << boost::num_vertices(graph) << "\n";
    os << "Arcs " << boost::num_edges(graph) << "\n";

    graph.applyEdges(
                [&os](Edge const& edge)
                {
                    os << "A " << edge.sourceID << ' ' << edge.targetID << ' ' << edge.cost << "\n";
                });
    os << "END\n\n";

    os << "SECTION Terminals\n";
    os << "Root " << root << "\n";
    os << "Terminals " << terminals.size() << "\n";
    std::for_each(terminals.begin(), terminals.end(),
                  [&os]( size_t t ) { os << "T " << t << "\n"; });
    os << "END\n";
    os << "\nEOF\n";
}

void STPWriter::save(const std::string &fname, const BoostGraph &graph, const std::vector<size_t> &terminals, size_t root)
{
    std::ofstream ofs(fname);

    if (!ofs)
        throw std::runtime_error("Couldn't create file " + fname);

    save(ofs, graph, terminals, root);
}
