#include <iostream>
#include <random>
#include <chrono>

#include "check_reach_solver.h"
#include "common.h"

using BoostPath = BoostGraph::BoostPath;
using BoostVertex = BoostGraph::BoostVertex;

Solution CheckReachSolver::solve(Task const& task)
{
    if (task.terminals.size() == 0)
        return Solution { 0, BoostGraph() };

    //auto ec = boost::num_edges(task.graph);
    //std::cerr << "ec " << ec << "\n";
    Solution solution = solve(task, task.source, task.terminals);
    return solution;
}

Solution CheckReachSolver::solve(Task const& task, size_t source, std::vector<size_t> const& terminals)
{
    auto &g = task.graph;
    Solution solution;

    auto computedPaths = g.shortestPaths(source);

    BoostGraph spt;
    try {
        for (size_t tIdx = 0; tIdx < terminals.size(); ++tIdx)
            spt.merge(g.shortestPath(source, terminals[tIdx], computedPaths));
    } catch (std::runtime_error &e) {
        std::cerr << "Error in CheckReachSolver: " << e.what() << "\n";
        return {};
    }

    // Compute total tree cost
    solution.sptCost = spt.cost();
    // Convert list of edges to graph.
    solution.tree = std::move(spt);

    return solution;
}
