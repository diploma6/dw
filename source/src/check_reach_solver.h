#ifndef CHECKREACHSOLVER_H
#define CHECKREACHSOLVER_H

#include "common.h"

class CheckReachSolver : public ISPTSolver
{
public: 
    /*!
     * \brief Solves task defined in SPT. First terminal should be source.
     * \param task Task (problem) to solve.
     * \return Problem solution: tree and cost.
     */
    Solution solve(Task const& task) override;

private:
    /*!
     * Computes shortest path tree
     */
    Solution solve(Task const& task, size_t source, std::vector<size_t> const& terminals);
};

#endif // CHECKREACHSOLVER_H
