#ifndef COMMON_H
#define COMMON_H

#include <string>
#include <vector>
#include <boost/variant.hpp>

#include "boost_graph.h"

/*!
 * \brief Task (problem) with graph and terminals.
 */
struct Task {
    BoostGraph graph;
    size_t threadCount;
    bool isDigraph;
    size_t source = std::numeric_limits<size_t>::max();
    std::vector<size_t> terminals;
    size_t validCost = std::numeric_limits<size_t>::max();
};

std::ostream & operator<<( std::ostream &os, Task const &t );

/*!
 * \brief Solution of SPT problem.
 */
struct Solution {
    size_t sptCost;
    BoostGraph tree;
};

/*!
 * \brief Task loader interface. Can load task from file.
 */
class ITaskLoader {
public:
    /*!
     * \brief Load task from file.
     * \param fname File name.
     * \return Loaded task.
     */
    virtual Task load(std::string const& fname) const = 0;

    virtual ~ITaskLoader() {}
};

/*!
 * \brief SPT solver interface.
 */
class ISPTSolver {
public:
    /*!
     * \brief Solves task defined in SPT. First terminal should be source.
     * \param task Task (problem) to solve.
     * \return Problem solution: tree and cost.
     */
    virtual Solution solve(Task const& task) = 0;

    virtual ~ISPTSolver() {}
};

/*!
 * \brief Graph saver.
 */
class IGraphWriter
{
public:
    /*!
     * \brief Save graph to output stream function.
     * \param os Output stream.
     * \param graph Graph to be written.
     * \param terminals Terminal list.
     * \param root Tree root.
     */
    virtual void save(std::ostream &os, BoostGraph const& graph,
                      std::vector<size_t> const& terminals, size_t root) = 0;

    /*!
     * \brief Save graph to file function.
     * \param fname File name.
     * \param graph Graph to be written.
     * * \param terminals Terminal list.
     * \param root Tree root.
     */
    virtual void save(std::string const& fname, BoostGraph const& graph,
                      std::vector<size_t> const& terminals, size_t root) = 0;
};

#endif // COMMON_H
