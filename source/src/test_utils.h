#pragma once

#include "boost_graph.h"
#include "common.h"

// using TestSet = std::map<std::string, double>;
struct TestSet
{
    std::vector<std::string> fileNames;
    size_t threads;
};

struct ScenarioTestMeta {
    // size_t backupSource;
};

using ScenarioTestSet = std::map<std::string, std::pair<std::string, ScenarioTestMeta>>;

/*!
 * \brief Parent testing case.
 */
struct TestCase {
    Task task;

    //! Construct test case with task.
    TestCase(Task&& task_) : task(std::forward<Task>(task_)) {}
    virtual ~TestCase() {}
};

/*!
 * \brief Basic test case: task and optimal cost.
 */
struct BasicCase : public TestCase {
    //! Derive test case constructor.
    using TestCase::TestCase;
};

//! Solution with solving time.
struct TimedSolution {
    Solution sol;
    double time;
    size_t validCost = std::numeric_limits<size_t>::max();
};

/*!
 * \brief Scenario testing result.
 */
struct TestResult {
    /*!
     * \brief Construct test result.
     * \param timedSol Solution with time.
     */
    TestResult(TimedSolution const& timedSol);

    size_t sptCost;
    Solution solution;
    double time;
    size_t validCost = std::numeric_limits<size_t>::max();
};


/*!
 * \brief Send test result to stream.
 * \param os Output stream.
 * \param result Test result.
 * \return Output stream.
 */
std::ostream& operator<<(std::ostream& os, TestResult const& result);

/*!
 * \brief Scenario testing results container.
 * Each vector entry corresponds to sceario's stage.
 */
using TestingResults = std::vector<TestResult>;

/*!
 * \brief Testing scenario.
 */
class Scenario {
public:
    // Add delay constraints
    // ...
    
    // Add / remove vertices
    // ...

    /*!
     * \brief Scenario factory.
     * \param name Scenario name.
     * \return Smart ptr to scenario if name is valid, else nullptr.
     */
    static std::unique_ptr<Scenario> make(std::string const& name);

    /*!
     * \brief Run scenario.
     * \param test Test case, should be compatible with scenario.
     * \param solver SPT solver.
     * \return Testing results.
     */
    virtual TestingResults run(TestCase const& test, ISPTSolver& solver) = 0;

    virtual std::string getStepName(std::size_t step);

    virtual ~Scenario() {}

protected:
    /*!
     * \brief Calculate SPT.
     * Also check that cost is computed correctly.
     *
     * \param task SPT task.
     * \param solver SPT solver.
     * \return Solution with its computation time.
     */
    TimedSolution calculateSPT(Task const& task, ISPTSolver& solver);

    /*!
     * \brief Try to calculate SPT or throw informative exception.
     * It is a wrapper over `calculateSPT` method.
     *
     * \param task SPT task.
     * \param solver SPT solver.
     * \param exceptionComment Comment for exception.
     * \return Solution with its computation time.
     */
    TimedSolution tryCalculateSPT(Task const& task, ISPTSolver& solver, std::string const& exceptionComment);
};

/*!
 * \brief Basic scenario.
 * Compute SPT of the given task.
 */
class BasicScenario : public Scenario {
public:
    TestingResults run(TestCase const& test, ISPTSolver& solver) override;
};

class TestCaseFactory {
public:
    /*!
     * \brief Test case factory.
     * \param scenario Scenario.
     * \param loader Task loader.
     * \param file File name.
     * \param filemeta Metafile name.
     * \param meta Scenario test meta information like optimal cost.
     * \return Smart ptr to test case.
     */
    static std::unique_ptr<TestCase> make(
            Scenario& s,
            ITaskLoader const& loader,
            std::string const& file);
};

/*!
 * \brief Graph helpf utils class.
 */
class GraphUtil
{
public:
    /*!
     * \brief Remove non-terminal branch function.
     * \param path Path to remove from (as vector).
     * \param graph Graph to be solved MST on.
     * \param source MST Source.
     * \param tree MST.
     * \param terminals Terminals.
     * \param terminalsRm Terminals to be removed.
     * \details terminalsRm can be removed.
     */
    static void rmBranch( std::vector<Vertex> const &path,
                   BoostGraph const &graph,
                   Vertex source,
                   BoostGraph &tree,
                   std::vector<Vertex> const& terminals,
                   std::vector<Vertex> const& terminalsRm);

    /*!
     * \brief rmRemove non-terminal branch function.
     * \param pathEnd Path end.
     * \param source MST source.
     * \param tree MST.
     * \param terminals Terminals.
     * \param func If !func(edge.targetID) => stop remove edges
     */
    static void rmBranch( Vertex pathEnd,
                          Vertex source,
                          BoostGraph &tree,
                          std::vector<Vertex> const& terminals,
                          std::function<bool( Vertex )> func);

    /*!
     * \brief Delete non-terminal branches function
     * \details From undirected tree!
     * \param source Source.
     * \param terminals Terminal set.
     * \param tree Tree to delete leaves from
     */
    static void rmNtermBranchUndir(
            Vertex source,
            std::vector<Vertex> const &terminals, BoostGraph& tree);

    /*!
     * \brief Get graph with inversed direction function.
     * \param graph Graph which weights to be inversed.
     * \return Inverse weights graph.
     */
    static BoostGraph inverseDirectionGraph(BoostGraph const& graph);

    /*!
     * \brief Remove back edges from solution.
     * \param solTree Solution tree.
     * \param root Tree root.
     */
    static void removeBackEdges(BoostGraph &solTree, Vertex root);
};

/*!
 * \brief Utility for graph tests.
 */
class GraphTestUtil {
public:
    /*!
     * \brief Given a tree compute its total cost.
     * \param graph Input tree.
     * \return Total cost.
     */
    static size_t computeSPTCost(BoostGraph const& graph);

    /*!
     * \brief Check if given graph is acyclic.
     * \param graph Input graph.
     * \return True if graph is acyclic.
     */
    static bool isAcyclic(BoostGraph const& graph);

    /*!
     * \brief Check if given graph is connected.
     * \param graph Input graph.
     * \param source Source to check connectivity from.
     * \return True if graph is connected.
     */
    static bool isTreeConnected(BoostGraph const& graph, size_t source);

    static bool areTerminalsReached(BoostGraph const& graph,
                                    std::vector<size_t> const& terminals,
                                    size_t source);

    /*!
     * \brief Check if given graph is tree.
     * \param graph Input graph.
     * \param root Tree root.
     * \return True if graph is tree.
     */
    static bool isTree(BoostGraph const& graph, size_t root);

    /*!
     * \brief Check that all tree leaves are terminals.
     * \param tree Input tree.
     * \param terminals Treminals ids.
     * \return True if all leaves are terminals.
     */
    static bool areLeavesTerminals(BoostGraph const& tree, std::vector<size_t> const& terminals);
};


/*!
 * \brief Solver benchmark.
 */
class Benchmark {
public:
    /*!
     * \brief Construct benchmark with test set.
     * \param tests Test set.
     * \param repetitions Number of repetitions for the same test.
     */
    Benchmark(TestSet&& tests, unsigned repetitions = 1);

    /*!
     * \brief Load & solve task, measure gap and performance.
     * \param loader Task loader.
     * \param solver SPT solver.
     */
    void run(ITaskLoader const& loader, ISPTSolver& solver);
private:
    //! Test set.
    TestSet _tests;
    //! Number of repetitions for the same test.
    unsigned _repetitions;
    //! Output values separator.
    std::string _separator = ";";
};

using AllTestingResults = std::unordered_map<std::string, TestingResults>;

/*!
 * \brief Scenario benchmark output mode.
 */
enum class BenchmarkOutputMode {
    silent,
    pretty,
    csv,
};

/*!
 * \brief Scenario benchmark.
 */
class ScenarioBenchmark {
public:
    ScenarioBenchmark(ScenarioTestSet&& tests, BenchmarkOutputMode mode = BenchmarkOutputMode::pretty);
    /*!
     * \brief Load & solve task, measure gap and performance.
     * \param loader Task loader.
     * \param scenario Scenario.
     * \param solver SPT solver.
     * \return All results indexed by testing file names.
     */
    AllTestingResults run(ITaskLoader const& loader, Scenario& scenario, ISPTSolver& solver);
private:
    ScenarioTestSet _tests;
    BenchmarkOutputMode _outputMode;
};

/*!
 * \brief Statistics for metrics aggregation.
 */
class Statistics {
public:
    /*!
     * \brief Append observation.
     * \param value Observation value. 
     * \return self.
     */
    Statistics& operator<<(double value);

    /*!
     * \brief Compute mean value.
     * \return Mean.
     */
    double mean();

    /*!
     * \brief Compute median value.
     * \return Median.
     */
    double median();
private:
    //! Accumulator.
    double _acc = 0.0;
    //! All seen values.
    std::vector<double> _values;
};

/*!
 * \brief Output value formatter.
 */
struct OutputValueCarrier {
    //! Vector of formatted values.
    std::vector<std::string> values;

    /*!
     * \brief Append operator.
     * \param value Value to format and append.
     * \return self.
     */
    template<typename T>
    OutputValueCarrier& operator<<(T value);
};

/*!
 * \brief Benchmark output formatter.
 * Format is CSV-like.
 */
class BenchmarkFormatter {
public:
    /*!
     * \brief Construct formatter with names of columns.
     * \param keys Vector of columns names.
     */
    BenchmarkFormatter(std::vector<std::string>&& keys);

    /*!
     * \brief Access operator.
     * \param key Key (column name).
     * \return Corresponding column.
     */
    OutputValueCarrier& operator [](std::string const& key);

    friend std::ostream& operator<<(std::ostream& os, BenchmarkFormatter const& f);
private:
    std::vector<std::string> _keys;
    std::unordered_map<std::string, OutputValueCarrier> _columns;
    std::string _separator = ";";
};

/*!
 * \brief Put formatter output to the stream.
 * \param os Output stream.
 * \param f Formatter.
 * \return self.
 */
std::ostream& operator<<(std::ostream& os, BenchmarkFormatter const& f);
