#include <chrono>
#include <boost/program_options.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <iostream>
#include <fstream>

// Common interfaces
#include "boost_graph.h"
#include "common.h"

// Loaders
#include "stp_loader.h"

// Utils
#include "test_utils.h"
#include "init_utils.h"

#include "stp_writer.h"

namespace po = boost::program_options;


int main(int argc, char const *argv[])
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "produce help message")
        ("testset,t", po::value<std::string>(), "test set name or STP file name")
        ("solver,s", po::value<std::string>()->default_value("DW"), "solver name: DW, EMV, CR")
        ("index,i", po::value<size_t>()->default_value(0), "start index")
        ("repetitions,r", po::value<unsigned>()->default_value(1), "benchmark number of repetitions")
        ("check-algo,c", po::value<std::string>(), "check algorithm on graphs. "
                                                   "If <list> mentioned, arg is list of graphs. Otherwise -- STP file name")
        ("list,l", "File contains list of STP files if option mentioned")
        ("threads", po::value<size_t>()->default_value(1), "Thread count");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    auto solver = createSolverByName(vm.at("solver").as<std::string>());

    if (solver == nullptr)
    {
        std::cerr << "Bad solver name\n";
        return 1;
    }

    size_t startIdx = vm.at("index").as<size_t>();
    unsigned repetitions = vm.at("repetitions").as<unsigned>();

    if (vm.count("testset")) {
        auto name = vm["testset"].as<std::string>();
        auto curTestSet =
                vm.count("list") ?
                    generateTestSetFromFile(name, vm.at("threads").as<size_t>()) :
                    TestSet{{name}, vm.at("threads").as<size_t>()};
        Benchmark bench(std::move(curTestSet), repetitions);
        STPTaskLoader stpLoader(startIdx);

        bench.run(stpLoader, *solver);
    }

    return 0;
}
