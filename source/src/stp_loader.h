#pragma once
#include "common.h"

/*!
 * \brief Task loader implementation. Can load task from file.
 */
class STPTaskLoader : public ITaskLoader
{
public:
    /*!
     * \brief Loader constructor.
     * \param startIdx start index for vertices enumeration.
     */
    STPTaskLoader(size_t _startIdx);

    /*!
     * \brief Load task from file.
     * \param fname File name.
     * \return Loaded task.
     */
    Task load(std::string const& fname) const override;

private:
    size_t _startIdx;
};
