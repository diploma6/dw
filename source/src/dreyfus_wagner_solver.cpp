#include <limits>
#include <chrono>
#include <thread>

#include "dreyfus_wagner_solver.h"
#include "test_utils.h"
#include "stp_writer.h"

//#define BENCHMARK

#ifdef BENCHMARK
#include <iostream>
#endif

#ifdef _MSC_VER
#include <intrin.h>

using uint = unsigned int;

static inline int __builtin_ctz(unsigned x) {
    unsigned long ret;
    _BitScanForward(&ret, x);

    return (int)ret;
}
#endif


const size_t DreyfusWagnerSolver::maxUint = std::numeric_limits<size_t>::max();


Solution DreyfusWagnerSolver::solve(Task const& task)
{
    _task = task;
    precomputedPaths.clear();
    return buildMinSpanTree();
}

void DreyfusWagnerSolver::setDeleteBackEdges(bool del)
{
    _deleteBackEdges = del;
}

void DreyfusWagnerSolver::buildMinSpanTree1Vert( size_t terminal, BoostGraph &resMinSpanTree ) const
{
    resMinSpanTree = BoostGraph();
    resMinSpanTree.addVertex(terminal);
}

void DreyfusWagnerSolver::buildMinSpanTree2Vert( size_t terminal0, size_t terminal1, BoostGraph &resMinSpanTree ) const
{
    if (terminal0 == terminal1)
    {
        buildMinSpanTree1Vert(terminal0, resMinSpanTree);
        return;
    }

    auto paths = _task.graph.shortestPath(
                    terminal0, terminal1,
                precomputedPaths.empty() ? BoostGraph::BoostPath() :
                    precomputedPaths.at(_task.graph.getBoostVByOrigV(terminal0)));

    paths.applyEdges(
                [&resMinSpanTree]( Edge const &e )
    {
        resMinSpanTree.addEdge(e);
        resMinSpanTree.addEdge({e.targetID, e.sourceID, e.cost});
    });
}

size_t DreyfusWagnerSolver::combinationIndex( size_t terminalComb, size_t w, size_t vertexCount ) const
{
    return terminalComb * vertexCount + w;
}

void DreyfusWagnerSolver::minimizerPerComb(
        size_t *x_combs,
        size_t term_size,
        size_t comb_start, size_t comb_end,
        size_t vc ) const
{
    //auto term_combinations = combinations(term_size);
    // enumerate all X: |X| == term_size

    size_t idx;
    size_t t1_cost, t2_cost, cost;
    for (size_t x_idx = comb_start; x_idx < comb_end; x_idx++)
    {
        auto x_comb = x_combs[x_idx];
        idx = combinationIndex(x_comb, 0, vc);
        auto curMin = solution_costs + idx;

        // generate all x1 - subsets of x
        // TODO not generate secoind half because of symmetry
        for (size_t bipartition1_comb = x_comb & -x_comb, x1_num = 0; x1_num < size_t(1ULL << (term_size - 1))/*bipartition1_comb != x_comb*/; bipartition1_comb = x_comb & (bipartition1_comb - x_comb), x1_num++)
        {
            // conjugated combination
            size_t bipartition2_comb = x_comb & ~bipartition1_comb;

            auto
                    solution_costs1 = solution_costs + combinationIndex(bipartition1_comb, 0, vc),
                    solution_costs2 = solution_costs + combinationIndex(bipartition2_comb, 0, vc);

            // enumerate all v in V
            for (size_t v = 0; v < vc; v++)
            {
                // T(x1 U w)
                t1_cost = solution_costs1[v].weight;

                // T(x2 U w)
                t2_cost = solution_costs2[v].weight;

                // infinite distance
                if (t1_cost == std::numeric_limits<size_t>::max() ||
                    t2_cost == std::numeric_limits<size_t>::max())
                    continue;

                cost = t1_cost + t2_cost;

                if (cost < curMin[v].weight)
                {
                    // minimization!
                    curMin[v].bipartition1_comb = bipartition1_comb;
                    curMin[v].w = v;
                    curMin[v].weight = cost;
                }
            }
        }

        for (size_t v = 0; v < vc; v++)
        {
            for (size_t u = 0; u < vc; u++)
            {
                auto distances = precomputedPaths[u].distances.data();

                // infinite distance
                if (distances[v] == std::numeric_limits<size_t>::max() ||
                    curMin[v].weight == std::numeric_limits<size_t>::max())
                    continue;
                // P_vw
                cost = distances[v] + curMin[v].weight;

                if (cost < curMin[u].weight)
                {
                    curMin[u].bipartition1_comb = x_comb;
                    curMin[u].weight = cost;
                    curMin[u].w = v;
                }
            }
        }
    }
}

Solution DreyfusWagnerSolver::buildMinSpanTree(void)
{
    if (_task.terminals.size() > 20)
        return {};

    BoostGraph resMinSpanTree;

    auto &m_terminals = _task.terminals;

    if (_task.source == std::numeric_limits<size_t>::max())
        throw std::runtime_error("DW: source is not mentioned");
    std::sort(m_terminals.begin(), m_terminals.end());
    m_terminals.push_back(_task.source);
    // suppose that source is in the end

    // trivial cases
    if (mstTrivialCases(m_terminals, resMinSpanTree))
        return {static_cast<size_t>(resMinSpanTree.cost()), resMinSpanTree};

#ifdef BENCHMARK
    std::chrono::high_resolution_clock clock;
    auto start = clock.now();
    std::cout << "Precomputation of shortest paths started...\n";
#endif

    precomputeDijkstra();

#ifdef BENCHMARK
    auto end = clock.now();
    std::cout << "Precomputing shortest paths time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms\n";
    std::cout << "Build MST started...\n";
    start = clock.now();
#endif

    // main branch
    auto vc = _task.graph.vertexCount();
    size_t m_tsize = m_terminals.size();

    // storage logic:
    // combination | <> - for combination of only terminals
    // combination | v - for other

    //1 << m_tsize === 2 ^ m_tsize:
    // Cn0 + Cn1 + ... + Cnn
    auto combCount = vc * (1ULL << m_tsize);

    solution_costs = new (std::nothrow) SolutionData[10*combCount/9];

    if (solution_costs == nullptr)
        return {0, resMinSpanTree};

    // TODO check maybe it is OK with map
    // TODO maybe rewrite combinationNumber in order to allocate
    // 1 << (m_tsize - 1) memory (really there is used combinations
    // only for |T| - 1


    mst1Terminal(m_terminals, m_tsize, vc);

    size_t z;
    size_t cost;
    size_t idx;
    std::vector<size_t> x_combs;

    // term_size <-> |X|
    // pay attention that we build st(X U v) => 3 - 1, size -1
    for (size_t term_size = 2; term_size <= m_tsize - 1; term_size++)
    {
        x_combs = {};
        //auto term_combinations = combinations(term_size);
        // enumerate all X: |X| == term_size
//        x_combs.reserve(combCount); // TODO reserve C_k^m

        for (unsigned long long x_comb = (1ULL << term_size) - 1;
            x_comb < (1ULL << m_tsize);
            z = x_comb | (x_comb - 1), x_comb = (z + 1) | (((~z & -~z) - 1) >> (__builtin_ctz(uint(x_comb)) + 1)))
        {
            x_combs.push_back(x_comb);
        }

        size_t thread_counter = _task.threadCount;
        size_t per_thread_cnt = (x_combs.size() + thread_counter - 1) / thread_counter;
        std::vector<std::thread> minimizer_threads(thread_counter);

        for (size_t i = 0; i < thread_counter; i++)
            minimizer_threads[i] = std::thread(
                        &DreyfusWagnerSolver::minimizerPerComb,
                        this, x_combs.data(), term_size,
                        per_thread_cnt * i, std::min(per_thread_cnt * (i + 1), x_combs.size()), vc);

        for (auto &thr: minimizer_threads)
            thr.join();
    }

    // solution only for m_terminals
    size_t term_comb = (1ULL << (m_tsize - 1)) - 1;

    idx = combinationIndex(term_comb, _task.graph.getBoostVByOrigV(m_terminals.back()), vc);
    resMinSpanTree = mstFull(vc, m_tsize, _task.graph.getBoostVByOrigV(m_terminals.back()));

    cost = solution_costs[idx].weight * (!_deleteBackEdges + 1);
    delete []solution_costs;

    if (_deleteBackEdges)
        GraphUtil::removeBackEdges(resMinSpanTree, _task.source);

#ifdef BENCHMARK
    end = clock.now();
    std::cout << "Build MST time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms\n";
#endif
    return {cost, resMinSpanTree};
}

BoostGraph DreyfusWagnerSolver::mstFull(size_t vc, size_t m_tsize, size_t terminalBack) const
{
    /*
    struct BacktrackData
    {
        BoostGraph mst;
        BacktrackData *t1, *t2;

        uint bipartition;
        uint dummy;

        BacktrackData( uint costIdx = maxUint ) : bipartition(costIdx) {}

        // TODO destructor
    };

    // solutions size 2 (1 terminal)
    std::stack<BacktrackData *> stack;
    BoostGraph mst, p_vw, u;

    stack.push(new BacktrackData(solutionIdx));

    uint bipartition1_comb;
    while (!stack.empty())
    {
        auto& top = stack.top();

        // no children => 1 terminal & vertex case
        if (top->t1 == nullptr)
        {
            assert(top->t2 == nullptr);
            bipartition1_comb = solution_costs[top->bipartition].bipartition1_comb;

            top->t1 = new BacktrackData(solution_costs[top->bipartition].bipartition1_comb);

            if (bipartition1_comb != maxUint)
            {
                stack.push(top->t1);
            }
            else // 2 terminal case
            {
                top->t1->mst = mst2VertByIdx(terminals, top->bipartition, vc);
                stack.pop();
            }
        }
        else if (top->t1 != nullptr && top->t2 == nullptr)
        {
            auto bipartition2_comb = top->bipartition & ~solution_costs[top->bipartition].bipartition1_comb;
            top->t2 = new BacktrackData(bipartition2_comb);
            if (bipartition2_comb != maxUint)
            {
                stack.push(top->t2);
            }
            else // 2 terminal case
            {
                top->t2->mst = mst2VertByIdx(terminals, top->bipartition, vc);
                stack.pop();
            }
        }
        else // both evaluated
        {
            uint
                    v = top->bipartition % vc,
                    w = solution_costs[top->bipartition].w;

            buildMinSpanTree2Vert(v, w, p_vw);
            assert(BoostGraph::unifyWithOneCommonVertex(top->t1->mst, top->t2->mst, w, u));
            assert(BoostGraph::unifyWithOneCommonVertex(p_vw, u, w, mst));
            stack.pop();
        }
    }
    */

    BoostGraph mst;
    mstFullRec((1ULL << (m_tsize - 1)) - 1, vc, m_tsize, terminalBack, mst);

    return mst;
}

// v - BOOST number
void DreyfusWagnerSolver::mstFullRec(size_t x_comb, size_t vc, size_t m_tsize, size_t v, BoostGraph &mst) const
{
    if (x_comb == 0 || v == maxUint)
        return;

    auto idx = combinationIndex(x_comb, v, vc);
    auto u = solution_costs[idx].w;

    if (v != u)
    {
        BoostGraph p_vu, oldmst = std::move(mst);
        buildMinSpanTree2Vert(_task.graph[v], _task.graph[u], p_vu);
        BoostGraph::merge(p_vu, oldmst, mst);
        mstFullRec(solution_costs[idx].bipartition1_comb, vc, m_tsize, u, mst);
    }
    else
    {
        size_t
                x1_comb = solution_costs[idx].bipartition1_comb,
                x2_comb = x_comb & ~x1_comb;

        if (x_comb == x1_comb)
            return;

        mstFullRec(x1_comb, vc, m_tsize, u, mst);
        mstFullRec(x2_comb, vc, m_tsize, u, mst);
    }
}

bool DreyfusWagnerSolver::mstTrivialCases( std::vector<size_t> const &m_terminals, BoostGraph &resMinSpanTree )
{
    auto m_tsize = m_terminals.size();

    // trivial cases
    if (m_tsize == 1)
    {
        buildMinSpanTree1Vert(m_terminals[0], resMinSpanTree);
        return true;
    }

    if (m_tsize == 2)
    {
        // TODO check digraph when build paths
        buildMinSpanTree2Vert(m_terminals[1], m_terminals[0], resMinSpanTree);
        return true;
    }
    return false;
}

void DreyfusWagnerSolver::mst1Terminal(
            std::vector<size_t> const &m_terminals,
            size_t m_tsize, size_t vertexCount )
{
    size_t idx;
    // terminals
    for (size_t t = 0; t < m_tsize - 1; t++)
    {
        auto boostT = _task.graph.getBoostVByOrigV(m_terminals[t]);
//        auto distances =
//                precomputedPaths[boostT].distances.data();
        for (size_t v = 0; v < vertexCount; v++)
        {
            idx = combinationIndex(1ULL << t, v, vertexCount);
            solution_costs[idx].weight =
                    precomputedPaths.at(v).distances[boostT];
            solution_costs[idx].w = boostT;
            solution_costs[idx].bipartition1_comb = 1ULL << t;
        }
    }

    // source
    assert(m_terminals.back() == _task.source);
    auto boostT = _task.graph.getBoostVByOrigV(_task.source);
    auto distances =
            precomputedPaths[boostT].distances.data();
    for (size_t v = 0; v < vertexCount; v++)
    {
        idx = combinationIndex(1ULL << (m_tsize - 1), v, vertexCount);
        solution_costs[idx].weight = distances[v];
        solution_costs[idx].w = boostT;
        solution_costs[idx].bipartition1_comb = 1ULL << m_tsize;
    }
}

void DreyfusWagnerSolver::precomputeDijkstra( void )
{
    auto vc = _task.graph.vertexCount();
    precomputedPaths.resize(vc);

    size_t thread_counter = _task.threadCount;
    size_t per_thread_cnt = (vc + thread_counter - 1) / thread_counter;
    std::vector<std::thread> dijkstra_threads(thread_counter);

    for (size_t i = 0; i < thread_counter; i++)
        dijkstra_threads[i] = std::thread(
                    &DreyfusWagnerSolver::dijkstraThread,
                    this, per_thread_cnt * i,
                    std::min(per_thread_cnt * (i + 1), vc));

    for (auto &thr: dijkstra_threads)
        thr.join();
}

void DreyfusWagnerSolver::dijkstraThread( size_t start, size_t end )
{
    // t is boost vertex number
    for (size_t t = start; t < end; t++)
        try {
            precomputedPaths[t] = _task.graph.shortestPaths(_task.graph[t]);
        } catch (std::bad_alloc &)
        {
            // no precomputation if no memory
            precomputedPaths.clear();
            break;
    }
}
