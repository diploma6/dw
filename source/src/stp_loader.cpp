#include <fstream>
#include <cassert>
#include <algorithm>
#include "stp_loader.h"

STPTaskLoader::STPTaskLoader(size_t startIdx) : _startIdx(startIdx) {}

Task STPTaskLoader::load(std::string const& fname) const {
    std::string line;
    size_t u, v, w;
    std::string section;

    Task task;

    size_t
            node_count = 0,
            edge_count = 0,
            term_count = 0,
            arc_count = 0;
    size_t
            edges_read = 0,
            arc_read = 0,
            term_read = 0;

    bool in_section = false;

    std::ifstream in(fname);

    if (!in)
        throw std::runtime_error(("Bad file " + fname).c_str());

    while (in) {
        in >> line;

        auto upper = []( std::string &s ) {
            for (auto &c: s)
                if (c >= 'a' && c <= 'z')
                    c += 'A' - 'a';
        };

        upper(line);
        if (line == "SECTION") {
            if (in_section)
                throw std::invalid_argument("Nested sections");

            in_section = true;

            in >> section;
            if (section.empty())
                throw std::invalid_argument("Invalid section line");

            upper(section);
            if (section != "COMMENT" &&
                section != "GRAPH" &&
                section != "TERMINALS" &&
                section != "COORDINATES")
                throw std::invalid_argument("Invalid section " + section);
        }
        else if (line == "END") {
            if (!in_section)
                throw std::invalid_argument("No section before end");
            in_section = false;
        }
        else if (line == "NODES") {
            if (!(in >> node_count))
                throw std::invalid_argument("Invalid nodes line");
        }
        else if (line == "EDGES") {
            if (!(in >> edge_count))
                throw std::invalid_argument("Invalid edges line");
        }
        else if (line == "ARCS") {
            if (!(in >> arc_count))
                throw std::invalid_argument("Invalid edges line");
        }
        else if (line == "TERMINALS") {
            if (!(in >> term_count))
                throw std::invalid_argument("Invalid terminals line");
        }
        else if (line == "COORDINATES") {
            continue;
        }
        else if (line == "E") {
            if (!(in >> u >> v >> w))
                throw std::invalid_argument("Invalid edge line: " + line);

            assert(u - _startIdx >= 0 && u - _startIdx < node_count);
            assert(v - _startIdx >= 0 && v - _startIdx < node_count);

            ++edges_read;
            assert(edges_read <= edge_count);

            task.graph.addEdge({u - _startIdx, v - _startIdx, w});
            task.graph.addEdge({v - _startIdx, u - _startIdx, w});
        }
        else if (line == "A") {
            if (!(in >> u >> v >> w))
                throw std::invalid_argument("Invalid arc line: " + line);

            assert(u - _startIdx >= 0 && u - _startIdx < node_count);
            assert(v - _startIdx >= 0 && v - _startIdx < node_count);

            ++arc_read;
            assert(arc_read <= arc_count);

            task.graph.addEdge({u - _startIdx, v - _startIdx, w});
        }
        else if (line == "T") {
            if (!(in >> u))
                throw std::invalid_argument("Invalid terminal line: " + line);

            assert(u - _startIdx >= 0 && u - _startIdx < node_count);
            ++term_read;
            assert(term_read <= term_count);

            task.terminals.push_back(u - _startIdx);
        }
        else if (line == "ROOT") {
            if (!(in >> u))
                throw std::invalid_argument("Invalid terminal line: " + line);

            assert(u - _startIdx >= 0 && u - _startIdx < node_count);
            assert(task.source == std::numeric_limits<size_t>::max());
            task.source = u - _startIdx;
        }
        else if (line == "COST")
        {
            if (!(in >> task.validCost))
                throw std::invalid_argument("Invalid cost line: " + line);
        }
        else
            continue;
    }

    assert(edges_read == edge_count);
    assert(arc_read == arc_count);
    assert(term_read == term_count);

    if (term_count == 0 || (edge_count == 0 && arc_count == 0))
        throw std::invalid_argument("Bad graph file. Possibly, you forgot option `-l`");

    // undirected case
    if (task.source == std::numeric_limits<size_t>::max())
    {
        task.source = task.terminals[0];
        task.terminals.erase(task.terminals.begin());
    }

    std::sort(task.terminals.begin(), task.terminals.end());

    return task;
}
