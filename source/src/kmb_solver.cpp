#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <unordered_set>

#include "kmb_solver.h"


Solution KMBSolver::solve(Task const& task)
{
    _task = task;

    BoostGraph graph1; // graph after first step

    step1(graph1); // first KMB step

    std::vector<BoostGraph::edge_descriptor> MST; // minimal spanning tree for graph1
    boost::kruskal_minimum_spanning_tree(graph1, std::back_inserter(MST),
                                         boost::weight_map(boost::get(&Edge::cost, graph1)));

    BoostGraph graphFromMST;

    makeGraphFromMST(MST, graphFromMST);

    deleteLeaves(graphFromMST);

    auto graph_cost = graphFromMST.cost();

    return {graph_cost, graphFromMST};
}

void KMBSolver::step1(BoostGraph &graph1)
{
    // alias
    auto root = _task.source;

    _dijkstra[root] = _task.graph.shortestPaths(root);

    for (auto &t : _task.terminals)
        _dijkstra[t] = _task.graph.shortestPaths(t);

    auto &root_distances = _dijkstra[root].distances;
    for (auto &t : _task.terminals)
        graph1.addEdge(root, t, root_distances[t]);

    for (size_t i = 0, t_size = _task.terminals.size(); i < t_size; i++)
    {
        auto &term_distances = _dijkstra[_task.terminals[i]].distances;

        graph1.addEdge(_task.terminals[i], root, term_distances[root]);

        for (size_t j = 0; j < t_size; j++)
            if (i != j)
                graph1.addEdge(_task.terminals[i], _task.terminals[j], term_distances[_task.terminals[j]]);
    }
}

void KMBSolver::makeGraphFromMST(std::vector<BoostGraph::edge_descriptor> &minMST, BoostGraph &newGraph) const
{
    BoostGraph path;
    for (auto& e : minMST)
    {
        auto edge = _task.graph[e];
        path = _task.graph.shortestPath(edge.sourceID, edge.targetID, _dijkstra.at(edge.sourceID).predecessors);

        BoostGraph backpath;

        path.applyEdges(
            [&backpath](Edge const& edge)
            {
                backpath.addEdge({edge.targetID, edge.sourceID, edge.cost});
            });

        backpath.applyEdges([&path](Edge const& e){ path.addEdge(e); });

        newGraph.merge(path);
    }
}

void KMBSolver::deleteLeaves(BoostGraph &mst) const
{
    std::vector<BoostGraph::out_edge_iterator> edgesToRemove;

    BoostGraph::vertex_iterator vertex, vertex_end;
    for (boost::tie(vertex, vertex_end) = boost::vertices(mst); vertex != vertex_end; ++vertex)
    {
        auto out_edges = boost::out_edges(*vertex, mst);
        auto out_edges_count = std::distance(out_edges.first, out_edges.second);

        // ORIGINAL vertex
        auto curVertex = mst[*vertex];

        assert(out_edges_count > 0);
        if (out_edges_count == 1 &&
                std::find(_task.terminals.begin(), _task.terminals.end(), curVertex) == _task.terminals.end() &&
                _task.source != curVertex)
        {
            size_t vertexId;
            bool foundTerminal = false;

            do
            {
                vertexId = mst[*(out_edges.first)].targetID;
                auto l_out_edges = mst.outEdges(vertexId);

                for (; l_out_edges.first != l_out_edges.second; l_out_edges.first++)
                {
                    vertexId = mst[*(l_out_edges.first)].targetID;

                    edgesToRemove.push_back(l_out_edges.first);

                    auto foundNow = std::find(_task.terminals.begin(), _task.terminals.end(), vertexId) != _task.terminals.end();
                    foundTerminal = std::max(foundTerminal, foundNow);

                    // edge from terminal to the leave direction
                    if (foundNow)
                        mst.removeEdge(mst[*(l_out_edges.first)].targetID, mst[*(l_out_edges.first)].sourceID);
                }
            } while (!foundTerminal);

            for (auto &e: edgesToRemove)
                boost::remove_edge(e, mst);
        }
    }
}
