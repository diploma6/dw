import sys
import matplotlib.pyplot as plt
import csv

x = []
y = []
with open(sys.argv[1]) as f:
	reader = csv.DictReader(f, delimiter=';')
	i = 0
	for line in reader:
		x.append(i)
		i += 1
		y.append(float(line[sys.argv[2]]))

	y.sort()
	plt.semilogy(x, y)

	if len(sys.argv) >= 4:
		plt.title(sys.argv[4])

	plt.savefig(sys.argv[3], dpi=150)

