#include <boost/test/unit_test.hpp>
#include "../src/common.h"
#include "../src/stp_loader.h"

BOOST_AUTO_TEST_SUITE(stp_loader)
 
    BOOST_AUTO_TEST_CASE(should_throw)
    {
        STPTaskLoader loader(0);
        BOOST_CHECK_THROW(loader.load("incorrect_file"), std::runtime_error);
    }

BOOST_AUTO_TEST_SUITE_END()
