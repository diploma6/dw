#include <boost/test/unit_test.hpp>
#include "../src/boost_graph.h"

BOOST_AUTO_TEST_SUITE(boost_graph)
 
    BOOST_AUTO_TEST_CASE(remove_nonexistent_vertex)
    {
        BoostGraph g;
        BOOST_CHECK_THROW(g.removeVertex(1), std::out_of_range);
    }

    BOOST_AUTO_TEST_CASE(remove_vertex)
    {
        BoostGraph g;
        Vertex vertexToRemove = 1;
        g.addVertex(vertexToRemove);
        g.addVertex(2);

        BOOST_TEST_CHECKPOINT("Graph vertices = { 1, 2 }");
        BOOST_CHECK_EQUAL(g.vertexCount(), 2);

        BOOST_CHECK_NO_THROW(g.removeVertex(vertexToRemove));
        BOOST_TEST_CHECKPOINT("Graph vertices = { 2 }");
        BOOST_CHECK_EQUAL(g.vertexCount(), 1);
        BOOST_CHECK_THROW(g.removeVertex(vertexToRemove), std::out_of_range);
    }

    BOOST_AUTO_TEST_CASE(find_edge_exists)
    {
        BoostGraph g;
        g.addEdge(1, 3, 5);
        g.addEdge(2, 4, 6);
        g.addEdge(1, 2, 2);
        g.addEdge(7, 3, 8);

        BOOST_CHECK_EQUAL(g.contains({1, 3, 5}), true);
        BOOST_CHECK_EQUAL(g.contains({2, 4, 10}, true), true);
    }

    BOOST_AUTO_TEST_CASE(find_edge_not_exists)
    {
        BoostGraph g;
        g.addEdge(1, 3, 5);
        g.addEdge(2, 4, 6);
        g.addEdge(1, 2, 2);
        g.addEdge(7, 3, 8);

        BOOST_CHECK_EQUAL(g.contains({2, 4, 10}), false);
        BOOST_CHECK_EQUAL(g.contains({1, 3, 10}, true), false);
        BOOST_CHECK_EQUAL(g.contains({3, 7, 8}, false), false);
    }

BOOST_AUTO_TEST_SUITE_END()
