#include <boost/test/unit_test.hpp>
#include "../src/boost_graph.h"
#include "../src/test_utils.h"

BOOST_AUTO_TEST_SUITE(test_utils)
 
    BOOST_AUTO_TEST_CASE(is_connected_correct)
    {
        BoostGraph g;
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        BOOST_CHECK_EQUAL(GraphTestUtil::isTreeConnected(g, 1), true);
    }

    BOOST_AUTO_TEST_CASE(is_acyclic_correct)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        BOOST_CHECK_EQUAL(GraphTestUtil::isAcyclic(g), true);
    }

    BOOST_AUTO_TEST_CASE(is_acyclic_incorrect1)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 1, 1);
        BOOST_CHECK_EQUAL(GraphTestUtil::isAcyclic(g), false);
    }

    BOOST_AUTO_TEST_CASE(is_acyclic_incorrect2)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        g.addEdge(5, 4, 1);
        BOOST_CHECK_EQUAL(GraphTestUtil::isAcyclic(g), false);
    }

    BOOST_AUTO_TEST_CASE(is_connected_incorrect)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        g.addEdge(6, 3, 1);
        for (size_t i = 1; i <= 6; i++)
            BOOST_CHECK_EQUAL(GraphTestUtil::isTreeConnected(g, i), false);
    }

    BOOST_AUTO_TEST_CASE(are_leaves_terminals_correct)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        std::vector<size_t> terminals = { 1, 3, 4, 5 };
        BOOST_CHECK_EQUAL(GraphTestUtil::isTree(g, 1), true);
        BOOST_CHECK_EQUAL(GraphTestUtil::areLeavesTerminals(g, terminals), true);
    }

    BOOST_AUTO_TEST_CASE(are_leaves_terminals_incorrect)
    {
        BoostGraph g;
        g.addEdge(1, 2, 1);
        g.addEdge(2, 3, 1);
        g.addEdge(3, 4, 1);
        g.addEdge(3, 5, 1);
        std::vector<size_t> terminals = { 1, 3, 4 };
        BOOST_CHECK_EQUAL(GraphTestUtil::isTree(g, 1), true);
        BOOST_CHECK_EQUAL(GraphTestUtil::areLeavesTerminals(g, terminals), false);
    }

BOOST_AUTO_TEST_SUITE_END()
