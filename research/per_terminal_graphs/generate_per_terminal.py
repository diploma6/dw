import argparse
import sys

class ScriptArg:
    def __init__(self, argv):
        self.parser = argparse.ArgumentParser()
        self.get_command_prompt(argv)


    def get_command_prompt(self, argv):
        self.parser.add_argument("-t", "--terminals", help="terminals count to remove", type=str)
        self.parser.add_argument("-i", "--input", help="input STP file address", type=str)
        self.parser.add_argument("-o", "--output", help="output STP file address", type=str)
        self.args = self.parser.parse_args()
                        
        
def check_in_terminals(in_terminals, file_string):
    words = file_string.split(' ')
    if len(words) == 2:
        if words[0].lower() == 'section' and \
            words[1].lower() == 'terminals\n':
            return True
            
    if len(words) == 1:
        if words[0].lower() == 'end\n' and in_terminals:
            return False
    
    return in_terminals
    
    
def find_terminals_count(s):
    words = s.split()
    
    if len(words) == 2:
        if words[0].lower() == 'terminals':
            return True, int(words[1])
            
    return False, 0
    

def is_terminal_string(s):
    words = s.split(' ')
    if len(words) == 2:
        return words[0].lower() == "t"
    return False
            
       
def remove_terminals(infile, outfile, need_rm_cnt):
    try:
        ifile = open(infile, "r")
        ofile = open(outfile, "w")
    except FileNotFoundError:
        print(f"File {infile} not found")
        return
        
    in_terminals = False
    terminal_cnt = -1
    removed_cnt = 0
    
    for s in ifile:
        in_terminals = check_in_terminals(in_terminals, s)
        
        if not in_terminals:
            ofile.write(s)
        else:
            if terminal_cnt == -1:
                # find terminals count
                found, cnt = find_terminals_count(s)
                if found:
                    if need_rm_cnt >= cnt:
                        raise RuntimeError("Too many terminals needed to be removed")
                    terminal_cnt = cnt
                    ofile.write(f"Terminals {terminal_cnt - need_rm_cnt}\n")
                else:
                    ofile.write(s)
            else:
                # already found terminals count
                if is_terminal_string(s):
                    if removed_cnt >= need_rm_cnt:
                        ofile.write(s)
                    else:
                        removed_cnt += 1
                
    ifile.close()
    ofile.close()


if __name__ == '__main__':
    parser = ScriptArg(sys.argv)
    try:
        remove_terminals(parser.args.input, parser.args.output, int(parser.args.terminals))
    except RuntimeError as e:
        print("Exception thrown:", e)

