t=10
v=5000
e=60000
d="false"
p="directed/testset1"

while [ -n "$1" ]
do
case "$1" in
-v) v=$2; shift ;;
-e) e=$2; shift ;;
-t) t=$2; shift ;;
-p) p=$2; shift ;;
-d) d=$2; shift ;;
*) echo "$1 is not valid option" ;;
esac
shift
done

echo $v vertices\; $e edges\; $t terminals\; directed: $d\; path to generate graphs: $p

../../gena/Generator -v $v --edges $e -t $t --exactSolution true -d $d -p 60 --vBackbone $((v-100)) --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5

for ((i=1; i <= $((t - 3)); i++))
do
        if [ $d = "false" ]; then
		suffix="_Undirection_AllBlocks.stp"
	else
		suffix="_Percent60_Direction_AllBlocks.stp"
	fi

       	python generate_per_terminal.py -i graph_${v}_${e}_${t}_Scenario1${suffix} -o ${p}/graph_${v}_${e}_$((t - i))_Scenario1${suffix} -t $i
	echo $i removed terminals passed!
done
