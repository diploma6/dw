#Undirected
../../gena/Generator -v 5000 --edges 60000 -t 12 --exactSolution true -d false --vBackbone 4900 --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5
echo 5000/60000/12/undirected passed!

../../gena/Generator -v 20000 --edges 60000 -t 8 --exactSolution true -d false --vBackbone 19900 --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5
echo 20000/60000/8/undirected passed!

#Directed
../../gena/Generator -v 5000 --edges 60000 -t 12 --exactSolution true -d true -p 60 --vBackbone 4900 --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5
echo 5000/60000/12/directed passed!

../../gena/Generator -v 20000 --edges 60000 -t 8 --exactSolution true -d true -p 60 --vBackbone 19900 --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5
echo 20000/60000/8/directed passed!
