from graphviz import Digraph
import networkx as nx
from os.path import splitext
from argparse import ArgumentParser
from typing import List, Tuple

class Drawable:
    """Drawable interface.

    Could specify some styles for graphviz."""
    def draw(self, dot: Digraph):
        raise NotImplementedError()


class Node(Drawable):
    """Graph node"""
    def __init__(self, i):
        self.id = i

    def draw(self, dot):
        dot.node(self.id)

    def __repr__(self):
        return "v" + str(self.id)

class Terminal(Node):
    """Terminal node"""
    def __init__(self, i):
        super().__init__(i)
    
    def draw(self, dot):
        dot.node(self.id, fillcolor='#FF00FF', style='filled')

class Source(Node):
    """Source node"""
    def __init__(self, i):
        super().__init__(i)
    
    def draw(self, dot):
        dot.node(self.id, fillcolor='#0000FF', style='filled')

class Edge(Drawable):
    """Edge"""
    def __init__(self, u, v, cost, directed=True):
        self.src = u
        self.dst = v
        self.cost = cost
        self.directed = directed

    def draw(self, dot):
        other = dict()
        if not self.directed:
            other["dir"] = 'none'
        dot.edge(self.src, self.dst, label=self.cost, **other)

def parse_stp(fname: str, directed: bool) -> Tuple[List[Node], List[Edge]]:
    """Parse STP file into nodes and edges.

    @param fname Input STP file name.
    @param directed Is graph directed.
    """
    nodes, edges = [], []
    with open(fname, 'r') as inf:
        for line in inf.readlines():
            if len(line) < 2:
                continue
            if line[:2] == 'E ':
                # E <u> <v> <cost>
                _, u, v, cost = line.split()
                edges.append(Edge(u, v, cost, directed=False))
            elif line[:2] == 'A ':
                # E <u> <v> <cost>
                _, u, v, cost = line.split()
                edges.append(Edge(u, v, cost, directed=True))
            elif line[:2] == 'T ':
                _, i = line.split()
                nodes.append(Terminal(i.strip()))
            elif line[:5] == 'Root ':
                _, i = line.split()
                nodes.append(Source(i.strip()))

    return nodes, edges


def draw_graph(nodes: List[Node], edges: List[Edge], fname: str):
    """Draw graph to file.

    @param fname Output file name.
    """
    name, ext = splitext(fname)
    dot = Digraph(format=ext.lstrip("."))

    for node in nodes:
        node.draw(dot)

    for edge in edges:
        edge.draw(dot)

    dot.render(name)

def print_stats(nodes: List[Node], edges: List[Edge]):
    """Print graph statistics.
    """
    print("Nodes:")
    sources = []
    terminals = []
    misc = set()
    for node in nodes:
        if type(node) is Source:
            sources.append(node.id)
        elif type(node) is Terminal:
            terminals.append(node.id)
        else:
            misc.append(node.id)
    
    for edge in edges:
        misc.add(edge.src)
        misc.add(edge.dst)
    misc.difference_update(set(sources))
    misc.difference_update(set(terminals))
    print("  Sources = ", sources)
    print("  Terminals(", len(terminals), ") = ", terminals)
    print("  #Other = ", len(misc))

    print("#Edges = ", len(edges))
    directed = []
    undirected = []
    for edge in edges:
        if edge.directed:
            directed.append(edge)
        else:
            undirected.append(edge)
    print("  #Directed = ", len(directed))
    print("  #Undirected = ", len(undirected))

def compute_paths(nodes: List[Node], edges: List[Edge]):
    """Compute shortest paths.
    """
    g = nx.DiGraph()
    # build a graph
    for edge in edges:
        g.add_edge(edge.src, edge.dst, weight=int(edge.cost))
        if not edge.directed:
            g.add_edge(edge.dst, edge.src, weight=int(edge.cost))
    # extract source
    source = None
    for node in nodes:
        if type(node) is Source:
            source = node.id
            break
    else:
        print("source is not found")
    # find all shortest paths
    print("Shortest paths:")
    for node in nodes:
        if type(node) is not Terminal:
            continue
        try:
            path = nx.shortest_path(g, source, node.id)
            print(source, "->", node.id, ": len = ", len(path))
        except Exception as ex:
            print(f"Can't compute path between {source} and {node.id}: ({ex})")


if __name__ == "__main__":
    arg_parser = ArgumentParser()
    arg_parser.add_argument("input", type=str, help="Input STP file")
    arg_parser.add_argument("-o", "--output", type=str, default="out.png", help="Output image file")
    arg_parser.add_argument("-d", "--directed", default=False, action='store_true', help="Graph is directed")
    arg_parser.add_argument("-v", "--vertices", default=None, type=int, help="Vertices number limit")
    arg_parser.add_argument("-e", "--edges", default=None, type=int, help="Edges number limit")
    arg_parser.add_argument("-p", "--paths", default=False, action='store_true', help="Compute shortest paths")
    args = arg_parser.parse_args()
    # TODO: add another types of vertices and more swag (https://graphviz.org/doc/info/attrs.html)
    nodes, edges = parse_stp(args.input, args.directed)

    if args.paths:
        print_stats(nodes, edges)
        compute_paths(nodes, edges)
        exit(0)

    if args.vertices is not None:
        nodes = nodes[:args.vertices]
    if args.edges is not None:
        edges = edges[:args.edges]
    draw_graph(nodes, edges, fname=args.output)

