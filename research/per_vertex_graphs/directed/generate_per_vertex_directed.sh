for ((i = 500; i <= 8000; i += 500))
do
	# Directed
	../../../gena/Generator -v $i --edges 60000 -t 10 --exactSolution true -d true -p 60 --vBackbone $((i - 100)) --vMetro 30 --vCpe 70 --vMetroAve 5 --vCpeAve 5
	timedatectl

	echo $i vertices passed!
done

