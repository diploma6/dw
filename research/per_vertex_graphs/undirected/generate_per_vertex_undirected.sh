for ((i = 500; i <= 8000; i += 500))
do
	# Undirected
	../../../gena/Generator -v $i --edges 60000 -t 10 --exactSolution true -d false --vBackbone $((i - 100)) --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5
	timedatectl

	echo $i vertices passed!
done

