# /usr/bin/python
# arguments: start, end, count

import sys
import numpy as np

start = int(sys.argv[1])
end = int(sys.argv[2])
count = int(sys.argv[3])

for i in np.linspace(start, end, count, dtype=int):
	print(i, end=' ')
print('\n')

