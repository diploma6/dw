import sys

flname = sys.argv[1]

fl = open(flname)

for fname in fl:
	print(fname)
	if 'Percent60' in fname:
		params = fname.split('_')
		outfname = '_'.join(params[0:5]) + '_Undirection_' + params[-1][:-1]
		f = open(fname[:-1])
		outf = open(outfname, 'w')

		edges = 0
		for s in f:
			words = s.split(' ')
			if words[0] == 'Edges':
				edges = int(words[1])
			elif words[0] == 'Arcs':
				edges += int(words[1])
				outf.write(f'Edges {edges}\n')
				print(f"EDGESF{edges}")
			elif words[0] == 'A':
				outf.write('E ' + ' '.join(words[1:]))
			else:
				outf.write(s)

