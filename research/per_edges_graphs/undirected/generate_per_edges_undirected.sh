str=`python ../uniform_grid.py 5000 500000 20

echo $str

for i in $str
do
	# Undirected
	../../../gena/Generator -v 2000 --edges $i -t 10 --exactSolution true -d false --vBackbone 1900 --vMetro 30 --vMetroAve 5 --vCpe 70 --vCpeAve 5

	timedatectl
	echo $i edges passed!
done
