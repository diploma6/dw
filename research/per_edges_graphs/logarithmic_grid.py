# /usr/bin/python
# arguments: start, end, count

import sys

start = int(sys.argv[1])
end = int(sys.argv[2])
count = int(sys.argv[3])

for i in range(0, count):
	print(int(start ** (1 - i / (count - 1)) * end ** (i / (count - 1))), end=' ')
print('\n')

