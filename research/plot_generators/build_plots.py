import matplotlib.pyplot as plt
import argparse
import csv
import numpy as np
from numpy.polynomial import Polynomial as nppoly
from numpy.polynomial.polynomial import polyval
 
class ScriptArg:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.get_command_prompt()
 
 
    def get_command_prompt(self):
        self.parser.add_argument("-f", help="Research result file names. Separate with |. Separate subplots with @", type=str)
        self.parser.add_argument("-y", help="Ordinate axis param name: v, e, term, thr, time. "\
                                            "Can be many axes. Separate with |. Separate subplots with @", type=str)
        self.parser.add_argument("-x", help="Absciss axis param name: v, e, term, thr, time. Separate subplots with @", type=str)
        self.parser.add_argument("-t", "--title", help="Plot title. Separate subplots with @", type=str)
        self.parser.add_argument("-o", "--output", help="Output PNG file name", type=str)
        self.parser.add_argument("--color", help="Color list. Separate per-file colors with /. Per plot -- with |. Result and approximation -- wtih ,. Separate subplots with @")
        self.parser.add_argument("--legend", help="Legend. Separate with |", type=str)
        self.parser.add_argument("--logx", help="If true, absciss axis has logarithmic scale", action="store_true")
        self.parser.add_argument("--logy", help="If true, ordinate axis has logarithmic scale", action="store_true")
        self.parser.add_argument("--fit-polynom", help="Degree of polynom to approximate function with. Separate subplots with @", type=str)
        self.parser.add_argument("--fit-exp", help="Exponential approximation", action="store_true")
        self.parser.add_argument("--subplots", help="Subplot count. Currently support 1 or 2", type=int)
        self.parser.add_argument("--suptitle", help="Common title for all subplots", type=str)
        self.parser.add_argument("--fullscreen", help="Enable fullscreen", action="store_true")
        
        self.args = self.parser.parse_args()


class ResultProcessor:
    def __init__(self, arg_parser):
        self.__args = arg_parser.args
        results_all = []

        for fname_subplot in arg_parser.args.f.split('@'):
            results = []
            for fname in fname_subplot.split('|'):
                f = open(fname)
                reader = csv.DictReader(f, delimiter=';')
                results.append(self.get_results(reader))
            results_all.append(results)

        self.plot_results(results_all)


    def get_results(self, reader):
        """
        Results getter.
        returns:
        """
        results = {'v': [], 'e': [], 'term': [], 'thr': [], 'time': []}
        for line in reader:
            filename = line["file"];
            path = filename.split('/')
            graph_params = path[-1].split('_')
            results['v'].append(int(graph_params[1]))
            results['e'].append(int(graph_params[2]))
            results['term'].append(int(graph_params[3]))
            results['thr'].append(int(line["threads"]))
            results['time'].append(float(line["median_time"]))
        
        return results


    def plot_fit(self, plotter, datax, datay, clr, degree):
        fit_params = nppoly.fit(datax, datay, deg=degree, domain=[datax[0], datax[-1]])
        poly_coeffs = fit_params.convert().coef
        fitx = np.linspace(datax[0], datax[-1], num=10)
        fity = polyval(fitx, poly_coeffs)
        if len(clr) == 1:
            plotter(fitx, fity, '--', color=clr[0])
        else:
            plotter(fitx, fity if self.__args.fit_exp is False else np.exp(fity), '--', color=clr[1], linewidth=1.9)
        return poly_coeffs
       

    def calc_r2(self, datax, datay, rcvy):
        ss_res = np.dot(datay - rcvy, datay - rcvy)
        ymean = np.mean(datay)
        ss_tot = np.dot(datay - ymean, datay - ymean)
        return 1 - ss_res / ss_tot
 
  
    def plot_one_line(self, resultsx, resultsy, plotter, clr, fit_polynom, fit_exp):
         data = [(x, y) for x, y in zip(resultsx, resultsy)]
         data.sort(key = lambda x: x[0])

         datax = np.asarray([x[0] for x in data])
         datay = np.asarray([x[1] for x in data])

         plotter(datax, datay, color=clr[0])

         if fit_polynom != -1:
             poly_coeffs = self.plot_fit(plotter, datax, datay, clr, fit_polynom)
             return self.calc_r2(datax, datay, polyval(datax, poly_coeffs))
         elif fit_exp is True:
             poly_coeffs = self.plot_fit(plotter, datax, np.log(datay), clr, 1)
             return self.calc_r2(datax, datay, np.exp(polyval(datax, poly_coeffs)))
             
         return None

    
    def plot_results(self, resultslst):
        if self.__args.x is None or self.__args.y is None:
            print('Plots can not be drawn without X or Y name')
            return

        if self.__args.subplots is None:
            self.__args.subplots = 1

        figure, axes = plt.subplots(nrows=1, ncols=self.__args.subplots)

        
        for idx, title, labelx, labely, colors, legendlst, results, fit_polynom in\
            zip(range(0, self.__args.subplots), self.__args.title.split('@'), self.__args.x.split('@'),
                        self.__args.y.split('@'), self.__args.color.split('@'), self.__args.legend.split('@'), resultslst,
                        self.__args.fit_polynom.split('@') if self.__args.fit_polynom is not None else [-1] * self.__args.subplots):
            ax = axes[idx] if self.__args.subplots != 1 else axes
            ax.set_title(title, fontsize=19+3-1)
            labels = {'v': "Число вершин", 'e': "Число рёбер", 
                  'term': "Число терминалов",
                  'thr': "Число потоков", 'time': "Время работы, с"}
            ax.set_xlabel(labels[labelx], fontsize=13)
            y_axes = labely.split('|')
            if len(y_axes) == 1:
                ax.set_ylabel(labels[y_axes[0]], fontsize=13)

            legend = legendlst.split('|')

            plotter = ax.plot if not self.__args.logy and not self.__args.logx\
                          else ax.semilogy if self.__args.logy and not self.__args.logx\
                          else ax.semilogx if self.__args.logx and not self.__args.logy\
                          else ax.loglog

            ax.tick_params(axis='x', labelsize=13)
            ax.tick_params(axis='y', labelsize=13)

            idx = 1

            for res, file_color in zip(results, colors.split('/')):
                for y, color in zip(y_axes, file_color.split('|')):
                    r2 = self.plot_one_line(res[labelx], res[y], plotter, color.split(','), int(fit_polynom), self.__args.fit_exp)
                    if r2 is not None:
                        legend[idx] += f'. $R^2 = {r2:.3f}$'
                    idx += 2
              
            ax.legend(legend, fontsize=11)

        figure.suptitle(self.__args.suptitle if self.__args.suptitle is not None else '', fontsize=20)

        if self.__args.fullscreen:
            mng = plt.get_current_fig_manager()
            mng.window.showMaximized()

        plt.show()
        figure.savefig(self.__args.output if self.__args.output is not None else f'research{self.__args.x}_{self.__args.y}.png', dpi=300)


if __name__ == '__main__':
    arg_parser = ScriptArg()
    res_processor = ResultProcessor(arg_parser)

