python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_first_DW.txt@../results/per_thread/per_thread_graphs_directed_first_EMV.txt" -x thr@thr -y time@time -o ../plots/per_thread_graph_directed_DW_EMV_first_plot.png --legend "ДВ@ЭМВ" --color red,green@blue,orange -t "ДВ@ЭМВ" --subplots 2 --fullscreen

python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_second_DW.txt@../results/per_thread/per_thread_graphs_directed_second_EMV.txt" -x thr@thr -y time@time -o ../plots/per_thread_graph_directed_DW_EMV_second_plot.png --legend "ДВ@ЭМВ" --color red,green@blue,orange -t "ДВ@ЭМВ" --subplots 2 --fullscreen

python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_first_DW.txt" -x thr -y time -o ../plots/per_thread_graph_directed_DW_first_plot.png --legend "ДВ" --color red,green -t "ДВ"

python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_first_EMV.txt" -x thr -y time -o ../plots/per_thread_graph_directed_EMV_first_plot.png --legend "ЭМВ" --color blue,orange -t "ЭМВ"

python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_second_DW.txt" -x thr -y time -o ../plots/per_thread_graph_directed_DW_second_plot.png --legend "ДВ" --color red,green -t "ДВ"

python build_plots.py -f "../results/per_thread/per_thread_graphs_directed_second_EMV.txt" -x thr -y time -o ../plots/per_thread_graph_directed_EMV_second_plot.png --legend "ЭМВ" --color blue,orange -t "ЭМВ"


