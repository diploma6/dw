cd per_vertex_graphs/undirected
./generate_per_vertex_undirected.sh

cd ../directed
./generate_per_vertex_directed.sh

cd ../../per_edges_graphs
cd undirected
./generate_per_edges_undirected.sh

cd ../directed
./generate_per_edges_directed.sh

cd ../../per_terminal_graphs
./generate_per_terminal.sh
