#pragma once

#include "boost_graph.h"
#include "common.h"
#include <vector>

using namespace std;

/*!
 * \brief Solves task defined in SPT by Dreyfus-Wagner algorithm.
 */
class DijkstraSolver
{
public:

    /*!
     * \brief Solves task.
     * \param task Task (problem) to solve.
     * \return Problem solution.
     */
    Solution solve(Task const& task);
};