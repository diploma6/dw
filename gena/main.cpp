#include "graph_generator.h"
#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

int main(int argc, char const* argv[])
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "produce help message")
        ("scenario,s", po::value<std::size_t>()->default_value(1), "number scanerio: 1-6")
        ("index,i", po::value<size_t>()->default_value(0), "start index")
        ("vertex,v", po::value<size_t>()->default_value(220099), "number vertices in graph")
        ("terminals,t", po::value<size_t>()->default_value(300), "number terminals in graph")
        ("exactSolution", po::value<bool>()->default_value(false), "need of exact solution for task")
        ("direction,d", po::value<bool>()->default_value(false), "direction graph")
        ("percent,p", po::value<size_t>()->default_value(0), "percent of one-direction edges")
        ("allBlocks,a", po::value<bool>()->default_value(true), "generate terminals in all blocks")
        ("edges", po::value<size_t>()->default_value(1000000), "number edges in graph")
        ("vBackbone", po::value<size_t>()->default_value(1507), "number vertex backbone")
        ("vMetro", po::value<size_t>()->default_value(1507), "number vertex metro")
        ("vMetroAve", po::value<size_t>()->default_value(74), "average number vertex metro")
        ("vCpe", po::value<size_t>()->default_value(217085), "number vertex cpe")
        ("vCpeAve", po::value<size_t>()->default_value(197), "average number vertex cpe");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << endl;
        return 0;
    }

	GraphGenerator graph(vm);
	graph.generateGraph();

	return 0;
}