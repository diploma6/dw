#pragma once

#include "common.h"

//! Steiner tree with delay constraint exact solver
class DelayExactSolver
{
public:
    /*!
     * \brief Solves task.
     * \param task Task (problem) to solve.
     * \return Problem solution.
     */
    Solution solve(Task const& task);

protected:
    using PathsVector = std::vector<BoostGraph::BoostPath>;

    size_t _source;
    const std::vector<size_t>* _terminals;
    BoostGraph _reducedGraph;
    long long _delayLimit;

    std::vector<PathsVector> _terminalPaths;
    BoostGraph::BoostPath _curMinTree;

    size_t _curCombination = 0;

    const static size_t MAX_COMBINATIONS;

    using DistanceMatrix = std::map<BoostGraph::vertex_descriptor,
        std::map<BoostGraph::vertex_descriptor, long long>>;

    struct TooManyCombinationsException : public std::exception
    {
    };

    /*!
     * \brief Initialize internal variables function.
     * \details Must be called in solve before any other methods.
     * \param task Task to initialize with.
     */
    void initInternal(const Task& task);

    /*!
     * \brief Remove redundant vertices and edges function.
     * \param graph Graph to reduce.
     */
    void reduceGraph(const BoostGraph& graph);

    /*!
     * \brief Find all paths on reduced graph from source to terminals.
     * \return Vectors of every path for each termianl.
     */
    std::vector<PathsVector> findEveryPath() const;

    /*!
     * \brief Search for minimum cost spanning tree function.
     */
    void findMinCostTree();

    /*!
     * \brief Recursively try every possible path merging combination function.
     * \param curTree Tree formed by paths to previous terminals.
     * \param curTerminalIndex Index of current terminal to try paths to.
     */
    void tryCombination(const BoostGraph::BoostPath& curTree,
        size_t curTerminalIndex);
};
