#include "graph_generator.h"
#include <functional>
#include <numeric>

#define MIN_WEIGHT_CITY 10
#define MAX_WEIGHT_CITY 1000

#define MIN_WEIGHT_BACKBONE 100
#define MAX_WEIGHT_BACKBONE 10000

#define MIN_DELAY_CITY 0
#define MAX_DELAY_CITY 1000

#define MIN_DELAY_BACKBONE 1000
#define MAX_DELAY_BACKBONE 100000

GraphGenerator::GraphGenerator(po::variables_map vm)
{
	srand((unsigned)time(0));

	numberVertices = vm.at("vertex").as<size_t>();
	numberTerminals = vm.at("terminals").as<size_t>();

	scenario = vm.at("scenario").as<size_t>();
	scenarios = parserScenarios(scenario);
	percentOneDirectionEdges = vm.at("percent").as<size_t>();
	exactSolution = vm.at("exactSolution").as<bool>();
	index = vm.at("index").as<size_t>();

	direction = vm.at("direction").as<bool>();
	typeTerminals = vm.at("allBlocks").as<bool>();

	numberEdges = vm.at("edges").as<size_t>();
	numV_backbone = vm.at("vBackbone").as<size_t>();
	numV_metro = vm.at("vMetro").as<size_t>();
	numV_metroAverage = vm.at("vMetroAve").as<size_t>();
	numV_cpe = vm.at("vCpe").as<size_t>();
	numV_cpeAverage = vm.at("vCpeAve").as<size_t>();

	cost = 0;
	costTwoScenario = 0;
	costThreeScenario = 0;
	costFourScenario = 0;
	costFiveScenario = 0;
	costSixScenario = 0;

	backupSource = 0;
	sourceProtectionPath = 0;

	minDelay = 0;
	maxDelay = 0;
	
	nameFileGraph = findNameFileGraphSTP();
	nameFileMeta = findNameFileMeta();
	nameFileSolution = findNameFileSolution();
}

bool GraphGenerator::generateGraph()
{
	std::cout << "Create Edges" << endl;
	if (!createEdges())
		return false;
	std::cout << "Create Directions" << endl;
	createDirections();
	std::cout << "Create Types" << endl;
	createTypes();
	std::cout << "Create Weights" << endl;
	createWeights();
	std::cout << "Create Terminals" << endl;
	if (!createTerminals())
		return false;
	std::cout << "Create Scenario" << endl;
	createScenarios();

	std::cout << "Create BoostGraph" << endl;
	BoostGraph inputGraph = createBoostGraph(weights);
	//std::cout << "Find exact solution" << endl;
	//cost = countExactSolution(inputGraph, terminals, source);
	std::cout << "Check all scenarios" << endl;
	BoostGraph additionalGraph = checkAllScenarios(inputGraph, false);
	//std::cout << boost::num_vertices(inputGraph) << " " << boost::num_edges(inputGraph) << std::endl;
	//std::cout << boost::num_vertices(additionalGraph) << " " << boost::num_edges(additionalGraph) << std::endl;
	std::cout << "Add one-directed edges" << endl;
	addOneDirectionEdges(inputGraph, additionalGraph);
	std::cout << "Update directions" << endl;
	updateDirectionAfterScenarios(inputGraph, additionalGraph);

	std::cout << "Write graph in file" << endl;
	writeGraphFile();
	std::cout << "Write metafile" << endl;
	writeMetaFile();	

	return true;
}

GraphGenerator::~GraphGenerator()
{
	terminals.clear();
	weights.clear();
	
	startV.clear();
	finishV.clear();
	directionEdges.clear();
	typeVertices.clear();
	indexMetro.clear();

	scenarios.clear();
	verticesBlockMetro.clear();
	verticesBlockCpe.clear();
	start_finish_VertexCpeBlock.clear();
	start_finish_VertexMetroBlock.clear();
	edgesBlockMetro.clear();
	edgesBlockCpe.clear();

	delays.clear();
	newTerminals.clear();
	deleteTerminals.clear();
}

////////////////////////////////////////////////////////////////

vector<size_t> GraphGenerator::parserScenarios(size_t s)
{
	vector<size_t> result;
	size_t allScenarios = s;
	while (allScenarios > 0) 
	{
		result.push_back(allScenarios % 10);
		allScenarios /= 10;
	}
	std::reverse(result.begin(), result.end());
	return result;
}

bool GraphGenerator::createEdges()
{
	size_t currEdgesBackbone;
	vector<size_t> numEdgesTransition_MetroCpe;
	vector<size_t> numVerticesTransition_BackboneMetro;
	vector<size_t> numVerticesTransition_MetroCpe;
	vector<size_t> numEdgesTransition_BackboneMetro;

	std::cout << "Count additional parameters" << endl;
	//VERTICES
	// block size
	numBlockMetro = numV_metro / numV_metroAverage;
	numBlockCpe = numV_cpe / numV_cpeAverage;
	// vertices for each block metro
	/*if (random<size_t>(1, 100) % 2)
		verticesBlockMetro = constVertexBlock(numBlockMetro, numV_metroAverage);
	else verticesBlockMetro = randomVertexBlock(numBlockMetro, numV_metroAverage);*/
	verticesBlockMetro = randomVertexBlock(numBlockMetro, numV_metroAverage);

	size_t summVerticesMetro = summArray(verticesBlockMetro);
	numV_metro = summVerticesMetro;

	start_finish_VertexMetroBlock =
		countStartFinishVertexBlock(verticesBlockMetro);

	// vertices for each block cpe
	/*if (random<size_t>(1, 100) % 2)
		verticesBlockCpe = constVertexBlock(numBlockCpe, numV_cpeAverage);
	else verticesBlockCpe = randomVertexBlock(numBlockCpe, numV_cpeAverage);*/
	verticesBlockCpe = randomVertexBlock(numBlockCpe, numV_cpeAverage);
	size_t summVerticesCpe = summArray(verticesBlockCpe);
	numV_cpe = summVerticesCpe;

	numberVertices = summVerticesCpe + summVerticesMetro + numV_backbone;

	start_finish_VertexCpeBlock =
		countStartFinishVertexBlock(verticesBlockCpe);

	// vertices for connection blocks
	numVerticesTransition_BackboneMetro =
		createCountVerticesTransition(numBlockMetro, 1, 5);
	numVerticesTransition_MetroCpe =
		createCountVerticesTransition(numBlockCpe, 2, 3);

	//EDGES
	
	// edges for connection
	// metro and cpe
	numEdgesTransition_MetroCpe =
		createCountEdgesTransition(numVerticesTransition_MetroCpe);
	// backbone and metro
	numEdgesTransition_BackboneMetro =
		createCountEdgesTransition(numVerticesTransition_BackboneMetro);

	size_t summBackboneMetro = summArray(numEdgesTransition_BackboneMetro);
	size_t summMetroCpe = summArray(numEdgesTransition_MetroCpe);

	// edges for each block metro
	edgesBlockMetro = countEdgeBlock(verticesBlockMetro);
	size_t summMetro = summArray(edgesBlockMetro);

	// edges for each block cpe
	edgesBlockCpe = countEdgeBlock(verticesBlockCpe);
	size_t summCpe = summArray(edgesBlockCpe);

	currEdgesBackbone = numberEdges - summBackboneMetro - summMetroCpe - summMetro - summCpe;
	//cout << "Input Edges=" << numberEdges << endl;
	//cout << "Other Edges=" << summBackboneMetro + summMetroCpe + summMetro + summCpe << endl;

	while (currEdgesBackbone < numV_backbone || currEdgesBackbone > numV_backbone * (numV_backbone - 1) / 2)
	{
		// edges for each block metro
		removeEdges(verticesBlockMetro, edgesBlockMetro);
		summMetro = summArray(edgesBlockMetro);
		// edges for each block cpe
		removeEdges(verticesBlockCpe, edgesBlockCpe);
		summCpe = summArray(edgesBlockCpe);

		currEdgesBackbone = numberEdges - summBackboneMetro - summMetroCpe - summMetro - summCpe;
		//cout << "Input Edges=" << numberEdges << endl;
		//cout << "Other Edges=" << summBackboneMetro + summMetroCpe + summMetro + summCpe << endl;
	}
	
	std::cout << endl;
	numE_backbone = currEdgesBackbone;
	
	
	//CREATE GRAPH

	//backbone
	std::cout << "Create Backbone" << endl;
	if (numE_backbone < numV_backbone)
	{
		std::cout << "Less backbone edges" << endl;
		return false;
	}

	if (numE_backbone > numV_backbone * (numV_backbone - 1) / 2)
	{
		std::cout << "More backbone edges" << endl;
		return false;
	}

	createPartGraph(0, numV_backbone - 1, currEdgesBackbone);

	size_t startBackbone = 0;
	size_t step = numV_backbone / numBlockMetro;

	//step += numV_backbone % numBlockMetro;
	//cout << "step = " << step << endl;
	//metro
	for (int i = 0; i < numBlockMetro; i++)
	{
		size_t startMetro = numV_backbone + start_finish_VertexMetroBlock[2 * i];
		size_t finishMetro = numV_backbone + start_finish_VertexMetroBlock[2 * i + 1];

		size_t finishBackbone = startBackbone + step - 1;

		if (finishBackbone > numV_backbone - 1)
			finishBackbone = numV_backbone - 1;

		std::cout << "Create Metro " << i << endl;
		createPartGraph(startMetro, finishMetro, edgesBlockMetro[i]);
		//merge metro and backbone
		mergeParts(startBackbone, finishBackbone, startMetro, finishMetro, numVerticesTransition_BackboneMetro[2 * i],
			numVerticesTransition_BackboneMetro[2 * i + 1], numEdgesTransition_BackboneMetro[i]);

		//cout << "start backbone = " << startBackbone << endl;
		startBackbone += step;
		//cout << "finish backbone = " << finishBackbone << endl;
	}

	size_t currIndexMetro;
	if (numBlockMetro > numBlockCpe)
		currIndexMetro = random<size_t>(0, numBlockMetro - 1);
	else currIndexMetro = 0;

	//create cpe
	for (int j = 0; j < numBlockCpe; j++)
	{
		size_t startCpe = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * j];
		size_t finishCpe = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * j + 1];

		if (numBlockMetro > numBlockCpe)
		{
			while (checkDouble(indexMetro, currIndexMetro) != -1)
			{
				currIndexMetro = random<size_t>(0, numBlockMetro - 1);
			}
		}
		else
		{
			if (currIndexMetro == numBlockMetro)
				currIndexMetro = 0;
		}

		indexMetro.push_back(currIndexMetro);

		size_t startMetro = numV_backbone + start_finish_VertexMetroBlock[2 * currIndexMetro];
		size_t finishMetro = numV_backbone + start_finish_VertexMetroBlock[2 * currIndexMetro + 1];

		std::cout << "Create CPE " << j << endl;
		createPartGraph(startCpe, finishCpe, edgesBlockCpe[j]);
		//merge cpe and metro
		//std::cout << "Merge CPE " << j << endl;
		mergeParts(startMetro, finishMetro, startCpe, finishCpe,
			numVerticesTransition_MetroCpe[2 * j], numVerticesTransition_MetroCpe[2 * j + 1],
			numEdgesTransition_MetroCpe[j]);

		if (numBlockMetro <= numBlockCpe)
			currIndexMetro++;
	}

	numVerticesTransition_BackboneMetro.clear();
	numVerticesTransition_MetroCpe.clear();
	numEdgesTransition_MetroCpe.clear();
	numEdgesTransition_BackboneMetro.clear();
	
	return true;
}

void GraphGenerator::createPartGraph(size_t startVertex, size_t finishVertex, size_t countEdges)
{
	size_t j = startVertex;
	vector<size_t> start, finish;
	
	for (size_t i = 0; i < countEdges; i++)
	{
		size_t startCurr, finishCurr;

		if (i < finishVertex - startVertex)
		{
			startCurr = j;
			finishCurr = j + 1;
			j++;
		}
		else if (i == finishVertex - startVertex &&
			checkDoubleEdge(start, finish, startVertex, j) == false)
		{
			startCurr = j;
			finishCurr = startVertex;
		}
		else
		{
			do {
				startCurr = random<size_t>(startVertex, finishVertex);
				finishCurr = random<size_t>(startVertex, finishVertex);

				if (startCurr == finishCurr)
					finishCurr++;

			} while (checkDoubleEdge(start, finish, startCurr, finishCurr) == true);
		}

		std::cout << ".";

		start.push_back(startCurr);
		finish.push_back(finishCurr);
	}
	for (int i = 0; i < start.size(); i++)
	{
		startV.push_back(start[i]);
		finishV.push_back(finish[i]);
	}
	start.clear();
	finish.clear();
	std::cout << std::endl;
}


void GraphGenerator::removeEdges(vector<size_t>& vertices, vector<size_t>& edges)
{
	for (int i = 0; i < vertices.size(); i++)
	{
		if (edges[i] > vertices[i])
			edges[i]--;
	}
}

void GraphGenerator::mergeParts(
	size_t oneStart, size_t oneFinish,
	size_t twoStart, size_t twoFinish,
	size_t numVerticesOne, size_t numVerticesTwo, size_t countEdges)
{
	vector<size_t> verticesOne;
	vector<size_t> verticesTwo;
	int start = -1, finish = -1;

	for (int i = 0; i < numVerticesOne; i++)
	{
		size_t curr, repeat = 0;

		do {
			repeat++;
			curr = random<size_t>(oneStart, oneFinish);
			if (repeat > 20)
				break;
		} while (checkDoubleVertex(verticesOne, curr) == true);

		if (repeat > 20)
		{
			for (int j = 0; j < oneFinish - oneStart; j++)
			{
				curr = oneStart + j;
				if (checkDoubleVertex(verticesOne, curr) == false)
					break;
			}
		}
		verticesOne.push_back(curr);
	}
	for (int i = 0; i < numVerticesTwo; i++)
	{
		size_t curr, repeat = 0;

		do {
			repeat++;
			curr = random<size_t>(twoStart, twoFinish);
			if (repeat > 20)
				break;
		} while (checkDoubleVertex(verticesTwo, curr) == true);

		if (repeat > 20)
		{
			for (int j = 0; j < twoFinish - twoStart; j++)
			{
				curr = twoStart + j;
				if (checkDoubleVertex(verticesTwo, curr) == false)
					break;
			}
		}
		verticesTwo.push_back(curr);
	}

	//std::cout << "Edges" << std::endl;
	for (int i = 0; i < countEdges; i++)
	{
		start = -1, finish = -1;

		if (i < numVerticesOne)
		{
			start = i;
		}
		if (i < numVerticesTwo)
		{
			finish = i;
		}
		if (start == -1 && finish != -1)
		{
			int repeat = 0;
			do {
				repeat++;
				start = random<size_t>(0, numVerticesOne - 1);
				if (repeat > 20)
					break;
			} while (checkDoubleEdge(startV, finishV, verticesOne[start], 
				verticesTwo[finish]) == true);

			if (repeat > 20)
			{
				for (int j = 0; j < numVerticesOne; j++)
				{
					start = j;
					if (checkDoubleEdge(startV, finishV, verticesOne[start], 
						verticesTwo[finish]) == false)
						break;
				}
			}
		}
		else if (finish == -1 && start != -1)
		{
			int repeat = 0;

			do {
				repeat++;
				finish = random<size_t>(0, numVerticesTwo - 1);
				if (repeat > 20)
					break;
			} while (checkDoubleEdge(startV, finishV, verticesOne[start],
				verticesTwo[finish]) == true);

			if (repeat > 20)
			{
				for (int j = 0; j < numVerticesTwo; j++)
				{
					finish = j;
					if (checkDoubleEdge(startV, finishV, verticesOne[start], 
						verticesTwo[finish]) == false)
						break;
				}
			}
		}
		else if (start == -1 && finish == -1)
		{
			int repeat = 0;
			do {
				repeat++;
				start = random<size_t>(0, numVerticesOne - 1);
				finish = random<size_t>(0, numVerticesTwo - 1);
				if (repeat > 20)
					break;
			} while (checkDoubleEdge(startV, finishV, verticesOne[start],
				verticesTwo[finish]) == true);

			if (repeat > 20)
			{
				for (int j = 0; j < numVerticesOne; j++)
				{
					for (int k = 0; k < numVerticesTwo; k++)
					{
						start = j;
						finish = k;
						if (checkDoubleEdge(startV, finishV, verticesOne[start], 
							verticesTwo[finish]) == false)
							break;
					}
				}
			}
		}

		if (checkDoubleEdge(startV, finishV, verticesOne[start], verticesTwo[finish]) == false)
		{
			startV.push_back(verticesOne[start]);
			finishV.push_back(verticesTwo[finish]);

		}
	}
	verticesOne.clear();
	verticesTwo.clear();
}

bool GraphGenerator::checkDoubleVertex(vector<size_t>& vec, size_t value)
{
	for (int i = 0; i < vec.size(); i++)
	{
		if (vec[i] == value)
			return true;
	}
	return false;
}

bool GraphGenerator::checkDoubleEdge(vector<size_t>& vecStart, vector<size_t>& vecFinish,
	size_t valueStart, size_t valueFinish)
{
	for (int i = 0; i < vecStart.size(); i++)
	{
		if (vecStart[i] == valueStart && vecFinish[i] == valueFinish)
			return true;

		if (vecStart[i] == valueFinish && vecFinish[i] == valueStart)
			return true;
	}
	return false;
}

size_t GraphGenerator::summArray(vector<size_t>& array)
{
	size_t summ = 0;
	for (size_t i = 0; i < array.size(); i++)
	{
		summ += array[i];
	}
	return summ;
}

vector<size_t> GraphGenerator::constVertexBlock(size_t size, size_t numAverage)
{
	vector<size_t> result;
	for (size_t i = 0; i < size; i++)
	{
		result.push_back(numAverage);
	}
	return result;
}

/*float avg(vector<size_t> vec)
{
	return std::accumulate(vec.begin(), vec.end(), 0) / static_cast<float>(vec.size());
}*/

vector<size_t> GraphGenerator::randomVertexBlock(size_t size, size_t numAverage)
{
	vector<size_t> result = constVertexBlock(size, numAverage);

	size_t start = 0;
	size_t finish = (25 * numAverage) / 100;

	for (size_t i = 0; i < size / 2; i += 2)
	{
		size_t rand = random<size_t>(start, finish);

		result[i] += rand;
		result[i + 1] -= rand;
	}
	//cout << "Average=" << avg(result) << endl;
	return result;
}

vector<size_t> GraphGenerator::countEdgeBlock(vector<size_t>& vec)
{
	vector<size_t> result;
	for (int i = 0; i < vec.size(); i++)
	{
		size_t min = vec[i];
		size_t max = vec[i]*(vec[i]-1) / 2;
		if (max == 0 || max < min)
			result.push_back(std::min(min,max));
		else result.push_back(random<size_t>(min, max));
	}
	return result;
}

vector<size_t> GraphGenerator::countStartFinishVertexBlock(vector<size_t>& array)
{
	vector<size_t> result;
	for (size_t i = 0; i < 2 * array.size(); i++)
	{
		result.push_back(0);
	}
	for (size_t i = 0; i < array.size(); i++)
	{
		// start interval
		if (i == 0)
			result[2 * i] = 0;
		else result[2 * i] = result[2 * i - 1] + 1;
		// finish interval
		result[2 * i + 1] = result[2 * i] + array[i] - 1;
	}
	return result;
}

vector<size_t> GraphGenerator::createCountVerticesTransition(
	size_t numBlock, size_t startRandom, size_t endRandom)
{
	vector<size_t> result;
	for (size_t i = 0; i < 2 * numBlock; i++)
	{
		size_t curr = random<size_t>(0, 100);

		if (curr % 2 == 0)
			curr = startRandom;
		else curr = endRandom;
		
		result.push_back(curr);
	}
	return result;
}

vector<size_t> GraphGenerator::createCountEdgesTransition(vector<size_t>& numVertices)
{
	vector<size_t> result;
	for (size_t i = 0; i < numVertices.size() / 2; i++)
	{
		size_t countOne = numVertices[2 * i];
		size_t countTwo = numVertices[2 * i + 1];

		if (countOne == 1 || countTwo == 1)
			result.push_back(countOne + countTwo - 1);
		else
		{
			size_t min = 0;
			(countOne > countTwo) ? min = countOne : min = countTwo;
			size_t max = countOne * countTwo;
			size_t rand = random<size_t>(min, max);

			result.push_back(rand);
		}
	}
	return result;
}

bool GraphGenerator::createWeights()
{
	for (size_t i = 0; i < startV.size(); i++)
	{
		if (startV[i] < numV_backbone && finishV[i] < numV_backbone)
			weights.push_back(random<size_t>(MIN_WEIGHT_BACKBONE, MAX_WEIGHT_BACKBONE));
		else weights.push_back(random<size_t>(MIN_WEIGHT_CITY, MAX_WEIGHT_CITY));
	}
	return true;
}

bool GraphGenerator::createDirections()
{
	// 0 ->; 1 <-; 2 <->;
	for (size_t i = 0; i < startV.size(); i++)
	{
		directionEdges.push_back(2);
	}
	return true;
}

bool GraphGenerator::createTerminals()
{
	// true - allBlocks; false - oneBlock
	size_t startT = 0, finishT = 0, startS = 0, finishS = 0;
	int indexBlock = -1, indexSource = -1;
	/*if (typeTerminals == false)
	{
		int summ = 0;
		for (int i = 0; i < numBlockCpe; i++)
		{
			if (numberTerminals <= verticesBlockCpe[i])
			{
				indexBlock = i;
				summ++;
			}
		}
		if (summ == 0)
		{
			std::cout << "ERROR: |T| > vertices in each cpe block" << std::endl;
			return false;
		}
	}*/
	/*if (typeTerminals == true)
	{
		if (numberTerminals + 1 > numBlockCpe)
		{
			std::cout << "ERROR: |T| > cpeBlocks" << std::endl;
			return false;
		}
	}*/

	if (typeTerminals == false)
	{
		indexSource = random<int>(0, numBlockCpe - 1);
		indexBlock = random<int>(0, numBlockCpe - 1);

		while (indexBlock == indexSource)
			indexSource = random<int>(0, numBlockCpe - 1);

		startT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock];
		finishT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock + 1];

		startS = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexSource];
		finishS = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexSource + 1];
	}

	if (typeTerminals == true)
	{
		indexBlock = 0;
		indexSource = random<int>(0, numBlockCpe - 1);

		while (indexBlock == indexSource)
			indexSource = random<int>(0, numBlockCpe - 1);

		startS = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexSource];
		finishS = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexSource + 1];
	}

	size_t currSummTerminals = 0;

	while (terminals.size() < numberTerminals)
	{
		if (terminals.size() == 0)
		{
			source = random<size_t>(startS, finishS);
		}

		if (typeTerminals == true)
		{
			if (indexBlock == numBlockCpe)
				indexBlock = 0; 

			startT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock];
			finishT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock + 1];

			if (finishT == numberVertices - 1)
				indexBlock = -1;
		}

		if (typeTerminals == false)
		{
			if (currSummTerminals == finishT - startT + 1)
			{
				currSummTerminals = 0;
				indexBlock++;

				if (indexBlock == numBlockCpe)
					indexBlock = 0;

				startT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock];
				finishT = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * indexBlock + 1];
			}
			currSummTerminals++;
		}

		size_t terminal = random<size_t>(startT, finishT);
		int index = checkDouble(terminals, terminal);

		while (index != -1)
		{
			terminal = random(startT, finishT);
			index = checkDouble(terminals, terminal);
		}
		terminals.push_back(terminal);

		if (typeTerminals == true)
		{
			indexBlock++;

			if (indexBlock == indexSource)
				indexBlock++;
		}
	}
	return true;
}

int GraphGenerator::checkDouble(vector<size_t>& vec, size_t value)
{
	auto it = std::find(vec.begin(), vec.end(), value);

	if (it != vec.end())
		return (size_t)(std::distance(vec.begin(), it));
	else
		return -1;
}

bool GraphGenerator::createTypes()
{
	// 0 - BACKBONE; 1 - METRO; 2 - CPE;
	for (size_t i = 0; i < numV_backbone; i++)
	{
		typeVertices.push_back(0);
	}
	for (size_t i = numV_backbone; i < numV_backbone + numV_metro; i++)
	{
		typeVertices.push_back(1);
	}
	for (size_t i = numV_backbone + numV_metro; i < numberVertices; i++)
	{
		typeVertices.push_back(2);
	}
	return true;
}

BoostGraph GraphGenerator::createBoostGraph(vector<long long>& w)
{
	BoostGraph graph;
	for (int i = 0; i < startV.size(); i++)
	{
		if (checkDouble(scenarios, 4) != -1)
		{
			graph.addEdge({ startV[i], finishV[i], w[i], delays[i] });
			graph.addEdge({ finishV[i], startV[i], w[i], delays[i] });
		}
		else
		{
			graph.addEdge({ startV[i], finishV[i], w[i] });
			graph.addEdge({ finishV[i], startV[i], w[i] });
		}
	}
	//std::cout << "Input graph" << std::endl;
	//std::cout << graph << std::endl;
	return graph;
}

bool GraphGenerator::writeGraphFile()
{
	ofstream outFile;
	outFile.open(nameFileGraph, ios_base::out);

	writeRemark(outFile);
	writeGraph(outFile);
	writeTerminals(outFile);

	// for 4 scenario
	if (checkDouble(scenarios, 4) != -1)
		writeGraphScenarioFour(outFile);

	outFile << "EOF";
	outFile.close();
	return true;
}

bool GraphGenerator::writeGraphScenarioFour(ofstream& outFile)
{
	size_t numArcs = countNumberArcs();
	outFile << "SECTION Delay" << endl;
	outFile << "Nodes " << numberVertices << endl;
	outFile << "Edges " << startV.size() - numArcs << endl;
	for (int i = 0; i < startV.size(); i++)
	{
		if (directionEdges[i] == 2)
		{
			outFile << "E " << startV[i] + index << " ";
			outFile << finishV[i] + index << " " << delays[i] << endl;
		}
	}
	if (numArcs != 0)
	{
		outFile << "Arcs " << numArcs << endl;
		for (int i = 0; i < startV.size(); i++)
		{
			if (directionEdges[i] == 0)
			{
				outFile << "A " << startV[i] + index << " ";
				outFile << finishV[i] + index << " " << delays[i] << endl;
			}
			else if (directionEdges[i] == 1)
			{
				outFile << "A " << finishV[i] + index << " ";
				outFile << startV[i] + index << " " << delays[i] << endl;
			}
		}
	}
	outFile << "END" << endl << endl;
	return true;
}

bool GraphGenerator::writeRemark(ofstream& outFile)
{
	outFile << "SECTION Comment" << endl;
	outFile << "Remark " << "\"" << nameFileMeta << "\"" << endl;
	outFile << "END" << endl << endl;
	return true;
}

size_t GraphGenerator::countNumberArcs()
{
	size_t summ = 0;
	if (direction == 1)
	{
		for (int i = 0; i < directionEdges.size(); i++)
		{
			if (directionEdges[i] != 2)
				summ++;
		}
	}
	return summ;
}

bool GraphGenerator::writeGraph(ofstream& outFile)
{
	size_t numArcs = countNumberArcs();
	outFile << "SECTION Graph" << endl;
	outFile << "Nodes " << numberVertices << endl;
	outFile << "Edges " << startV.size() - numArcs << endl;
	for (int i = 0; i < startV.size(); i++)
	{
		if (directionEdges[i] == 2)
		{
			outFile << "E " << startV[i] + index << " ";
			outFile << finishV[i] + index << " " << weights[i] << endl;
		}
	}
	if (numArcs != 0)
	{
		outFile << "Arcs " << numArcs << endl;
		for (int i = 0; i < startV.size(); i++)
		{
			if (directionEdges[i] == 0)
			{
				outFile << "A " << startV[i] + index << " ";
				outFile << finishV[i] + index << " " << weights[i] << endl;
			}
			else if (directionEdges[i] == 1)
			{
				outFile << "A " << finishV[i] + index << " ";
				outFile << startV[i] + index << " " << weights[i] << endl;
			}
		}	
	}
	outFile << "END" << endl << endl;
	return true;
}

bool GraphGenerator::writeTerminals(ofstream& outFile)
{
	outFile << "SECTION Terminals" << endl;
	if (cost != 0)
		outFile << "Cost " << cost << endl;
	outFile << "Root " << source + index << endl;
	outFile << "Terminals " << terminals.size() << endl;
	for (int i = 0; i < terminals.size(); i++)
	{
		outFile << "T " << terminals[i] + index << endl;
	}
	outFile << "END" << endl << endl;
	return true;
}

bool GraphGenerator::writeMetaFile()
{
	ofstream outFile;
	outFile.open(nameFileMeta, ios_base::out);
	writeGenerateParametersMetaFile(outFile);
	writeScenariosMetaFile(outFile);
	writeEachCityMetaFile(outFile);
	outFile.close();
	return true;
}

bool GraphGenerator::writeGenerateParametersMetaFile(ofstream& outFile)
{
	outFile << "[Parameters]" << endl;
	outFile << "V=" << numberVertices << endl;
	outFile << "E=" << startV.size() << endl;
	outFile << "V_backbone=" << numV_backbone << endl;
	outFile << "V_Metro=" << numV_metro << endl;
	outFile << "V_averageMetro=" << numV_metroAverage << endl;
	outFile << "V_Cpe=" << numV_cpe << endl;
	outFile << "V_averageCpe=" << numV_cpeAverage << endl;
	outFile << "Terminals=" << terminals.size() << endl;
	outFile << "City=" << numBlockMetro + 1 << endl;
	if (cost != 0)
		outFile << "Opt_cost=" << cost << endl << endl;
	outFile << endl;
	return true;
}

bool GraphGenerator::writeScenariosMetaFile(ofstream& outFile)
{
	//outFile << "[Scenario]" << endl;
	for (size_t i = 0; i < scenarios.size(); i++)
	{
		if(scenarios[i] == 1)
			writeScenarioOneMetaFile(outFile, scenarios[i]);
		else if (scenarios[i] == 2)
			writeScenarioTwoMetaFile(outFile, scenarios[i]);
		else if (scenarios[i] == 3)
			writeScenarioThreeMetaFile(outFile, scenarios[i]);
		else if (scenarios[i] == 4)
			writeScenarioFourMetaFile(outFile, scenarios[i]);
		else if (scenarios[i] == 5)
			writeScenarioFiveMetaFile(outFile, scenarios[i]);
		else if (scenarios[i] == 6)
			writeScenarioSixMetaFile(outFile, scenarios[i]);
	}
	return true;
}

bool GraphGenerator::writeScenarioOneMetaFile(ofstream& outFile, size_t currScenario)
{
	outFile << "[Scenario_" << currScenario << "]" << endl;
	if (exactSolution == true && numberVertices < 20000 && cost != 0)
		outFile << "OptCost=" << cost << endl;
	return 0;
}

bool GraphGenerator::writeScenarioTwoMetaFile(ofstream& outFile, size_t currScenario)
{
	outFile << "[Scenario_" << currScenario << "]" << endl;
	outFile << "BackupSource=" << backupSource + index << endl;
	if (exactSolution == true && numberVertices < 20000 && costTwoScenario != 0)
		outFile << "OptCost=" << costTwoScenario << endl;
	return true;
}

bool GraphGenerator::writeScenarioThreeMetaFile(ofstream& outFile, size_t currScenario)
{
	outFile << "[Scenario_" << currScenario << "]" << endl;
	outFile << "BackupSource=" << sourceProtectionPath + index << endl;
	if (exactSolution == true && numberVertices < 20000 && costThreeScenario != 0)
		outFile << "OptCost=" << costThreeScenario << endl;
	return true;
}

bool GraphGenerator::writeScenarioFourMetaFile(ofstream& outFile, size_t currScenario)
{
	if (statusFourScenario == false)
		return true;

	outFile << "[Scenario_" << currScenario << "]" << endl;
	if (exactSolution == true && numberVertices <= 40 && costFourScenario != 0)
		outFile << "OptCost=" << costFourScenario << endl;
	//outFile << "MinDelay=" << minDelay << endl;
	//outFile << "MaxDelay=" << maxDelay << endl;
	outFile << "DelayLimit=" << delayLimit << endl;
	
	outFile << endl;
	return true;
}

bool GraphGenerator::writeScenarioFiveMetaFile(ofstream& outFile, size_t currScenario)
{
	if (statusFiveScenario == false)
		return true;

	outFile << "[Scenario_" << currScenario << "]" << endl;
	if (exactSolution == true && numberVertices < 20000 && costFiveScenario != 0)
		outFile << "OptCost=" << costFiveScenario << endl;
	outFile << "SizeTerminals=" << newTerminals.size() << endl;
	outFile << "ListTerminals=";
	for (int i = 0; i < newTerminals.size(); i++)
		outFile << newTerminals[i] << " ";
	outFile << endl;
	return true;
}

bool GraphGenerator::writeScenarioSixMetaFile(ofstream& outFile, size_t currScenario)
{
	if (statusSixScenario == false)
		return true;

	outFile << "[Scenario_" << currScenario << "]" << endl;
	if (exactSolution == true && numberVertices < 20000 && costSixScenario != 0)
		outFile << "OptCost=" << costSixScenario << endl;
	outFile << "SizeTerminals=" << deleteTerminals.size() << endl;
	outFile << "ListTerminals=";
	for (int i = 0; i < deleteTerminals.size(); i++)
		outFile << deleteTerminals[i] << " ";
	outFile << endl;
	return true;
}

bool GraphGenerator::writeEachCityMetaFile(ofstream& outFile)
{
	outFile << endl << "; Info about each block:" << endl;
	for (int i = 0; i < numBlockMetro + 1; i++)
	{
		writeMetaInfoOneCity(outFile, i);
	}
	return true;
}

bool GraphGenerator::writeMetaInfoOneCity(ofstream& outFile, int id)
{
	vector<size_t> cpeIndex;
	int numBlocksCpe = 0, numBlocksMetro = 0, numBlocksBackbone = 0;
	if (id == 0)
	{
		numBlocksCpe = 0, numBlocksMetro = 0, numBlocksBackbone = 1;
	}
	else
	{
		numBlocksBackbone = 0, numBlocksMetro = 1;

		for (size_t i = 0; i < indexMetro.size(); i++)
		{
			if (indexMetro[i] == id - 1)
			{
				numBlocksCpe++;
				cpeIndex.push_back(i);
			}
		}
	}

	outFile << "[City" << id << "]" << endl;
	outFile << "BlocksBackbone=" << numBlocksBackbone << endl;
	outFile << "BlocksMetro=" << numBlocksMetro;

	if (id != 0)
		outFile << " " << id - 1;
	outFile << endl;

	outFile << "BlocksCpe=" << numBlocksCpe << " ";
	for (size_t i = 0; i < cpeIndex.size(); i++)
		outFile << cpeIndex[i] << " ";
	outFile << endl;

	for (size_t i = 0; i < numBlocksBackbone; i++)
	{
		outFile << "[Block_" << i << "_BACKBONE]" << endl;
		outFile << "Type=" << "BACKBONE" << endl;
		outFile << "FirstVertex=" << 0 + index << endl;
		outFile << "LastVertex=" << numV_backbone - 1 + index << endl;
		outFile << "NumberVertices=" << numV_backbone << endl;
		outFile << "NumberEdges=" << numE_backbone << endl;
		outFile << "NumberTerminals=" << 0 << endl;
		outFile << "Source=" << false << endl;
	}
	for (size_t i = 0; i < numBlocksMetro; i++)
	{
		size_t start = numV_backbone + start_finish_VertexMetroBlock[2 * (id - 1)];
		size_t finish = numV_backbone + start_finish_VertexMetroBlock[2 * (id - 1) + 1];

		outFile << "[Block_" << id - 1 << "_METRO]" << endl;
		outFile << "Type=" << "METRO" << endl;
		outFile << "FirstVertex=" << start + index << endl;
		outFile << "LastVertex=" << finish + index << endl;
		outFile << "NumberVertices=" << verticesBlockMetro[id - 1] << endl;
		outFile << "NumberEdges=" << edgesBlockMetro[id - 1] << endl;
		outFile << "NumberTerminals=" << 0 << endl;
		outFile << "Source=" << false << endl;
	}

	vector<size_t> sources;
	sources.push_back(source);
	if (checkDouble(scenarios, 2) != -1)
		sources.push_back(backupSource);
	if (checkDouble(scenarios, 3) != -1)
		sources.push_back(sourceProtectionPath);

	for (size_t i = 0; i < numBlockCpe; i++)
	{
		if (indexMetro[i] == id - 1)
		{
			size_t start = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * i];
			size_t finish = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * i + 1];

			outFile << "[Block_" << i << "_CPE]" << endl;
			outFile << "Type=" << "CPE" << endl;
			outFile << "FirstVertex=" << start + index << endl;
			outFile << "LastVertex=" << finish + index << endl;
			outFile << "NumberVertices=" << verticesBlockCpe[i] << endl;
			outFile << "NumberEdges=" << edgesBlockCpe[i] << endl;
			outFile << "NumberTerminals=" << countNumVerticesEachBlock(terminals, start, finish) << endl;
			outFile << "Source=" << countNumVerticesEachBlock(sources, start, finish) << endl;
		}
	}
	outFile << endl;
	return true;
}

size_t GraphGenerator::countExactSolution(BoostGraph& graph, vector<size_t>& _terminals, size_t _source)
{
	size_t result = 0;
	if (exactSolution &&
	((numberVertices <= 20000 && numberTerminals <= 10) ||
	(numberVertices <= 10000 && numberTerminals <= 15)))
	{
		Task task;
		task.graph = graph;
		task.source = _source;
		task.terminals = _terminals;
		DreyfusWagnerSolver dw;
		Solution sol = dw.solve(task);
		result = sol.sptCost;
	}
	return result;
}

bool GraphGenerator::createScenarios()
{
	for (size_t i = 0; i < scenarios.size(); i++)
	{
		if (scenarios[i] == 1)
			generateDataScenarioOne();
		else if (scenarios[i] == 2)
			generateDataScenarioTwo();
		else if (scenarios[i] == 3)
			generateDataScenarioThree();
		else if (scenarios[i] == 4)
			generateDataScenarioFour();
		else if (scenarios[i] == 5)
			generateDataScenarioFive();
		else if (scenarios[i] == 6)
			generateDataScenarioSix();
	}
	return true;
}

string GraphGenerator::findNameFileGraphSTP()
{
	string result = "graph_" + to_string(numberVertices)
					+ "_" + to_string(numberEdges)
					+ "_" + to_string(numberTerminals)
					+ "_Scenario" + to_string(scenario);
	if (direction)
		result += "_Percent" + to_string(percentOneDirectionEdges) + "_Direction";
	else result += "_Undirection";

	if (typeTerminals)
		result += "_AllBlocks";
	else result += "_OneBlock";

	result += ".stp";
	return result;
}

string GraphGenerator::findNameFileMeta()
{
	string result = "metainfo_";
	string graphName = nameFileGraph;
	graphName.erase(graphName.end() - 3, graphName.end());
	result += graphName + "ini";
	return result;
}

string GraphGenerator::findNameFileSolution()
{
	string result = "solution_";
	string graphName = nameFileGraph;
	graphName.erase(graphName.end() - 3, graphName.end());
	result += graphName + "txt";
	return result;
}

size_t GraphGenerator::countNumVerticesEachBlock(vector<size_t>& vec, size_t start, size_t finish)
{
	size_t summ = 0;
	for (size_t i = 0; i < vec.size(); i++)
	{
		if (vec[i] >= start && vec[i] <= finish)
			summ++;
	}
	return summ;
}

bool GraphGenerator::generateDataScenarioOne()
{
	return true;
}

bool GraphGenerator::generateDataScenarioTwo()
{
	size_t start;
	size_t finish;
	vector<size_t> sources;
	sources.push_back(source);

	for (size_t i = 0; i < numBlockCpe; i++)
	{
		start = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * i];
		finish = numV_backbone + numV_metro + start_finish_VertexCpeBlock[2 * i + 1];
		size_t numSource = countNumVerticesEachBlock(sources, start, finish);
		if (numSource == 1)
		{
			backupSource = random<size_t>(start, finish);
			break;
		}		
	}
	if (backupSource == 0)
	{
		start = numV_backbone + numV_metro;
		finish = numV_backbone + numV_metro + numV_cpe - 1;
		backupSource = random<size_t>(start, finish);
	}
	return true;
}

bool GraphGenerator::generateDataScenarioThree()
{
	sourceProtectionPath = source;
	return true;
}

bool GraphGenerator::generateDataScenarioFour()
{
	for (size_t i = 0; i < startV.size(); i++)
	{
		if (startV[i] < numV_backbone && finishV[i] < numV_backbone)
			delays.push_back(random<long long>(MIN_DELAY_BACKBONE, MAX_DELAY_BACKBONE));
		else delays.push_back(random<long long>(MIN_DELAY_CITY, MAX_DELAY_CITY));
	}

	return true;
}

bool GraphGenerator::generateDataScenarioFive()
{
	size_t numNewTerminals = random<size_t>(1, numberTerminals - 1);
	while (newTerminals.size() != numNewTerminals)
	{
		size_t newTerminal = random<size_t>(numV_backbone + numV_metro, numberVertices - 1);
		if (!checkDoubleVertex(terminals, newTerminal) && newTerminal != source)
		{
			newTerminals.push_back(newTerminal);
			//std::cout << newTerminals[newTerminals.size() - 1] << std::endl;
		}
	}	
	return true;
}

bool GraphGenerator::generateDataScenarioSix()
{
	vector<size_t> currTerminals = terminals;
	size_t numDeleteTerminals = random<size_t>(1, numberTerminals - 2);
	while (deleteTerminals.size() != numDeleteTerminals)
	{
		size_t indexTerminal = random<size_t>(0, currTerminals.size() - 1);
		deleteTerminals.push_back(currTerminals[indexTerminal]);
		currTerminals.erase(currTerminals.begin() + indexTerminal);
	}
	return true;
}

template < typename T >
T GraphGenerator::random(T start, T finish) 
{
	return (T)(start + (float)rand() / RAND_MAX * (finish - start));
}

void GraphGenerator::updateDirectionAfterScenarios(BoostGraph& inputGraph, BoostGraph& additionalGraph)
{
	if (direction == false || percentOneDirectionEdges == 0)
		return;

	auto iteratorsEdges = boost::edges(inputGraph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto egdeTaskGraph = inputGraph[*iteratorsEdges.first];
		long long w = 0;
		bool existEdgeAddit, existInvertEdgeAddit;
		
		existEdgeAddit = additionalGraph.getWeight(egdeTaskGraph.sourceID, egdeTaskGraph.targetID, w);
		existInvertEdgeAddit = additionalGraph.getWeight(egdeTaskGraph.targetID, egdeTaskGraph.sourceID, w);
		
		if (existEdgeAddit == true && existInvertEdgeAddit == false) // 01 <-
		{
			changeOneDirection(egdeTaskGraph, 1);
		}
		else if (existEdgeAddit == false && existInvertEdgeAddit == true) // 10 ->
		{
			changeOneDirection(egdeTaskGraph, 0);
		}
	}
}

void GraphGenerator::changeOneDirection(Edge e, size_t dir)
{
	for (size_t i = 0; i < startV.size(); i++)
	{
		if (startV[i] == e.sourceID && finishV[i] == e.targetID)
			directionEdges[i] = dir;
	}
}

BoostGraph GraphGenerator::checkAllScenarios(BoostGraph& graph, bool needSolution)
{
	BoostGraph resultGraph, summResultGraph;
	ofstream outFile;

	if (needSolution)
		outFile.open(nameFileSolution, ios_base::out);

	for (size_t i = 0; i < scenarios.size(); i++)
	{
		cout << "Check " << scenarios[i] << " scenario" << endl;

		//std::cout << boost::num_vertices(summResultGraph) << " " << boost::num_edges(summResultGraph) << std::endl;
		//std::cout << boost::num_vertices(resultGraph) << " " << boost::num_edges(resultGraph) << std::endl;

		if (scenarios[i] == 1)
		{
			resultGraph = checkScenarioOne(graph);
			if (needSolution)
				writeSolutionScenarioOne(resultGraph, outFile);
		}
		else if (scenarios[i] == 2)
		{
			BoostGraph additional = deleteEdgesInResult(graph, summResultGraph);
			resultGraph = checkScenarioTwo(additional);
			if (needSolution)
				writeSolutionScenarioTwo(resultGraph, outFile);
		}
		else if (scenarios[i] == 3)
		{
			BoostGraph additional = deleteEdgesInResult(graph, summResultGraph);
			resultGraph = checkScenarioThree(additional);
			if (needSolution)
				writeSolutionScenarioThree(resultGraph, outFile);
		}
		else if (scenarios[i] == 4)
		{
			resultGraph = checkScenarioFour(graph);
			//if (needSolution)
			//	writeSolutionScenarioFour(resultGraph, outFile);
		}
		else if (scenarios[i] == 5)
		{
			resultGraph = checkScenarioFive(graph);
			if (needSolution)
				writeSolutionScenarioFive(resultGraph, outFile);
		}
		else if (scenarios[i] == 6)
		{
			resultGraph = checkScenarioSix(graph);
			if (needSolution)
				writeSolutionScenarioSix(resultGraph, outFile);			
		}
		//std::cout << "Graph" << std::endl;
		//std::cout << graph << std::endl;
		/*std::cout << "Result Graph" << std::endl;
		std::cout << resultGraph << std::endl;*/

		summResultGraph.merge(resultGraph);
		//std::cout << "Summ Result Graph" << std::endl;
		//std::cout << summResultGraph << std::endl;
	}
	//std::cout << boost::num_vertices(summResultGraph) << " " << boost::num_edges(summResultGraph) << std::endl;
	//std::cout << boost::num_vertices(resultGraph) << " " << boost::num_edges(resultGraph) << std::endl;
	if (needSolution)
		outFile.close();
	return deleteEdgesInResult(graph, summResultGraph);
}

void GraphGenerator::addOneDirectionEdges(BoostGraph& inputGraph, BoostGraph& additionalGraph)
{
	if (direction == false || percentOneDirectionEdges == 0)
		return;

	size_t numOneDirEdges = numberEdges * ((float)percentOneDirectionEdges / 100);
	size_t currOneDirEdges = boost::num_edges(inputGraph) - boost::num_edges(additionalGraph);
	
	size_t start = numV_backbone + numV_metro;
	size_t finish = numberVertices - 1;
	
	// create random terminals and run Dijkstra
	while (currOneDirEdges < numOneDirEdges)
	{
		BoostGraph dijkstraGraph = additionalGraph;

		size_t sizeTerminals = random<size_t>(2, numberTerminals - 1);
		vector<size_t> randTerminals = createRandTerminals(dijkstraGraph, start, finish, sizeTerminals);
		vector<size_t> randSources = createRandTerminals(dijkstraGraph, start, finish, 1);
		DijkstraSolver ds;
		Task task;
		task.graph = dijkstraGraph;
		task.source = randSources[0];
		task.terminals = randTerminals;
		BoostGraph result = ds.solve(task).tree;
		//cout << "Dijkstra = " << boost::num_edges(dijkstraGraph) << " = " << boost::num_edges(result);

		if (boost::num_edges(result) == 0)
			continue;

		auto iteratorsEdges = boost::edges(dijkstraGraph);
		for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
		{
			auto currEdge = dijkstraGraph[*iteratorsEdges.first];
			long long w = 0;
			if (result.getWeight(currEdge.sourceID, currEdge.targetID, w))
				additionalGraph.removeEdge(currEdge);
		}
		currOneDirEdges = boost::num_edges(inputGraph) - boost::num_edges(additionalGraph);
		//cout << " + " << boost::num_edges(additionalGraph) << endl;
	}
}

vector<size_t> GraphGenerator::createRandTerminals(BoostGraph& graph, 
	size_t start, size_t finish, size_t sizeTerminals)
{
	if (finish - start < sizeTerminals)
		sizeTerminals = finish - start;

	vector<size_t> result;
	vector<size_t> currCpe;
	auto iteratorsVertices = boost::vertices(graph);
	for (; iteratorsVertices.first != iteratorsVertices.second; ++iteratorsVertices.first)
	{
		auto currVertex = graph[*iteratorsVertices.first];
		if (currVertex >= start && currVertex <= finish)
			currCpe.push_back(currVertex);
	}

	while (result.size() < sizeTerminals)
	{
		size_t randTerm = random<size_t>(start, finish);
		if(checkDouble(currCpe, randTerm) != -1)
			result.push_back(randTerm);
	}
	
	return result;
}
BoostGraph GraphGenerator::checkScenario(BoostGraph& graph, size_t number, 
	vector<size_t>& _terminals, size_t _source, size_t& _cost)
{
	BoostGraph result, empty, resultDW;
	Solution sol;
	// small graph
	if (exactSolution &&
		((numberVertices <= 20000 && numberTerminals <= 10) ||
			(numberVertices <= 10000 && numberTerminals <= 15)))
	{
		BoostGraph twoDirGraph, oneDirEdges;
		splitDirEdges(graph, twoDirGraph, oneDirEdges);
		DreyfusWagnerSolver dw;
		Task task;
		task.graph = twoDirGraph;
		task.terminals = _terminals;
		task.source = _source;
		sol = dw.solve(task);
		resultDW = sol.tree;
		//std::cout << resultDW << std::endl;
		if (resultDW.vertexCount() == 0)
		{
			cout << "Scenario " << number << " - NO" << endl;
			setStatusScenario(false, number);
			return empty;
		}

		size_t costDW = sol.sptCost;
		BoostGraph twoDirResult = addTwoDirEdges(resultDW);
		DijkstraSolver ds;
		task.graph = twoDirResult;
		sol = ds.solve(task);
		result = sol.tree;
		//std::cout << result << std::endl;
		_cost = sol.sptCost;
	}
	// large graph
	else
	{
		DijkstraSolver ds;
		Task task;
		task.graph = graph;
		task.source = _source;
		task.terminals = _terminals;
		sol = ds.solve(task);
		result = sol.tree;
	}
	if (result.vertexCount() != 0)
	{
		cout << "Scenario " << number << " - OK" << endl;
		setStatusScenario(true, number);
	}
	else
	{
		cout << "Scenario " << number << " - NO" << endl;
		setStatusScenario(false, number);
		return empty;
	}
	return result;
}

BoostGraph GraphGenerator::checkScenarioOne(BoostGraph& graph)
{
	return checkScenario(graph, 1, terminals, source, cost);
}

BoostGraph GraphGenerator::checkScenarioTwo(BoostGraph& graph)
{
	return checkScenario(graph, 2, terminals, backupSource, costTwoScenario);
}

BoostGraph GraphGenerator::checkScenarioThree(BoostGraph& graph)
{
	return checkScenario(graph, 3, terminals, sourceProtectionPath, costThreeScenario);
}

BoostGraph GraphGenerator::checkScenarioFour(BoostGraph& graph)
{
	BoostGraph empty;
	minDelay = graph.getMinDelayLimit(source, terminals);
	if (minDelay == 0)
	{
		cout << "Scenario 4 - NO" << endl;
		setStatusScenario(false, 4);
		return empty;
	}
	maxDelay = graph.getMaxDelayLimit(source, terminals);
	if (maxDelay == 0)
	{
		cout << "Scenario 4 - NO" << endl;
		setStatusScenario(false, 4);
		return empty;
	}

	delayLimit = (maxDelay + minDelay) / 2;

	// exact solver for 4 scenario
	if (numberVertices <= 40 && terminals.size() <= 3)
	{
		Task task;
		task.graph = graph;
		task.delayLimit = delayLimit;
		task.source = source;
		task.terminals = terminals;
		DelayExactSolver des;
		Solution sol = des.solve(task);
		//std::cout << sol.tree << std::endl;
		if (sol.tree.vertexCount() == 0)
		{
			cout << "Scenario 4 - NO" << endl;
			setStatusScenario(false, 4);
			return empty;
		}
		else
		{
			costFourScenario = sol.sptCost;
			cout << "Scenario 4 - OK" << endl;
			setStatusScenario(true, 4);
			return sol.tree;
		}
	}
	else
	{
		cout << "Scenario 4 - OK" << endl;
		setStatusScenario(true, 4);
		return empty;
	}
}

BoostGraph GraphGenerator::checkScenarioFive(BoostGraph& graph)
{
	vector<size_t> term = terminals;
	term.insert(term.end(), newTerminals.begin(), newTerminals.end());

	return checkScenario(graph, 5, term, source, costFiveScenario);
}

BoostGraph GraphGenerator::checkScenarioSix(BoostGraph& graph)
{
	vector<size_t> term;
	for (size_t i = 0; i < terminals.size(); i++)
	{
		if (checkDouble(deleteTerminals, terminals[i]) == -1)
		{
			term.push_back(terminals[i]);
		}
	}

	return checkScenario(graph, 6, term, source, costSixScenario);
}

BoostGraph GraphGenerator::deleteEdgesInResult(BoostGraph& graph, BoostGraph& result)
{
	BoostGraph notResult;
	auto iteratorsEdges = boost::edges(graph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto egdeTaskGraph = graph[*iteratorsEdges.first];
		long long w = 0;
		if (!result.getWeight(egdeTaskGraph.sourceID, egdeTaskGraph.targetID, w))
			notResult.addEdge(egdeTaskGraph);
	}
	return notResult;
}

void GraphGenerator::splitDirEdges(BoostGraph& graph, BoostGraph& twoDirGraph,
	BoostGraph& oneDirEdges)
{
	auto iteratorsEdges = boost::edges(graph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto edge = graph[*iteratorsEdges.first];
		long long w = 0;

		if (graph.getWeight(edge.targetID, edge.sourceID, w))
			twoDirGraph.addEdge(edge);
		else oneDirEdges.addEdge(edge);
	}
}

/*size_t GraphGenerator::countDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source)
{
	size_t result = 0;
	vector<size_t> term;

	for (size_t i = 0; i < _terminals.size(); i++)
	{
		term.push_back(_terminals[i]);
		DijkstraSolver ds(graph, term, _source);
		BoostGraph currResult = ds.solutionDijkstra();
		if (boost::num_edges(currResult) == 0)
			return 0;
		//cout << currResult << endl;
		size_t summDelay = countSummDelay(currResult);
		term.clear();

		if (summDelay > result)
			result = summDelay;
	}
	return result;
}*/

/*size_t GraphGenerator::countMinDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source)
{
	size_t result = 0;
	BoostGraph delayGraph;
	//change weight -> delay
	auto iteratorsEdges = boost::edges(graph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto edge = graph[*iteratorsEdges.first];
		delayGraph.addEdge(edge.sourceID, edge.targetID, delays[findIdEdgeBoostGraphVector(edge)]);
	}
	result = countDelay(delayGraph, _terminals, _source);
	delayGraph.clear();
	return result;
}*/

/*size_t GraphGenerator::findIdEdgeBoostGraphVector(Edge e)
{
	for (size_t i = 0; i < startV.size(); i++)
	{
		if (startV[i] == e.sourceID && finishV[i] == e.targetID)
			return i;
		if (startV[i] == e.targetID && finishV[i] == e.sourceID)
			return i;
	}
	return startV.size();
}*/

/*size_t GraphGenerator::countSummDelay(BoostGraph& graph)
{
	size_t result = 0;
	auto iteratorsEdges = boost::edges(graph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto edge = graph[*iteratorsEdges.first];
		result += delays[findIdEdgeBoostGraphVector(edge)];
	}
	return result;
}*/

/*size_t GraphGenerator::countMaxDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source)
{
	return countDelay(graph, _terminals, _source);
}*/

void GraphGenerator::setStatusScenario(bool status, size_t numberScenario)
{
	if(numberScenario == 1)
		statusOneScenario = status;
	else if(numberScenario == 2)
		statusTwoScenario = status;
	else if (numberScenario == 3)
		statusThreeScenario = status;
	else if (numberScenario == 4)
		statusFourScenario = status;
	else if (numberScenario == 5)
		statusFiveScenario = status;
	else if (numberScenario == 6)
		statusSixScenario = status;
}

void GraphGenerator::writeSolutionScenarioOne(BoostGraph& resultGraph, ofstream& outFile)
{
	writeSolution(outFile, resultGraph, 1, terminals, source, cost);
}

void GraphGenerator::writeSolutionScenarioTwo(BoostGraph& resultGraph, ofstream& outFile)
{
	writeSolution(outFile, resultGraph, 2, terminals, backupSource, costTwoScenario);
}

void GraphGenerator::writeSolutionScenarioThree(BoostGraph& resultGraph, ofstream& outFile)
{
	writeSolution(outFile, resultGraph, 3, terminals, sourceProtectionPath, costThreeScenario);
}

void GraphGenerator::writeSolutionScenarioFive(BoostGraph& resultGraph, ofstream& outFile)
{

	vector<size_t> term = terminals;
	term.insert(term.end(), newTerminals.begin(), newTerminals.end());

	writeSolution(outFile, resultGraph, 5, term, source, costFiveScenario);
}

void GraphGenerator::writeSolutionScenarioSix(BoostGraph& resultGraph, ofstream& outFile)
{

	vector<size_t> term;
	for (size_t i = 0; i < terminals.size(); i++)
	{
		if (checkDouble(deleteTerminals, terminals[i]) == -1)
		{
			term.push_back(terminals[i]);
		}
	}
	writeSolution(outFile, resultGraph, 6, term, source, costSixScenario);
}

void GraphGenerator::writeSolution(ofstream& outFile, BoostGraph& resultGraph, 
	size_t number, vector<size_t>& _terminals, size_t _source, size_t& _cost)
{
	outFile << "Scenario=" << number << endl;
	outFile << "Source=" << _source << endl;
	outFile << "Terminals=";

	for(size_t i=0 ; i< _terminals.size();i++)
		outFile << _terminals[i] << " ";
	outFile << endl;

	outFile << "Cost=" << _cost << endl;
	outFile << "Solution=" << endl;
	outFile << resultGraph << endl << endl;
}

BoostGraph GraphGenerator::addTwoDirEdges(BoostGraph& graph)
{
	BoostGraph result;
	auto iteratorsEdges = boost::edges(graph);
	for (; iteratorsEdges.first != iteratorsEdges.second; ++iteratorsEdges.first)
	{
		auto edge = graph[*iteratorsEdges.first];
		long long w = 0;

		result.addEdge(edge);
		if (!graph.getWeight(edge.targetID, edge.sourceID, w))
			result.addEdge(edge.targetID, edge.sourceID, edge.cost);
	}
	return result;
}