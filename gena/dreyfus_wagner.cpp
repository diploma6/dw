#include <limits>
#include <chrono>

#include "dreyfus_wagner.h"

//#define BENCHMARK

#ifdef BENCHMARK
#include <iostream>
#endif

#ifdef _MSC_VER
#include <intrin.h>

using uint = unsigned int;

static inline int __builtin_ctz(unsigned x) {
    unsigned long ret;
    _BitScanForward(&ret, x);

    return (int)ret;
}
#endif


const size_t DreyfusWagnerSolver::maxUint = std::numeric_limits<size_t>::max();


Solution DreyfusWagnerSolver::solve(Task const& task)
{
    _terminals = task.terminals;
    _source = task.source;
    _task = task.graph;

    return buildMinSpanTree();
}

void DreyfusWagnerSolver::buildMinSpanTree1Vert(size_t terminal, BoostGraph& resMinSpanTree) const
{
    resMinSpanTree = BoostGraph();
    resMinSpanTree.addVertex(terminal);
}

void DreyfusWagnerSolver::buildMinSpanTree2Vert(size_t terminal0, size_t terminal1, BoostGraph& resMinSpanTree) const
{
    if (terminal0 == terminal1)
    {
        buildMinSpanTree1Vert(terminal0, resMinSpanTree);
        return;
    }

    // shortest path
    resMinSpanTree =
        _task.shortestPath(
            terminal0, terminal1,
            precomputedPaths.empty() ? BoostGraph::BoostPath() :
            precomputedPaths.at(_task.getBoostVByOrigV(terminal0)));
}

size_t DreyfusWagnerSolver::combinationIndex(size_t terminalComb, size_t w, size_t vertexCount) const
{
    return terminalComb * vertexCount + w;
}

Solution DreyfusWagnerSolver::buildMinSpanTree()
{
    BoostGraph resMinSpanTree;

    auto& m_terminals = _terminals;
    m_terminals.push_back(_source);
    std::sort(m_terminals.begin(), m_terminals.end());

    // trivial cases
    if (mstTrivialCases(m_terminals, resMinSpanTree))
        return { resMinSpanTree.cost(), resMinSpanTree };

#ifdef BENCHMARK
    std::chrono::high_resolution_clock clock;
    auto start = clock.now();
    std::cout << "Precomputation of shortest paths started...\n";
#endif

    precomputeDijkstra();

#ifdef BENCHMARK
    auto end = clock.now();
    std::cout << "Precomputing shortest paths time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms\n";
    std::cout << "Build MST started...\n";
    start = clock.now();
#endif

    // main branch
    auto vc = _task.vertexCount();
    size_t m_tsize = m_terminals.size();

    // storage logic:
    // combination | <> - for combination of only terminals
    // combination | v - for other

    //1 << m_tsize === 2 ^ m_tsize:
    // Cn0 + Cn1 + ... + Cnn
    auto combCount = vc * (1ULL << m_tsize);

    solution_costs = new (std::nothrow) SolutionData[combCount];

    if (solution_costs == nullptr)
        return { 0, resMinSpanTree };

    // TODO check maybe it is OK with map
    // TODO maybe rewrite combinationNumber in order to allocate
    // 1 << (m_tsize - 1) memory (really there is used combinations
    // only for |T| - 1


    mst1Terminal(m_terminals, m_tsize, vc);

    long long z;
    long long* distances;

    size_t
        bipartition1_comb,
        bipartition2_comb;

    size_t t1_cost, t2_cost;
    size_t cost;
    //size_t* distances;

    size_t idx;

    size_t x1_num = 0;
    SolutionData* curMin;

    // term_size <-> |X|
    // pay attention that we build st(X U v) => 3 - 1, size -1
    for (size_t term_size = 2; term_size <= m_tsize - 1; term_size++)
    {
        //auto term_combinations = combinations(term_size);
        // enumerate all X: |X| == term_size
        for (long long x_comb = (1ULL << term_size) - 1;
            x_comb < (1LL << m_tsize);
            z = x_comb | (x_comb - 1), x_comb = (z + 1) | (((~z & -~z) - 1) >> (__builtin_ctz(uint(x_comb)) + 1)))
        {
            idx = combinationIndex(x_comb, 0, vc);
            curMin = solution_costs + idx;

            // generate all x1 - subsets of x
            // TODO not generate secoind half because of symmetry
            for (bipartition1_comb = x_comb & -x_comb, x1_num = 0; x1_num < size_t(1ULL << (term_size - 1))/*bipartition1_comb != x_comb*/; bipartition1_comb = x_comb & (bipartition1_comb - x_comb), x1_num++)
            {
                // conjugated combination
                bipartition2_comb = x_comb & ~bipartition1_comb;

                auto
                    solution_costs1 = solution_costs + combinationIndex(bipartition1_comb, 0, vc),
                    solution_costs2 = solution_costs + combinationIndex(bipartition2_comb, 0, vc);

                // enumerate all v in V
                for (size_t v = 0; v < vc; v++)
                {
                    // T(x1 U w)
                    t1_cost = solution_costs1[v].weight;

                    // T(x2 U w)
                    t2_cost = solution_costs2[v].weight;

                    cost = t1_cost + t2_cost;

                    if (cost < curMin[v].weight)
                    {
                        // minimization!
                        curMin[v].bipartition1_comb = bipartition1_comb;
                        curMin[v].w = v;
                        curMin[v].weight = cost;
                    }
                }
            }

            for (size_t v = 0; v < vc; v++)
            {
                distances = precomputedPaths[v].distances.data();
                for (size_t u = 0; u < vc; u++)
                {
                    // P_vw
                    cost = distances[u] + curMin[v].weight;
                    if (cost < curMin[u].weight)
                    {
                        curMin[u].bipartition1_comb = x_comb;
                        curMin[u].weight = cost;
                        curMin[u].w = v;
                    }
                }
            }
        }
    }

    // solution only for m_terminals
    size_t term_comb = (1ULL << (m_tsize - 1)) - 1;

    idx = combinationIndex(term_comb, _task.getBoostVByOrigV(m_terminals.back()), vc);
    resMinSpanTree = mstFull(vc, m_tsize, m_terminals.back());

    if(resMinSpanTree.vertexCount() == 0)
        return { 0, resMinSpanTree };

    cost = solution_costs[idx].weight;
    delete[]solution_costs;

#ifdef BENCHMARK
    end = clock.now();
    std::cout << "Build MST time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms\n";
#endif

    return { cost, resMinSpanTree };
}

BoostGraph DreyfusWagnerSolver::mstFull(size_t vc, size_t m_tsize, size_t terminalBack) const
{
    BoostGraph mst;
    mstFullRec((1ULL << (m_tsize - 1)) - 1, vc, m_tsize, _task.getBoostVByOrigV(terminalBack), mst);

    return mst;
}

// v - BOOST number
void DreyfusWagnerSolver::mstFullRec(size_t x_comb, size_t vc, size_t m_tsize, size_t v, BoostGraph& mst) const
{
    if (x_comb == 0 || v == maxUint)
        return;

    auto idx = combinationIndex(x_comb, v, vc);
    auto u = solution_costs[idx].w;

    if (v != u)
    {
        BoostGraph p_vu, oldmst = std::move(mst);
        buildMinSpanTree2Vert(_task[v], _task[u], p_vu);
        if (p_vu.vertexCount() == 0)
            return;
        BoostGraph::merge(p_vu, oldmst, mst);
        mstFullRec(solution_costs[idx].bipartition1_comb, vc, m_tsize, u, mst);
    }
    else
    {
        size_t
            x1_comb = solution_costs[idx].bipartition1_comb,
            x2_comb = x_comb & ~x1_comb;

        if (x_comb == x1_comb)
            return;

        mstFullRec(x1_comb, vc, m_tsize, u, mst);
        mstFullRec(x2_comb, vc, m_tsize, u, mst);
    }
}

bool DreyfusWagnerSolver::mstTrivialCases(std::vector<size_t> const& m_terminals, BoostGraph& resMinSpanTree)
{
    uint m_tsize = uint(m_terminals.size());

    // trivial cases
    if (m_tsize == 1)
    {
        buildMinSpanTree1Vert(m_terminals[0], resMinSpanTree);
        return true;
    }

    if (m_tsize == 2)
    {
        buildMinSpanTree2Vert(m_terminals[0], m_terminals[1], resMinSpanTree);
        return true;
    }
    return false;
}

void DreyfusWagnerSolver::mst1Terminal(
    std::vector<size_t> const& m_terminals,
    size_t m_tsize, size_t vertexCount)
{
    size_t idx;
    for (size_t t = 0; t < m_tsize; t++)
    {
        auto boostT = _task.getBoostVByOrigV(m_terminals[t]);
        auto distances =
            precomputedPaths[boostT].distances.data();
        for (size_t v = 0; v < vertexCount; v++)
        {
            idx = combinationIndex(1ULL << t, v, vertexCount);
            solution_costs[idx].weight = distances[v];
            solution_costs[idx].w = boostT;
            solution_costs[idx].bipartition1_comb = 1ULL << t;
        }
    }
}

void DreyfusWagnerSolver::precomputeDijkstra(void)
{
    auto vc = _task.vertexCount();
    precomputedPaths.resize(vc);

    // t is boost vertex number
    for (uint t = 0; t < vc; t++)
        try {
        precomputedPaths[t] = _task.shortestPaths(_task[t]);
    }
    catch (std::bad_alloc&)
    {
        // no precomputation if no memory
        precomputedPaths.clear();
        break;
    }
}

size_t DreyfusWagnerSolver::shortestPathCost(size_t v, size_t w) const
{
    if (v == w)
        return 0;
    return precomputedPaths.
        at(_task.getBoostVByOrigV(v)).
        distances[_task.getBoostVByOrigV(w)];
}
