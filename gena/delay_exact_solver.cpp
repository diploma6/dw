#include <stack>
#include <list>
#include <boost/graph/floyd_warshall_shortest.hpp>
#include <iostream>

#include "delay_exact_solver.h"
#include "dspt_solver.h"

const size_t DelayExactSolver::MAX_COMBINATIONS = 1000000000;

void DelayExactSolver::initInternal(const Task& task)
{
    _source = task.source;
    _delayLimit = task.delayLimit;
    _terminals = &task.terminals;
} // end of 'DelayExactSolver::initInternal' function

void DelayExactSolver::reduceGraph(const BoostGraph& graph)
{
    _reducedGraph = graph;

    DistanceMatrix distanceMatr;
    std::map<BoostGraph::vertex_descriptor, long long>
        lambda, mu;
    std::vector<bool> isRemoved(graph.vertexCount(), false);

    bool haveReduced;
    do
    {
        haveReduced = false;

        // calculate shortest delay for each vertex pair
        boost::floyd_warshall_all_pairs_shortest_paths(
            _reducedGraph,
            distanceMatr,
            // delay as cost function
            weight_map(boost::get(&Edge::delay, _reducedGraph))
        );

        // calculate lambda and mu for each vertex
        auto vPair = boost::vertices(_reducedGraph);
        for (auto vIt = vPair.first; vIt != vPair.second; vIt++)
        {
            auto v = *vIt;

            // skip already removed vertices
            if (isRemoved[v])
                continue;

            // lower delay bound
            lambda[v] = distanceMatr[_reducedGraph.getBoostVByOrigV(_source)][v];

            // upper delay boubd
            mu[v] = std::numeric_limits<long long>::max();
            for (auto t : *_terminals)
                mu[v] = std::min(mu[v], distanceMatr[v][_reducedGraph.getBoostVByOrigV(t)]);
            mu[v] = _delayLimit - mu[v];

            // remove vertex if lower bound exceeds upper bound
            if (lambda[v] > mu[v])
            {
                haveReduced = true;
                // only remove incident edges to preserve vertex descriptors
                boost::clear_vertex(v, _reducedGraph);
                isRemoved[v] = true;
            }
        }

        // remove redundant edges
        auto predicate = [&](const BoostGraph::edge_descriptor& e) -> bool
        {
            bool res = lambda[e.m_source] + _reducedGraph[e].delay > mu[e.m_target];

            if (res)
                haveReduced = true;

            return res;
        };

        boost::remove_edge_if(predicate, _reducedGraph);
    } while (haveReduced);
} // end of 'DelayExactSolver::reduceGraph' function

std::vector<DelayExactSolver::PathsVector>
DelayExactSolver::findEveryPath() const
{
    const size_t vc = _reducedGraph.vertexCount();
    std::vector<PathsVector> result(_terminals->size());
    BoostGraph::BoostPath curBoostPath =
    {
        BoostGraph::BoostPredecessorMap(vc),
        std::vector<long long>(vc, std::numeric_limits<long long>::max()),
        0
    };
    std::vector<size_t> curPath(vc, 0);
    std::vector<bool> visited(vc, false);
    size_t curLen = 0;

    struct Node
    {
        size_t vertex, length;
        long long delay, distance;
    };
    std::stack<Node, std::list<Node>> nodeStack;

    auto getBoostParent = [&](size_t i) -> size_t
    {
        if (i == 0)
            return _reducedGraph.getBoostVByOrigV(curPath[0]);
        return _reducedGraph.getBoostVByOrigV(curPath[i - 1]);
    };

    auto findIndex = [](size_t x, const std::vector<size_t>& vec) -> size_t
    {
        for (size_t i = 0; i < vec.size(); i++)
            if (x == vec[i])
                return i;
        return vec.size();
    };

    nodeStack.push({ _source, 1, 0, 0 });
    while (nodeStack.size() > 0)
    {
        // pop vertex
        Node node = nodeStack.top();
        nodeStack.pop();

        // clean up old path if returned back
        if (node.length <= curLen)
            for (size_t i = node.length - 1; i < curLen; i++)
            {
                size_t v = _reducedGraph.getBoostVByOrigV(curPath[i]);
                curBoostPath.predecessors[v] = v;
                visited[curPath[i]] = false;
            }

        // add new vertex to current path
        curLen = node.length;
        curPath[curLen - 1] = node.vertex;
        visited[node.vertex] = true;
        auto curBoostV = _reducedGraph.getBoostVByOrigV(node.vertex);
        size_t boostParent = getBoostParent(curLen - 1);
        curBoostPath.predecessors[curBoostV] = boostParent;
        curBoostPath.distances[curBoostV] = node.distance;

        // if encountered a terminal save path to set
        size_t i = findIndex(node.vertex, *_terminals);
        if (i != _terminals->size())
            result[i].push_back(curBoostPath);

        // add neighbours to stack
        auto ePair = _reducedGraph.outEdges(node.vertex);
        for (auto e = ePair.first; e != ePair.second; e++)
        {
            const auto& edge = _reducedGraph[*e];

            // check not visited and delay constraint not broken
            if (visited[edge.targetID] ||
                node.delay + edge.delay > _delayLimit)
                continue;

            // add node to stack
            nodeStack.push({ edge.targetID, node.length + 1,
                node.delay + edge.delay, node.distance + edge.cost });
        }
    }

    return result;
} // end of 'DelayExactSolver::findEveryPath' function

void DelayExactSolver::findMinCostTree()
{
    size_t vc = _reducedGraph.vertexCount();

    BoostGraph::BoostPath tree =
    {
        BoostGraph::BoostPredecessorMap(vc), // predecessors
        std::vector<long long>(vc),          // distances
        0                                    // cost
    };

    _curCombination = 0;
    try
    {
        tryCombination(tree, 0);
    }
    catch (const TooManyCombinationsException&)
    {
    }
} // end of 'DelayExactSolver::findMinCostTree' function

void DelayExactSolver::tryCombination(const BoostGraph::BoostPath& curTree,
    size_t curTerminalIndex)
{
    // iterate paths to current terminal
    for (const auto& path : _terminalPaths[curTerminalIndex])
    {
        try
        {
            // merge path into current tree
            const auto tree = curTree.getMerged(path,
                _reducedGraph.getBoostVByOrigV((*_terminals)[curTerminalIndex]));

            // skip futher merging if already exceed current minimum cost
            if (tree.cost >= _curMinTree.cost)
                continue;

            // keep merging paths to next terminals
            if (curTerminalIndex < _terminals->size() - 1)
                tryCombination(tree, curTerminalIndex + 1);
            else // tree already contains every terminal
            {
                if (tree.cost < _curMinTree.cost)
                    _curMinTree = tree;

                if (++_curCombination > MAX_COMBINATIONS)
                {
                    std::cout << "Number of checked trees exceeds " << MAX_COMBINATIONS << ". Stopping search and using current optimum.\n";
                    throw TooManyCombinationsException();
                }
            }
        }
        catch (const BoostGraph::BoostPath::NotATreeException&)
        {
            // paths don't form a tree
            continue;
        }
    }
} // end of 'DelayExactSolver::tryCombination' function

Solution DelayExactSolver::solve(Task const& task)
{
    // init internal variables
    initInternal(task);

    // graph preprocessing
    reduceGraph(task.graph);

    // find initial optimum approximation
    Task reducedTask = task;
    reducedTask.graph = _reducedGraph;
    _curMinTree = DSPTSolver().makeDSPT(reducedTask);
    _curMinTree.cost = _reducedGraph.buildTreeSubgraph(_source,
        *_terminals, _curMinTree).cost();

    // find every path to each terminal
    _terminalPaths = findEveryPath();

    // try every path combination
    findMinCostTree();

    // build solution
    auto tree = _reducedGraph.buildTreeSubgraph(_source, *_terminals, _curMinTree);
    return { tree.cost(), std::move(tree) };
} // end of 'DelayExactSolver::solve' function
