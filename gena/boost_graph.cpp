#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include "boost_graph.h"

void BoostGraph::clear()
{
    std::vector<Edge> edges(boost::num_edges(*this));
    size_t idx = 0;
    applyEdges([&edges, &idx](Edge const& edge) {edges[idx++] = edge; });

    std::vector<Vertex> vertices(boost::num_vertices(*this));
    idx = 0;
    applyVertices([&vertices, &idx](Vertex const& v) {vertices[idx++] = v; });

    for (auto& e : edges)
        removeEdge(e);

    for (auto& v : vertices)
        removeVertex(v);
}
void BoostGraph::addVertex(size_t v)
{
    auto vd = boost::add_vertex(*this);
    (*this)[vd] = Vertex{ v };
    _mapOrigVToBoostV[v] = vd;
}

void BoostGraph::addEdge(const Edge& e, bool checkExists)
{
    // if vertices do not exist, add
    if (_mapOrigVToBoostV.find(e.sourceID) == _mapOrigVToBoostV.end())
        addVertex(e.sourceID);

    if (_mapOrigVToBoostV.find(e.targetID) == _mapOrigVToBoostV.end())
        addVertex(e.targetID);

    std::pair<BoostGraph::edge_descriptor, bool> checkExistsEdge;

    if (checkExists)
        checkExistsEdge = boost::edge(_mapOrigVToBoostV.at(e.sourceID), _mapOrigVToBoostV.at(e.targetID), *this);
    else
        checkExistsEdge.second = false;

    // if edge exists, only change weight and delay
    if (checkExistsEdge.second)
    {
        (*this)[checkExistsEdge.first].cost = e.cost;
        (*this)[checkExistsEdge.first].delay = e.delay;
        return;
    }
    auto arc = boost::add_edge(_mapOrigVToBoostV[e.sourceID],
        _mapOrigVToBoostV[e.targetID], *this);
    (*this)[arc.first] = e;
}

void BoostGraph::addEdge(size_t sourceID, size_t targetID, long long cost,
    long long delay, bool checkExists)
{
    addEdge({ sourceID, targetID, cost, delay }, checkExists);
}

void BoostGraph::addEdge(size_t sourceID, size_t targetID, long long cost, bool checkExists)
{
    addEdge({ sourceID, targetID, cost, 0 }, checkExists);
}

void BoostGraph::removeEdge(size_t sourceID, size_t targetID, bool checkUnique)
{
    if (checkUnique)
        if (getParallelEdges(sourceID, targetID).size() > 1)
            throw std::runtime_error("removeEdge: edge is not unique");
    boost::remove_edge(_mapOrigVToBoostV.at(sourceID), _mapOrigVToBoostV.at(targetID), *this);
}

void BoostGraph::removeEdge(const Edge& edge, bool checkUnique)
{
    if (!checkUnique)
        boost::remove_edge(_mapOrigVToBoostV.at(edge.sourceID), _mapOrigVToBoostV.at(edge.targetID), *this);
    else
        boost::remove_out_edge_if(_mapOrigVToBoostV.at(edge.sourceID),
            [this, &edge](BoostGraph::edge_descriptor const& it)
            {
                return (*this)[it] == edge;
            }, *this);
}

bool BoostGraph::getEdge(size_t sourceID, size_t targetID, Edge& edge) const
{
    if (!_mapOrigVToBoostV.count(sourceID) || !_mapOrigVToBoostV.count(targetID))
        return false;

    auto e = boost::edge(_mapOrigVToBoostV.at(sourceID), _mapOrigVToBoostV.at(targetID), *this);
    if (!e.second)
        return false;

    edge = (*this)[e.first];
    return true;
}

bool BoostGraph::getWeight(size_t sourceID, size_t targetID, long long& weight) const
{
    Edge edge;
    bool result = getEdge(sourceID, targetID, edge);

    if (result)
        weight = edge.cost;

    return result;
}

bool BoostGraph::getDelay(size_t sourceID, size_t targetID, long long& delay) const
{
    Edge edge;
    bool result = getEdge(sourceID, targetID, edge);

    if (result)
        delay = edge.delay;

    return result;
}

bool BoostGraph::setDelay(size_t sourceID, size_t targetID, long long delay)
{
    if (!_mapOrigVToBoostV.count(sourceID) || !_mapOrigVToBoostV.count(targetID))
        return false;

    auto e = boost::edge(_mapOrigVToBoostV.at(sourceID), _mapOrigVToBoostV.at(targetID), *this);
    if (!e.second)
        return false;

    (*this)[e.first].delay = delay;
    return true;
}

std::vector<Edge> BoostGraph::getParallelEdges(size_t sourceID, size_t targetID) const
{
    std::vector<Edge> edges;
    if (!contains(sourceID) || !contains(targetID))
        return edges;

    auto out_edges = outEdges(sourceID);

    for (auto e : boost::make_iterator_range(out_edges))
    {
        auto edge = (*this)[e];
        if (edge.targetID == targetID)
            edges.emplace_back(edge);
    }

    return edges;
}

size_t BoostGraph::vertexCount() const
{
    return boost::num_vertices(*this);
}

void BoostGraph::removeVertex(const size_t vertex)
{
    auto descriptor = _mapOrigVToBoostV.at(vertex);
    boost::remove_vertex(descriptor, *this);
    recomputeMapOrigToBoost();
}

std::pair<BoostGraph::out_edge_iterator,
    BoostGraph::out_edge_iterator> BoostGraph::outEdges(size_t vertexID) const
{
    return boost::out_edges(_mapOrigVToBoostV.at(vertexID), *this);
}

std::pair<BoostGraph::in_edge_iterator,
    BoostGraph::in_edge_iterator> BoostGraph::inEdges(size_t vertexID) const
{
    return boost::in_edges(_mapOrigVToBoostV.at(vertexID), *this);
}

size_t BoostGraph::cost() const
{
    long long cost = 0;
    applyEdges([&cost](Edge const& edge) { cost += edge.cost; });

    if (cost < 0)
        throw std::runtime_error("Tree with negate cost");
    return static_cast<size_t>(cost);
}

long long BoostGraph::delay() const
{
    long long delay = 0;
    applyEdges([&delay](Edge const& edge) { delay += edge.delay; });

    return delay;
}

size_t BoostGraph::getBoostVByOrigV(size_t v) const
{
    return _mapOrigVToBoostV.at(v);
}

bool BoostGraph::contains(size_t vertex) const
{
    return _mapOrigVToBoostV.count(vertex) != 0;
}

bool BoostGraph::contains(const Edge& edge, bool weightCheck) const
{
    if (!weightCheck)
    {
        long long w;
        return getWeight(edge.sourceID, edge.targetID, w);
    }
    auto edges = getParallelEdges(edge.sourceID, edge.targetID);
    return std::find(edges.begin(), edges.end(), edge) != edges.end();
}
BoostGraph::BoostPredecessorMap::BoostPredecessorMap(size_t size) :
    std::vector<BoostVertex>(size)
{
    for (size_t i = 0; i < size; i++)
        at(i) = i;
}

bool BoostGraph::BoostPredecessorMap::isLoop(size_t startV) const
{
    size_t curV = startV;
    while (at(curV) != curV)
    {
        if (at(curV) == startV)
            return true;
        curV = at(curV);
    }
    return false;
}

BoostGraph::BoostPredecessorMap BoostGraph::BoostPredecessorMap::getReversePath(size_t startV) const
{
    BoostPredecessorMap result(size());
    for (size_t i = 0; i < result.size(); i++)
        result[i] = i;

    size_t cur = startV;
    while (at(cur) != cur)
    {
        result[at(cur)] = cur;
        cur = at(cur);
    }

    return result;
}

void BoostGraph::BoostPredecessorMap::calculateDistance(size_t startV,
    const std::function<long long(size_t srcV, size_t trgV)>& metric,
    std::vector<long long>& output) const
{
    size_t curV = startV;
    while (at(curV) != curV)
    {
        output[at(curV)] = output[curV] + metric(curV, at(curV));
        curV = at(curV);
    }
}

BoostGraph::BoostPath BoostGraph::shortestPaths(size_t src) const
{
    auto vc = boost::num_vertices(*this);
    // Output for predecessors of each node in the shortest path tree result
    BoostPredecessorMap predMap(vc);
    // Output for distances for each node with initial size
    // of number of vertices
    std::vector<long long> distMap(vc);

    BoostVertex startV = _mapOrigVToBoostV.at(src);
    // BoostVertex endV = boost::vertex(terminal, g);

    //solve shortest path problem
    boost::dijkstra_shortest_paths(*this, startV,
        weight_map(boost::get(&Edge::cost, *this)) //arc costs from bundled properties
        .predecessor_map(boost::make_iterator_property_map(predMap.begin(), //property map style
            boost::get(boost::vertex_index, *this)))
        .distance_map(boost::make_iterator_property_map(distMap.begin(), //property map style
            boost::get(boost::vertex_index, *this)))
    );

    return { predMap, distMap }; // Check NRVO
}

BoostGraph BoostGraph::shortestPath(size_t src, size_t dst, BoostGraph::BoostPath const& paths) const
{
    if (paths.predecessors.empty())
    {
        auto dijkstra = shortestPaths(src);
        auto predecessors = dijkstra.predecessors;
        return buildPathSubgraph(shortestPathAsVector(src, dst, predecessors), dijkstra.distances);
    }

    return buildPathSubgraph(shortestPathAsVector(src, dst, paths.predecessors), paths.distances);
}

BoostGraph::BoostPath BoostGraph::negateShortestPaths(size_t src) const
{
    auto vc = boost::num_vertices(*this);
    // Output for predecessors of each node in the shortest path tree result
    BoostPredecessorMap predMap(vc);
    // Output for distances for each node with initial size
    // of number of vertices
    std::vector<long long> distMap(vc, std::numeric_limits<long long>::max());

    // mention start vertex
    distMap[_mapOrigVToBoostV.at(src)] = 0;
    // BoostVertex endV = boost::vertex(terminal, g);

    //solve shortest path problem
    bool rc = boost::bellman_ford_shortest_paths(*this, vc,
        weight_map(boost::get(&Edge::cost, *this)) //arc costs from bundled properties
        .predecessor_map(boost::make_iterator_property_map(predMap.begin(),//property map style
            boost::get(boost::vertex_index, *this)))
        .distance_map(boost::make_iterator_property_map(distMap.begin(),//property map style
            boost::get(boost::vertex_index, *this)))
    );

    if (!rc)
        throw std::runtime_error("Bellman-Ford: Negative cycle in graph provided");

    return { predMap, distMap }; // Check NRVO
}

void BoostGraph::merge(const BoostGraph& s1, const BoostGraph& s2, BoostGraph& s)
{
    s = s1;
    merge(s, s2);
}

void BoostGraph::merge(BoostGraph&& s1, BoostGraph const& s2, BoostGraph& s)
{
    s = std::move(s1);
    merge(s, s2);
}

void BoostGraph::merge(BoostGraph const& s)
{
    BoostGraph res;
    BoostGraph::merge(s, *this, res);
    *this = std::move(res);
}

void BoostGraph::merge(BoostGraph&& s)
{
    BoostGraph res;
    BoostGraph::merge(std::move(s), *this, res);
    *this = std::move(res);
}

void BoostGraph::applyEdges(std::function<void(Edge&)> visitor)
{
    for (auto e : boost::make_iterator_range(boost::edges(*this)))
        visitor((*this)[e]);
}

void BoostGraph::applyEdges(std::function<void(const Edge&)> visitor) const
{
    for (auto const e : boost::make_iterator_range(boost::edges(*this)))
        visitor((*this)[e]);
}

void BoostGraph::applyVertices(std::function<void(Vertex&)> visitor)
{
    for (auto const e : boost::make_iterator_range(boost::vertices(*this)))
        visitor((*this)[e]);
}

void BoostGraph::applyVertices(std::function<void(const Vertex&)> visitor) const
{
    for (auto const e : boost::make_iterator_range(boost::vertices(*this)))
        visitor((*this)[e]);
}

void BoostGraph::recomputeMapOrigToBoost()
{
    _mapOrigVToBoostV.clear();

    for (auto vertices = boost::vertices(*this); vertices.first != vertices.second; ++vertices.first)
        _mapOrigVToBoostV[(*this)[*vertices.first]] = *vertices.first;
}

std::vector<size_t> BoostGraph::shortestPathAsVector(size_t src, size_t dst,
    std::vector<size_t> const& predecessors) const
{
    std::vector<size_t> path, empty;

    src = _mapOrigVToBoostV.at(src);
    dst = _mapOrigVToBoostV.at(dst);

    path.push_back(dst);

    auto v = dst;

    while (v != predecessors[v])
    {
        path.push_back(predecessors[v]);
        v = predecessors[v];
    }

    if (v != src)
        return empty;

    std::reverse(path.begin(), path.end());
    return path;
}

BoostGraph BoostGraph::buildPathSubgraph(
    std::vector<size_t> const& path,
    std::vector<long long> const& distMap) const
{
    BoostGraph t;

    size_t path_size = path.size();
    if (path_size == 0)
        return t;

    for (size_t i = 0; i < path_size - 1; i++)
    {
        long long w;
        if (!contains((*this)[path[i]]) || !contains((*this)[path[i + 1]]))
            throw std::runtime_error("Path vertex does not exist");
        // assert that edge exists
        bool edgeExists = getWeight((*this)[path[i]], (*this)[path[i + 1]], w);
        if (!edgeExists)
            throw std::runtime_error("Path edge does not exist");
        auto cost = distMap[path[i + 1]] - distMap[path[i]];
        auto delay = (*this)[boost::edge(path[i], path[i + 1], *this).first].delay;
        t.addEdge((*this)[path[i]], (*this)[path[i + 1]], cost, delay);
    }

    return t;
}

BoostGraph BoostGraph::buildTreeSubgraph(size_t source,
    const std::vector<size_t>& terminals,
    const BoostGraph::BoostPath& tree) const
{
    BoostGraph subgraph;
    for (size_t t : terminals)
        subgraph.merge(shortestPath(source, t, tree));

    return subgraph;
}

std::vector<size_t> BoostGraph::vertices() const
{
    std::vector<size_t> vertices;
    for (auto v : boost::make_iterator_range(boost::vertices(*this)))
        vertices.push_back((*this)[v]);

    return vertices;
}

size_t BoostGraph::inDegree(Vertex v) const
{
    return boost::in_degree(_mapOrigVToBoostV.at(v), *this);
}

size_t BoostGraph::outDegree(Vertex v) const
{
    return boost::out_degree(_mapOrigVToBoostV.at(v), *this);
}

long long BoostGraph::getMinDelayLimit(size_t source, const std::vector<size_t>& terminals) const
{
    // find delayLimit trough dijkstra
    std::vector<long long> minDelayMap = std::vector<long long>(vertexCount());
    boost::dijkstra_shortest_paths(
        *this,
        // start from source
        getBoostVByOrigV(source),
        // delay as cost function
        weight_map(boost::get(&Edge::delay, *this))
        // record to 'minDelayMap'
        .distance_map(boost::make_iterator_property_map(
            minDelayMap.begin(),
            boost::get(boost::vertex_index, *this)))
    );
    long long minDelayLimit = 0;
    for (size_t t : terminals)
        minDelayLimit = std::max(minDelayLimit, minDelayMap[getBoostVByOrigV(t)]);

    return minDelayLimit;
}

long long BoostGraph::getMaxDelayLimit(size_t source, const std::vector<size_t>& terminals) const
{
    BoostGraph::BoostPredecessorMap predMap(vertexCount());
    boost::dijkstra_shortest_paths(
        *this,
        // start from source
        getBoostVByOrigV(source),
        // delay as cost function
        weight_map(boost::get(&Edge::cost, *this))
        // record to 'predMap'
        .predecessor_map(boost::make_iterator_property_map(
            predMap.begin(),
            boost::get(boost::vertex_index, *this)))
    );
    long long maxDelayLimit = 0;
    std::vector<long long> delay(vertexCount(), 0);
    for (size_t t : terminals)
    {
        predMap.calculateDistance(getBoostVByOrigV(t),
            [&](size_t src, size_t dst) -> size_t
            {
                return (*this)[boost::edge(dst, src, *this).first].delay;
            }, delay);

        maxDelayLimit = std::max(maxDelayLimit, delay[getBoostVByOrigV(source)]);
    }

    return maxDelayLimit;
}

void BoostGraph::merge(BoostGraph& s, const BoostGraph& s2)
{
    BoostGraph::edge_iterator
        e2_end, e2;

    BoostGraph::out_edge_iterator
        e_end, e;
    bool exists;

    for (boost::tie(e2, e2_end) = boost::edges(s2); e2 != e2_end; e2++)
    {
        exists = false;

        size_t v = s2[*e2].sourceID;

        if (!s.contains(v))
            exists = false;
        else
        {
            for (boost::tie(e, e_end) = s.outEdges(s2[*e2].sourceID); e != e_end; ++e)
                if (s[*e] == s2[*e2])
                {
                    exists = true;
                    break;
                }
        }

        if (!exists)
            s.addEdge(s2[*e2]);
    }
}

std::ostream& operator<<(std::ostream& os, BoostGraph const& g)
{
    os << "Vertices:\n";
    for (auto iterators = boost::vertices(g); iterators.first != iterators.second; ++iterators.first)
    {
        auto v = g[*iterators.first];
        os << v << " ";
    }
    os << "Vertex count: " << boost::num_vertices(g) << "\n";
    os << "Edge count: " << boost::num_edges(g) << "\n";

    os << "\nEdges:\n";
    g.applyEdges(
        [&os](Edge const& edge)
        {
            os << edge.sourceID << ' ' << edge.targetID << ' ' << edge.cost << "\n";
        });
    return os;
}

void BoostGraph::BoostPath::merge(const BoostPath& path, size_t pathStart)
{
    *this = getMerged(path, pathStart);
}

BoostGraph::BoostPath BoostGraph::BoostPath::getMerged(const BoostPath& path,
    size_t pathStart) const
{
    // If tree root is changed by merging NotATreeException will be thrown!

    BoostPath result = *this;

    // merge predecessor maps
    size_t curV = pathStart;
    size_t mergePoint = std::numeric_limits<size_t>::max();

    bool isFirst = true;
    do
    {
        // move up only after checking while condition
        if (isFirst)
            isFirst = false;
        else
            curV = path.predecessors[curV];

        if (mergePoint == std::numeric_limits<size_t>::max())
        {
            if (result.predecessors[curV] != curV ||
                result.predecessors[curV] == path.predecessors[curV])
                mergePoint = curV;
        }
        if (mergePoint != std::numeric_limits<size_t>::max() &&
            result.predecessors[curV] != path.predecessors[curV])
            throw NotATreeException();

        result.predecessors[curV] = path.predecessors[curV];
    } while (curV != path.predecessors[curV]);

    if (mergePoint == std::numeric_limits<size_t>::max())
        throw NotATreeException();

    // update distance map
    auto reversePath = result.predecessors.getReversePath(pathStart);

    auto edgeCost = [&result, &path](size_t src, size_t trg) -> long long
    {
        result.cost += path.distances[trg] - path.distances[src];

        return path.distances[trg] - path.distances[src];
    };

    reversePath.calculateDistance(mergePoint, edgeCost, result.distances);

    return result;
}

BoostGraph::BoostPath BoostGraph::BoostPath::merge(const BoostPath& path1,
    const BoostPath& path2, size_t path2Start)
{
    return path1.getMerged(path2, path2Start);
}
