#include <limits>
#include <chrono>
#include "dijkstra.h"

Solution DijkstraSolver::solve(Task const& task)
{
    Solution sol;
    Task _task = task;

    auto computedPaths = task.graph.shortestPaths(_task.source);
    BoostGraph spt, empty;
    std::map<Vertex, std::vector<Vertex>> terminalPaths;
    for (size_t tIdx = 0; tIdx < _task.terminals.size(); ++tIdx)
    {
        terminalPaths[_task.terminals[tIdx]] = _task.graph.shortestPathAsVector(
            _task.source, _task.terminals[tIdx], computedPaths.predecessors);
        if (terminalPaths[_task.terminals[tIdx]].empty())
        {
            sol.sptCost = 0;
            sol.tree = empty;
            return sol;
        }
        spt.merge(_task.graph.buildPathSubgraph(terminalPaths[_task.terminals[tIdx]], computedPaths.distances));
    }

    // Compute total tree cost
    sol.sptCost = spt.cost();
    // Convert list of edges to graph.
    sol.tree = std::move(spt);
    return sol;
}