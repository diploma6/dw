#pragma once

#include "boost_graph.h"
#include "dijkstra.h"
#include "dreyfus_wagner.h"
#include "delay_exact_solver.h"

#include <vector>
#include <algorithm>
#include <string>
#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <cmath>


using namespace std;
namespace po = boost::program_options;

class GraphGenerator
{
public:
	
	GraphGenerator(po::variables_map vm);

	bool generateGraph();

	~GraphGenerator();

private:

	size_t numberVertices;
	size_t numberEdges;
	size_t numberTerminals;

	vector<size_t> terminals;
	size_t source;
	vector<long long> weights;

	size_t scenario;
	vector<size_t> scenarios;
	size_t percentOneDirectionEdges;
	bool exactSolution; // false - Not need; true - Need
	bool direction; // false - Undirection; true - Direction
	bool typeTerminals; // false - allBlocks; true - oneBlock
	size_t index;
	size_t numBlockCpe;
	size_t numBlockMetro;
	size_t numV_metroAverage;
	size_t numV_cpeAverage;
	vector<size_t> verticesBlockMetro;
	vector<size_t> verticesBlockCpe;
	vector<size_t> start_finish_VertexCpeBlock;
	vector<size_t> start_finish_VertexMetroBlock;
	vector<size_t> edgesBlockMetro;
	vector<size_t> edgesBlockCpe;

	string nameFileGraph;
	string nameFileMeta;
	string nameFileSolution;

	size_t cost;
	bool statusOneScenario;
	size_t costTwoScenario;
	bool statusTwoScenario;
	size_t costThreeScenario;
	bool statusThreeScenario;
	size_t costFourScenario;
	bool statusFourScenario;
	size_t costFiveScenario;
	bool statusFiveScenario;
	size_t costSixScenario;
	bool statusSixScenario;

	vector<size_t> startV;
	vector<size_t> finishV;
	
	size_t numV_backbone;
	size_t numV_metro;
	size_t numV_cpe;
	size_t numE_backbone;

	vector<size_t> directionEdges; // 0 ->; 1 <-; 2 <->;
	vector<size_t> typeVertices; // 0 - BACKBONE; 1 - METRO; 2 - CPE;
	vector<size_t> indexMetro;

	// Info for scenario
	size_t backupSource;
	size_t sourceProtectionPath;
	vector<long long> delays;
	vector<size_t> newTerminals;
	vector<size_t> deleteTerminals;
	long long delayLimit;
	long long minDelay;
	long long maxDelay;

	// 

	template < typename T >
	T random(T start, T finish);

	bool createEdges();

	vector<size_t> parserScenarios(size_t s);

	vector<size_t> constVertexBlock(size_t size, size_t numAverage);

	vector<size_t> randomVertexBlock(size_t size, size_t numAverage);

	vector<size_t> countEdgeBlock(vector<size_t>& vec);

	vector<size_t> createCountEdgesTransition(vector<size_t>& numVertices);

	size_t summArray(vector<size_t>& array);

	vector<size_t> countStartFinishVertexBlock(vector<size_t>& array);

	vector<size_t> createCountVerticesTransition(
		size_t numBlock, size_t startRandom, size_t endRandom);

	void createPartGraph(size_t startVertex, size_t finishVertex, size_t countEdges);

	bool checkDoubleEdge(vector<size_t>& vecStart, vector<size_t>& vecFinish,
		size_t valueStart, size_t valueFinish);


	void mergeParts(
		size_t oneStart, size_t oneFinish,
		size_t twoStart, size_t twoFinish,
		size_t numVerticesOne, size_t numVerticesTwo, size_t countEdges);

	bool checkDoubleVertex(vector<size_t>& vec, size_t value);

	int checkDouble(vector<size_t>& vec, size_t value);

	size_t countNumVerticesEachBlock(vector<size_t>& vec, size_t start, size_t finish);

	bool createWeights();

	bool createTypes();

	bool createDirections();

	bool createTerminals();

	bool createScenarios();

	BoostGraph createBoostGraph(vector<long long>& w);

	bool writeGraphFile();

	bool writeRemark(ofstream& outFile);

	bool writeGraphScenarioFour(ofstream& outFile);
	
	bool writeGraph(ofstream& outFile);

	size_t countNumberArcs();
	
	bool writeTerminals(ofstream& outFile);

	bool writeMetaFile();

	bool writeGenerateParametersMetaFile(ofstream& outFile);

	bool writeScenariosMetaFile(ofstream& outFile);

	bool writeScenarioOneMetaFile(ofstream& outFile, size_t currScenario);

	bool writeScenarioTwoMetaFile(ofstream& outFile, size_t currScenario);
	
	bool writeScenarioThreeMetaFile(ofstream& outFile, size_t currScenario);
	
	bool writeScenarioFourMetaFile(ofstream& outFile, size_t currScenario);
	
	bool writeScenarioFiveMetaFile(ofstream& outFile, size_t currScenario);
	
	bool writeScenarioSixMetaFile(ofstream& outFile, size_t currScenario);
	
	bool writeEachCityMetaFile(ofstream& outFile);

	bool writeMetaInfoOneCity(ofstream& outFile, int id);

	size_t countExactSolution(BoostGraph& graph, vector<size_t>& _terminals, size_t _source);

	string findNameFileGraphSTP();

	string findNameFileMeta();

	string findNameFileSolution();

	bool generateDataScenarioOne();

	bool generateDataScenarioTwo();

	bool generateDataScenarioThree();

	bool generateDataScenarioFour();

	bool generateDataScenarioFive();

	bool generateDataScenarioSix();

	void updateDirectionAfterScenarios(BoostGraph& inputGraph, BoostGraph& additionalGraph);

	BoostGraph checkAllScenarios(BoostGraph& graph, bool needSolution);

	void addOneDirectionEdges(BoostGraph& inputGraph, BoostGraph& additionalGraph);

	BoostGraph checkScenarioOne(BoostGraph& graph);

	BoostGraph checkScenarioTwo(BoostGraph& graph);

	BoostGraph checkScenarioThree(BoostGraph& graph);

	BoostGraph checkScenarioFour(BoostGraph& graph);

	BoostGraph checkScenarioFive(BoostGraph& graph);

	BoostGraph checkScenarioSix(BoostGraph& graph);

	void changeOneDirection(Edge e, size_t dir);

	vector<size_t> createRandTerminals(BoostGraph& graph,
		size_t start, size_t finish, size_t sizeTerminals);

	BoostGraph deleteEdgesInResult(BoostGraph& graph, BoostGraph& result);

	void splitDirEdges(BoostGraph& graph, BoostGraph& twoDirGraph, BoostGraph& oneDirEdges);

	BoostGraph checkScenario(BoostGraph& graph, size_t number,
		vector<size_t>& _terminals, size_t _source, size_t& _cost);

	//size_t countMinDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source);

	//size_t countMaxDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source);

	//size_t countDelay(BoostGraph& graph, vector<size_t>& _terminals, size_t _source);
	
	//size_t findIdEdgeBoostGraphVector(Edge e);

	//size_t countSummDelay(BoostGraph& currResult);

	void setStatusScenario(bool status, size_t numberScenario);

	void writeSolutionScenarioOne(BoostGraph& resultGraph, ofstream& outFile);

	void writeSolutionScenarioTwo(BoostGraph& resultGraph, ofstream& outFile);

	void writeSolutionScenarioThree(BoostGraph& resultGraph, ofstream& outFile);

	void writeSolutionScenarioFive(BoostGraph& resultGraph, ofstream& outFile);

	void writeSolutionScenarioSix(BoostGraph& resultGraph, ofstream& outFile);

	void writeSolution(ofstream& outFile, BoostGraph& resultGraph,
		size_t number, vector<size_t>& _terminals, size_t _source, size_t& _cost);

	BoostGraph addTwoDirEdges(BoostGraph& graph);

	void removeEdges(vector<size_t>& vertices, vector<size_t>& edges);
};
