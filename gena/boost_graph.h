#pragma once

// STL
#include <functional>

// BGL
#include <boost/graph/adjacency_list.hpp>
// Parallel BGL
//#include <boost/graph/use_mpi.hpp>
//#include <boost/graph/distributed/adjacency_list.hpp>
//#include <boost/graph/distributed/dijkstra_shortest_paths.hpp>


//! Vertex structure
using Vertex = size_t;
//struct Vertex
//{
//    //! Vertex ID
//    size_t vertexID;
//};

//! No separate graph class for this particular implementation.
struct Edge {
    //! Start vertex
    size_t sourceID;
    //! End vertex
    size_t targetID;
    //! Edge cost
    long long cost;
    //! Edge delay
    long long delay;

    bool operator==(Edge const& rhs) const
    {
        return
            sourceID == rhs.sourceID &&
            targetID == rhs.targetID &&
            cost == rhs.cost;
    }
};

//! typedef of a templated directed adjacency_list
// with std::vector-Containers for nodes and arcs (=vecS)
// the nodes do not have any special properties (-> boost::no_property)
// while the arcs are defined with a bundled property
using PureBoostGraph = boost::adjacency_list< boost::vecS,
    boost::vecS,
    //boost::distributedS<boost::parallel::mpi::bsp_process_group, boost::vecS>,
    boost::bidirectionalS,
    Vertex,
    Edge >;

//! Boost adjacency list with support of disabling / enabling vertices.
class BoostGraph : public PureBoostGraph {
public:
    /* types */
    //! typedef of the vertex_descriptor depends of the defined graph type
    using BoostVertex = boost::graph_traits<BoostGraph>::vertex_descriptor;

    //! Ordered list of vertices
    class BoostPredecessorMap : public std::vector<BoostVertex>
    {
    public:
        BoostPredecessorMap(size_t size = 0);

        //using std::vector<BoostVertex>::vector;

        /*!
         * \brief Check if predecessor tree contains a loop function.
         * \param startV Vertice to start checking from
         * \return True if loop exists, false otherwise
         */
        bool isLoop(size_t startV) const;

        /*!
         * \brief Get predecessor graph with reverse path function.
         * \param startV Path start vertex
         */
        BoostPredecessorMap getReversePath(size_t startV) const;

        /*!
         * \brief Calculate distance on path from startV function.
         * \param startV Path start vertex
         * \param metric Edge metric
         * \param output Distance to vertices
         */
        void calculateDistance(size_t startV,
            const std::function<long long(size_t srcV, size_t trgV)>& metric,
            std::vector<long long>& output) const;
    };

    /*!
     * \brief Clear graph function.
     */
    void clear();

    /*!
     * \brief Add vertex function.
     * \param v Vertex to add.
     */
    void addVertex(size_t v);

    /*!
     * \brief Add edge function.
     * \param e Edge to add.
     * \param checkExists Check if edge exists.
     */
    void addEdge(Edge const& e, bool checkExists = true);

    /*!
     * \brief Add edge function.
     * \param sourceID Source ID.
     * \param targetID Target ID.
     * \param cost Edge cost.
     * \param delay Edge delay.
     * \param checkExists Check if edge exists.
     */
    void addEdge(size_t sourceID,
        size_t targetID,
        long long cost,
        long long delay,
        bool checkExists = true);

    /*!
     * \brief Add edge function.
     * \param sourceID Source ID.
     * \param targetID Target ID.
     * \param cost Edge cost.
     * \param checkExists Check if edge exists.
     */
    void addEdge(size_t sourceID,
        size_t targetID,
        long long cost,
        bool checkExists = true);

    /*!
     * \brief Remove edge function.
     * \param sourceID Source ID.
     * \param targetID Target ID.
     * \param checkUnique Check if [src; target] edge is unique.
     */
    void removeEdge(size_t sourceID,
        size_t targetID,
        bool checkUnique = false);

    /*!
     * \brief Remove edge function.
     * \param edge Edge.
     * * \param checkUnique Check if [src; target] edge is unique.
     */
    void removeEdge(Edge const& edge, bool checkUnique = false);

    /*!
     * \brief Get weight function.
     * \param sourceID Source ID.
     * \param targetID Target ID.
     * \param weight Edge weight.
     * \return true if edge exists, false otherwise.
     */
    bool getWeight(size_t sourceID,
        size_t targetID,
        long long& weight) const;

    /*!
     * \brief Get parallel edges (all edges with mentioned
     * src and dst but diff weights) function.
     * \param sourceID Source ID.
     * \param targetID Target ID.
     * \return Found edges.
     */
    std::vector<Edge> getParallelEdges(size_t sourceID,
        size_t targetID) const;

    /*!
     * \brief Get edge delay function.
     * \param sourceID Source vertex ID
     * \param targetID Target vertex ID
     * \param delay Edge delay store reference
     * \return true if edge exists, false otherwise
     */
    bool getDelay(size_t sourceID,
        size_t targetID,
        long long& delay) const;

    /*!
     * \brief Set edge delay function.
     * \param sourceID Source vertex ID
     * \param targetID Target vertex ID
     * \param delay New edge delay
     * \return true if edge exists, false otherwise
     */
    bool setDelay(size_t sourceID,
        size_t targetID,
        long long delay);

    /*!
     * \brief Compute vertex count function.
     * \return Vertex count.
     */
    size_t vertexCount() const;

    /*!
     * \brief Remove vertex function.
     * \param vertex Vertex to remove.
     */
    void removeVertex(size_t const vertex);

    /*!
     * \brief Get outcoming edges from vertex function.
     * \param vertexID Original vertex identifier.
     * \return begin and end edge iterators.
     */
    std::pair<out_edge_iterator, out_edge_iterator> outEdges(size_t vertexID) const;

    /*!
     * \brief Get incoming edges into vertex function.
     * \param vertexID Original vertex identifier
     * \return begin and end edge iterators.
     */
    std::pair<in_edge_iterator, in_edge_iterator> inEdges(size_t vertexID) const;

    /*!
     * \brief Evaluate cost function.
     * \return Cost.
     */
    size_t cost() const;

    /*!
     * \brief Evaluate delay function.
     * \return Delay.
     */
    long long delay() const;

    /*!
     * \brief Get boost vertex number by original function.
     * \param v Original vertex number
     * \return Boost vertex number.
     */
    size_t getBoostVByOrigV(size_t v) const;

    /*!
     * \brief Contains vertex check function.
     * \param vertex Vertex to be checked.
     * \return true if contains, false otherwise.
     */
    bool contains(size_t vertex) const;

    /*!
     * \brief Contains edge check function.
     * \param edge Edge to be checked
     * \param weightCheck Set true if needed
     * to check weight of searching edge.
     * \return true if contains, false otherwise.
     */
    bool contains(Edge const& edge, bool weightCheck = true) const;

    /*!
     * \brief The BoostPath struct
     */
    struct BoostPath
    {
        BoostPredecessorMap predecessors;
        std::vector<long long> distances;
        long long cost = 0;

        struct NotATreeException : public std::exception {};

        /*!
         * \brief Merge path into current tree functuon.
         * \details Throws NotATreeException if merge doesn't yield a tree.
         *          Tree root being changed by added path is not supported!
         *          NotATreeException will be thrown!
         * \param path Path to merge.
         * \param pathStart Path first vertex (predecessors[pathStart] != pathStart).
         */
        void merge(const BoostPath& path, size_t pathStart);

        /*!
         * \brief Merge path into current tree functuon.
         * \details Throws NotATreeException if merge doesn't yield a tree.
         *          Tree root being changed by added path is not supported!
         *          NotATreeException will be thrown!
         * \param path Path to merge.
         * \param pathStart Path first vertex (predecessors[pathStart] != pathStart).
         */
        BoostPath getMerged(const BoostPath& path, size_t pathStart) const;

        /*!
         * \brief Merge path2 into path1 functuon.
         * \details Throws NotATreeException if merge doesn't yield a tree.
         *          Tree root being changed by added path is not supported!
         *          NotATreeException will be thrown!
         * \param path1 Path to merge into.
         * \param path2 Path to merge.
         * \param path2Start Path 2 first vertex (predecessors[path2Start] != path2Start).
         */
        static BoostPath merge(const BoostPath& path1,
            const BoostPath& path2, size_t path2Start);
    };

    /*!
     * \brief Evaluate shortest paths from vertex function.
     * \param src Vertex to evaluate paths from
     * \return Predecessor map and distances array
     */
    BoostPath shortestPaths(size_t src) const;

    /*!
     * \brief Evaluate shortest path function.
     * \param src Source vertex
     * \param dst Destination vertex
     * \param paths BoostPath for source
     * \return
     */
    BoostGraph shortestPath(size_t src, size_t dst, BoostPath const& paths) const;

    /*!
     * \brief Evaluate shortest paths for negate weights from vertex function.
     * \param src Vertex to evaluate paths from
     * \return Predecessor map and distances array
     */
    BoostPath negateShortestPaths(size_t src) const;

    /*!
     * \brief Merge graphs function.
     * \param s1 First graph
     * \param s2 Second graph
     * \param s Result graph
     */
    static void merge(BoostGraph const& s1, BoostGraph const& s2, BoostGraph& s);

    /*!
     * \brief Merge graphs function.
     * \param s1 First graph
     * \param s2 Second graph
     * \param s Result graph
     */
    static void merge(BoostGraph&& s1, BoostGraph const& s2, BoostGraph& s);

    /*!
     * \brief Merge graph into current function.
     * \param s Graph to merge with.
     */
    void merge(BoostGraph const& s);

    /*!
     * \brief Merge graph into current function.
     * \param s Graph to merge with.
     */
    void merge(BoostGraph&& s);

    /*!
     * \brief Entire graph edges visitor function.
     * \param visitor function to be applied to each edge.
     */
    void applyEdges(std::function<void(Edge&)> visitor);

    /*!
     * \brief Entire graph edges visitor function.
     * \param visitor function to be applied to each edge.
     */
    void applyEdges(std::function<void(Edge const&)> visitor) const;

    /*!
     * \brief Entire graph vertices visitor function.
     * \param visitor function to be applied to each vertex.
     */
    void applyVertices(std::function<void(Vertex&)> visitor);

    /*!
     * \brief Entire graph vertices visitor function.
     * \param visitor function to be applied to each vertex.
     */
    void applyVertices(std::function<void(Vertex const&)> visitor) const;

    /*!
     * \brief Evaluate shortestPath [src; dst] function.
     * \param src Path source.
     * \param dst Path destination.
     * \param predecessors Predecessors map.
     * \return Evaluated path.
     */
    std::vector<size_t> shortestPathAsVector(size_t src, size_t dst, std::vector<size_t> const& predecessors) const;

    /*!
     * \brief Build path subgraph from vertex list function.
     * \param path Path represented as vertex array.
     * \param distMap Distance map.
     * \return Path represented as graph.
     */
    BoostGraph buildPathSubgraph(std::vector<size_t> const& path,
        std::vector<long long> const& distMap) const;

    /*!
     * \brief Build tree subgraph from BoostPath function.
     * \param source Tree source vertex.
     * \param terminals Tree termainl vertices.
     * \param tree Tree represented as BoostPath.
     * \return Tree represented as graph.
     */
    BoostGraph buildTreeSubgraph(size_t source,
        const std::vector<size_t>& terminals,
        const BoostGraph::BoostPath& tree) const;

    /*!
     * \brief Get vertices function.
     * \return
     */
    std::vector<size_t> vertices() const;

    /*!
     * \brief Get in degree of vertex function.
     * \param v Vertex.
     * \return Degree.
     */
    size_t inDegree(Vertex v) const;

    /*!
     * \brief Get out degree of vertex function.
     * \param v Vertex.
     * \return Degree.
     */
    size_t outDegree(Vertex v) const;

    long long getMinDelayLimit(size_t source, const std::vector<size_t>& terminals) const;

    long long getMaxDelayLimit(size_t source, const std::vector<size_t>& terminals) const;

    friend std::ostream& operator<<(std::ostream& os, BoostGraph const& g);

private:
    /*!
     * \brief Private merge: s2 into s.
     * \param s Graph to be merged in.
     * \param s2 Graph to merge.
     */
    static void merge(BoostGraph& s, BoostGraph const& s2);

    /*!
     * \brief Recompute map from original vertex number to boost function.
     * \details Must be called after vertex remove.
     */
    void recomputeMapOrigToBoost(void);

    /*!
     * \brief Get edge data function.
     * \param sourceID Source vertex ID
     * \param targetID Target vertex ID
     * \param edge Edge data store reference
     * \return true if edge exists, false otherwise
     */
    bool getEdge(size_t sourceID,
        size_t targetID,
        Edge& edge) const;

    std::unordered_map<size_t, BoostGraph::vertex_descriptor> _mapOrigVToBoostV;
};
