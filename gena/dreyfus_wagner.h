#pragma once

#include "common.h"
#include "boost_graph.h"

#include <vector>

using namespace std;

/*!
 * \brief Solves task defined in SPT by Dreyfus-Wagner algorithm.
 */
class DreyfusWagnerSolver
{
public:

    //DreyfusWagnerSolver(BoostGraph& graph, vector<size_t>& terminals, size_t source);

    //size_t costDreyfusWagner();
    //BoostGraph solutionDreyfusWagner();

    /*!
     * \brief Solves task.
     * \param task Task (problem) to solve.
     * \return Problem solution.
     */
    Solution solve(Task const& task);

private:

    /*!
     * \brief Builds MST over one vertex function.
     * \param terminal Vertex (terminal) number
     * \param resMinSpanTree MST
     */
    void buildMinSpanTree1Vert(size_t terminal, BoostGraph& resMinSpanTree) const;

    /*!
     * \brief Builds MST over two terminals function.
     * \param terminal0 Terminal number
     * \param terminal1 Terminal number
     * \param resMinSpanTree MST
     */
    void buildMinSpanTree2Vert(size_t terminal0, size_t terminal1,
        BoostGraph& resMinSpanTree) const;

    /*!
     * \brief MST solver function
     * \return MST
     */
    Solution buildMinSpanTree(void);

    /*!
     * \brief Builds MST for trivial cases (1, 2 terminal) function.
     * \param m_terminals Terminal set
     * \param resMinSpanTree MST
     * \return true if trivial case, false otherwise
     */
    bool mstTrivialCases(std::vector<size_t> const& m_terminals, BoostGraph& resMinSpanTree);

    /*!
     * \brief Build MST over 1 terminal and ALL vertices function.
     * \param m_terminals Terminal set
     * \param m_tsize |T|
     * \param vertexCount |V|
     */
    void mst1Terminal(std::vector<size_t> const& m_terminals,
        size_t m_tsize,
        size_t vertexCount);

    /*!
     * \brief Restore MST function.
     * \param vc |V|
     * \param m_tsize |T|
     * \param terminalBack T[-1]
     * \return MST
     */
    BoostGraph mstFull(size_t vc, size_t m_tsize, size_t terminalBack) const;

    /*!
     * \brief Recursive restore MST function (TMP) function.
     * \param x_comb Curent terminal combination
     * \param vc |V|
     * \param m_tsize |T|
     * \param v Common boostvertex
     * \param mst MST spanning over X U {v}
     */
    void mstFullRec(size_t x_comb, size_t vc, size_t m_tsize, size_t v, BoostGraph& mst) const;


    /*!
     * \brief Precompute Dijkstra function.
     */
    void precomputeDijkstra();

    /*!
     * \brief Evaluate shortest path cost between 2 vertices function.
     * \param v Source ID
     * \param w Target ID
     * \return Cost
     */
    size_t shortestPathCost(size_t v, size_t w) const;

    size_t combinationIndex(size_t terminalComb,
        size_t w, size_t vertexCount) const;

    //std::unordered_map<size_t, BoostGraph::BoostPath> precomputedPaths;
    std::vector<BoostGraph::BoostPath> precomputedPaths;

    struct SolutionData
    {
        //! idx of T(x1 U v)
        size_t bipartition1_comb = maxUint;
        //! common vertex
        size_t w = maxUint;
        //! weight
        size_t weight = maxUint;
    };

    SolutionData* solution_costs;
    static const size_t maxUint;
    BoostGraph _task;
    vector<size_t> _terminals;
    size_t _source;
};