#pragma once

#include <string>
#include <vector>
#include <boost/variant.hpp>

#include "boost_graph.h"

/*!
 * \brief Task (problem) with graph and terminals.
 */
struct Task {
    BoostGraph graph;
    size_t source = std::numeric_limits<size_t>::max();
    std::vector<size_t> terminals;
    size_t validCost = std::numeric_limits<size_t>::max();
    long long delayLimit = 0;
};

std::ostream& operator<<(std::ostream& os, Task const& t);

/*!
 * \brief Solution of SPT problem.
 */
struct Solution {
    size_t sptCost;
    BoostGraph tree;
};

class UnreachableTerminalException : public std::runtime_error
{
public:
    UnreachableTerminalException(size_t source, size_t terminal)
        : std::runtime_error("Terminal " + std::to_string(terminal) +
            " is unreachable from source " + std::to_string(source) + "!") {}
};
