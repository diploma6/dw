#include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <random>

#include "dspt_solver.h"

Solution DSPTSolver::solve(Task const& task)
{
    if (task.terminals.size() == 0)
        return Solution{ 0, BoostGraph() };

    Solution solution;

    auto computedPaths = makeDSPT(task.graph, task.source,
        task.terminals, task.delayLimit);

    BoostGraph spt = task.graph.buildTreeSubgraph(task.source,
        task.terminals, computedPaths);

    // Compute total tree cost
    solution.sptCost = spt.cost();
    // Convert list of edges to graph.
    solution.tree = std::move(spt);

    return solution;
} // end of 'DSPTSolver::solve' function

void DSPTSolver::cutBranch(const BoostGraph &g, BoostGraph::BoostPredecessorMap &tree,
    const std::vector<size_t> &boostTerminals, size_t startV) const
{
    size_t curV = startV;

    do
    {
        // remove edge from final tree and move up the path
        std::swap(tree[curV], curV);

        // check if curV has another child in tree
        // (in addition to the one we came from)
        auto edgePair = boost::out_edges(curV, g);
        for (BoostGraph::out_edge_iterator e = edgePair.first; e != edgePair.second; ++e)
        {
            size_t boostChild = boost::target(*e, g);
            if (tree[boostChild] == curV)
                // reached vertex with more then 1 child, branch cut
                return;
        }
    } while (tree[curV] != curV &&                             // not at source
        !std::binary_search(boostTerminals.begin(),       // not at terminal
            boostTerminals.end(), curV));
} // end of 'DSPTSolver::cutBranch' function

std::vector<std::future<BoostGraph::BoostPath>>
    DSPTSolver::findShortestPaths(const BoostGraph &g)
{
    size_t vc = g.vertexCount();
    _minDelayMap = std::vector<long long>(vc);

    // calculate minimum delay to each vertex
    boost::dijkstra_shortest_paths(
        g,
        // start from source
        _boostSrc,
        // delay as cost function
        weight_map(boost::get(&Edge::delay, g))
        // record to '_minDelayMap'
        .distance_map(boost::make_iterator_property_map(
            _minDelayMap.begin(),
            boost::get(boost::vertex_index, g)))
    );

    // make reverse graph from copy because reverse graph operator[] doesn't work if made from *this for some GOSH DARN REASON
    _copy = g;
    auto reverseGraph = boost::make_reverse_graph(_copy);

    _costDelayMap = std::map<BoostReverseGraph::edge_descriptor, CostDelay>();
    auto edgePair = boost::edges(reverseGraph);
    for (auto e = edgePair.first; e != edgePair.second; ++e)
    {
        _costDelayMap.insert({ *e,
            { reverseGraph[*e].cost, reverseGraph[*e].delay,
              g.getBoostVByOrigV(reverseGraph[*e].sourceID) } });
    }

    // run Dijkstra on reverse graph from each terminal in parallel
    auto runDijkstra = [this, vc](size_t boostT) -> BoostGraph::BoostPath
    {
        // check if path exists
        if (_minDelayMap[boostT] > _delayLimit)
            throw UnreachableTerminalException(_copy[_boostSrc], _copy[boostT]);

        auto reverseGraph = boost::make_reverse_graph(_copy);
        BoostGraph::BoostPredecessorMap predMap(vc);
        std::vector<CostDelay> fromTerminalCostDelayMap(vc);

        // custom compare and combine functions to check delay constraint
        CostDelay zero = { 0, 0, 0 }, inf = { CostDelay::MAX, CostDelay::MAX, 0 };
        auto compare = [](const CostDelay &a, const CostDelay &b) -> bool
        {
            // compare with infinity
            if (b.delay == CostDelay::MAX)
                return true;

            return a.cost < b.cost;
        };
        auto combine = [&](const CostDelay &a, const CostDelay &b) -> CostDelay
        {
            // combine with zero edge
            if (b.delay == 0)
                return a;

            // adding to infinity results in infinity
            if (a.delay == CostDelay::MAX)
                return a;

            // return infinity if delay constraint is broken
            if (a.delay + b.delay > _delayLimit - _minDelayMap[b.curV])
                return inf;

            return { a.cost + b.cost, a.delay + b.delay, b.curV };
        };

        class Visitor : public boost::default_dijkstra_visitor
        {
        private:
            size_t _boostSrc;

        public:
            Visitor(size_t boostSrc) : _boostSrc(boostSrc)
            {
            }

            void examine_vertex(const BoostReverseGraph::vertex_descriptor &v,
                const BoostReverseGraph &g) const
            {
                g[v];
                if (v == _boostSrc)
                    throw std::runtime_error("reached source");
            }
        };

        // run Dijkstra
        try
        {
            boost::dijkstra_shortest_paths(
                // on reverse graph
                reverseGraph,
                // start from terminal
                boostT,
                // edge cost and delay as weight map
                weight_map(boost::associative_property_map<std::map<BoostReverseGraph::edge_descriptor, CostDelay>>(_costDelayMap))
                // save sum cost and delay from terminal
                .distance_map(boost::make_iterator_property_map(
                    fromTerminalCostDelayMap.begin(),
                    boost::get(boost::vertex_index, reverseGraph)))
                // record predecessor graph to 'predMap'
                .predecessor_map(boost::make_iterator_property_map(
                    predMap.begin(),
                    boost::get(boost::vertex_index, reverseGraph)))
                // custom compare function to prevent constraint breaking relaxations
                .distance_compare(compare)
                .distance_combine(combine)
                .distance_zero(zero)
                .distance_inf(inf)
                .visitor(Visitor(_boostSrc))
            );
        }
        catch (const std::runtime_error &)
        {
            ; // found shortest path to source and exited prematurely
        }

        std::vector<long long> delays(vc);

        for (size_t i = 0; i < vc; i++)
            delays[i] = fromTerminalCostDelayMap[i].delay;

        // reverse path to be from terminal to source
        return { predMap.getReversePath(_boostSrc),
                 std::move(delays) };
    };

    std::vector<std::future<BoostGraph::BoostPath>> paths(_boostTerminals.size());
    for (size_t i = 0; i < _boostTerminals.size(); i++)
        paths[i] = std::async(std::launch::async, runDijkstra, _boostTerminals[i]);

    return paths;
} // end of 'DSPTSolver::findShortestPaths' function

BoostGraph::BoostPath DSPTSolver::makeDSPT(const Task &task)
{
    return makeDSPT(task.graph, task.source, task.terminals, task.delayLimit);
} // end of 'DSPTSolver::shortestPaths' function

BoostGraph::BoostPath DSPTSolver::makeDSPT(const BoostGraph &g, size_t src,
    std::vector<size_t> const &terminals, long long delayLimit)
{
    if (delayLimit <= 0)
        throw std::runtime_error("Delay limit <= 0!");

    _delayLimit = delayLimit;
    _boostSrc = g.getBoostVByOrigV(src);

    auto vc = g.vertexCount();

    // TODO: check for duplicate terminals
    _boostTerminals = std::vector<size_t>(terminals.size());
    for (size_t i = 0; i < terminals.size(); i++)
        _boostTerminals[i] = g.getBoostVByOrigV(terminals[i]);
    std::sort(_boostTerminals.begin(), _boostTerminals.end());

    std::vector<std::future<BoostGraph::BoostPath>> paths = findShortestPaths(g);

    // merge paths into a tree
    BoostGraph::BoostPredecessorMap finalTree(vc);
    for (size_t i = 0; i < _boostTerminals.size(); i++)
    {
        // extract path
        BoostGraph::BoostPath path = paths[i].get();

        // merge into 'finalTree'
        bool wasCutBranchFlag = false;
        // starting from terminal
        size_t curV = _boostTerminals[i];
        std::vector<long long> fromSourceDelayMap(vc, std::numeric_limits<long long>::max());//CostDelay::MAX);
        fromSourceDelayMap[_boostSrc] = 0;
        while (curV != path.predecessors[curV])
        {
            // if current vertex already in final tree try merging
            if (finalTree[curV] != curV)
            {
                auto fromSourceDelay = [&](size_t curV) -> long long
                {
                    if (fromSourceDelayMap[curV] == std::numeric_limits<long long>::max()/*CostDelay::MAX*/)
                    {
                        auto delayFunc = [&g](size_t srcV, size_t trgV) -> size_t
                        {
                            return g[boost::edge(srcV, trgV, g).first].delay;
                        };
                        finalTree.getReversePath(curV)
                            .calculateDistance(_boostSrc, delayFunc, fromSourceDelayMap);
                    }
                    return fromSourceDelayMap[curV];
                };

                // if cut branches before and created a loop
                // or delay constraint on path source -> curV -> terminal[i] is broken
                if ((wasCutBranchFlag && finalTree.isLoop(curV)) ||
                    fromSourceDelay(curV) + path.distances[curV] > delayLimit)
                {
                    // can't merge, cut branch and
                    // continue writing path into final tree
                    cutBranch(g, finalTree, _boostTerminals, curV);
                    wasCutBranchFlag = true;
                }
                else
                    // succesfully merged
                    break;
            } // end of trying to merge at curV

            // continue writing current path to final tree
            size_t nextV = path.predecessors[curV];
            finalTree[curV] = nextV;
            curV = nextV;
        } // end of merging path into final tree
    }

    // calculate distances in final tree
    std::vector<long long> finalDist(vc, std::numeric_limits<long long>::max());//CostDelay::MAX);
    finalDist[_boostSrc] = 0;
    auto costFunc = [&g](size_t srcV, size_t trgV) -> size_t
    {
        return g[boost::edge(srcV, trgV, g).first].cost;
    };
    for (size_t boostT : _boostTerminals)
        finalTree.getReversePath(boostT).
        calculateDistance(_boostSrc, costFunc, finalDist);

    return { std::move(finalTree), std::move(finalDist) };
} // end of 'DSPTSolver::shortestPaths' function
