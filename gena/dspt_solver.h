#pragma once

#include <future>
#include <boost/graph/reverse_graph.hpp>
#include <limits>

#include "common.h"

//! Shortest path tree with delay constraint solver
class DSPTSolver
{
public:
    /*!
     * \brief Solves task.
     * \param task Task (problem) to solve.
     * \return Problem solution.
     */
    Solution solve(Task const& task);

    /*!
     * \brief Compute delay constrained shortest path tree function.
     * \param g Graph
     * \param src Source vertex
     * \param terminals Terminal vertices
     * \param delayLimit Delay limit for delay constraint
     * \return DSPT in boost path form.
     */
    BoostGraph::BoostPath makeDSPT(const BoostGraph &g, size_t src,
        std::vector<size_t> const& terminals, long long delayLimit);

    /*!
     * \brief Compute delay constrained shortest path tree function.
     * \param task Task
     * \return DSPT in boost path form.
     */
    BoostGraph::BoostPath makeDSPT(const Task &task);

protected:
    using BoostReverseGraph = boost::reverse_graph<BoostGraph>;

    struct CostDelay
    {
        static const long long MAX = std::numeric_limits<long long>::max();

        long long cost, delay;
        size_t curV;
    };

    //! Source vertex mapped to boost graph
    size_t _boostSrc;
    //! Terminal vertices mapped to boost graph, sorted
    std::vector<size_t> _boostTerminals;
    //! Maximum allowed sum delay of edges on path from source to any terminal
    long long _delayLimit;
    std::vector<long long> _minDelayMap;

    /*!
     * \brief Find shortest paths from source to each terminal function.
     * \param g Graph
     * \returns vector of shortest paths that satisfy delay constraint.
     *          Distances are delay from terminal!
     */
    virtual std::vector<std::future<BoostGraph::BoostPath>>
        findShortestPaths(const BoostGraph &g);

    /*!
     * \brief Cut branch operation function.
     * \details Remove parent edges in predecessor tree from startV to
     *          first encountered of the following:
     *           - source
     *           - terminal
     *           - vertex with more then one child
     * \param g Full graph
     * \param tree Predecessor tree to cut branch from
     * \param boostTerminals Terminal vertices
     * \param startV Vertice to cut from
     */
    void cutBranch(const BoostGraph &g, BoostGraph::BoostPredecessorMap &tree,
        const std::vector<size_t> &boostTerminals, size_t startV) const;

private:
    std::map<BoostReverseGraph::edge_descriptor, CostDelay> _costDelayMap;
    BoostGraph _copy;
};
